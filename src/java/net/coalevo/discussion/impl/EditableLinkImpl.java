/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.EditableLink;
import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.foundation.model.AgentIdentifier;

import java.util.Set;

/**
 * Implements {@link EditableLink}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class EditableLinkImpl
    extends LinkImpl
    implements EditableLink {

  public EditableLinkImpl(ForumIdentifier fid, String id, AgentIdentifier author,boolean approved) {
    super(id);
    m_ForumIdentifier = fid;
    m_Author = author;
    m_Approved = approved;
    m_Created = System.currentTimeMillis();
    m_Modified = m_Created;
    m_Functional = false;
    m_LastChecked = -1;    
  }//constructor

  public EditableLinkImpl(ForumIdentifier fid, String id, AgentIdentifier author,
                          String caption, String descformat,
                          String description, String link,
                          Set<String> tags, boolean approved,
                          boolean functional, long created,
                          long modified, long lastchecked) {
    super(id);
    m_ForumIdentifier = fid;
    m_Author = author;
    m_Caption = caption;
    m_DescriptionFormat = descformat;
    m_Description = description;
    m_Link = link;
    m_Tags = tags;
    m_Approved = approved;
    m_Functional = functional;
    m_Created = created;
    m_Modified = modified;
    m_LastChecked = lastchecked;
  }//constructor

  public void setCaption(String caption) {
    m_Caption = caption;
  }//setCaption

  public void setDescriptionFormat(String format) {
    m_DescriptionFormat = format;
  }//setDescriptionFormat

  public void setDescription(String desc) {
    m_Description = desc;
  }//setDescription

  public void setLink(String link) {
    m_Link = link;
  }//setLink

  public void setFunctional(boolean b) {
    m_Functional = b;
  }//setFunctional

  public void setTags(Set<String> tags) {
    m_Tags = tags;
  }//setTags

  public void addTag(String tag) {
    m_Tags.add(tag);
  }//addTag

  public void removeTag(String tag) {
    m_Tags.remove(tag);
  }//removeTag

  public void setApproved(boolean b) {
    m_Approved = b;
  }//setApproved

  public void setModified() {
    m_Modified = System.currentTimeMillis();
  }//setModified

  public void updateLastChecked() {
    m_LastChecked = System.currentTimeMillis();
  }//updateLastChecked

  public LinkImpl toLink() {
    return new LinkImpl(
        this.m_ForumIdentifier,
        this.m_Identifier,
        this.m_Author,
        this.m_Caption,
        this.m_DescriptionFormat,
        this.m_Description,
        this.m_Link,
        this.m_Tags,
        this.m_Approved,
        this.m_Functional,
        this.m_Created,
        this.m_Modified,
        this.m_LastChecked
    );
  }//toLink

}//class EditableLinkImpl
