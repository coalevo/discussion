/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.bank.service.BankService;
import net.coalevo.discussion.model.*;
import net.coalevo.discussion.service.DiscussionService;
import net.coalevo.foundation.model.*;
import net.coalevo.security.model.NoSuchActionException;
import net.coalevo.security.model.PolicyProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import org.osgi.framework.BundleContext;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.List;
import java.util.WeakHashMap;

/**
 * Implements {@link DiscussionService} based on the {@link DiscussionStore}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class DiscussionServiceImpl
    extends BaseService
    implements DiscussionService {

  private Marker m_LogMarker = MarkerFactory.getMarker(DiscussionServiceImpl.class.getName());
  private Messages m_BundleMessages;

  //Coalevo Services
  private ServiceMediator m_Services;

  //Security Context
  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;

  //Store
  private DiscussionStore m_DiscussionStore;

  //Indices
  private EntryIndex m_EntryIndex;

  //Transaction Trackers
  private WeakHashMap<ForumIdentifier, EditableForumDescriptorImpl> m_CreateForumTracker;
  private WeakHashMap<String, SubscriptionImpl> m_CreateSubscriptionTracker;
  private WeakHashMap<String, EditableLinkImpl> m_CreateLinkTracker;
  private WeakHashMap<String, EditableEntryImpl> m_CreateEntryTracker;

  //Manager
  private ForaManager m_ForaManager;
  private SubscriptionManager m_SubscriptionManager;
  private BanManager m_BanManager;
  private LinkManager m_LinkManager;
  private EntryManager m_EntryManager;
  private NotificationManager m_NotificationManager;

  //Anon author
  private AgentIdentifier m_AnonymousAuthor;

  public DiscussionServiceImpl(DiscussionStore ds) {
    super(DiscussionService.class.getName(), ACTIONS);
    m_DiscussionStore = ds;
  }//DiscussionServiceImpl


  public boolean activate(BundleContext bc) {
    m_Services = Activator.getServices();
    m_BundleMessages = Activator.getBundleMessages();

    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/DiscussionService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bc);


    //3. prepare refs
    m_ForaManager = m_DiscussionStore.getForaManager();
    m_BanManager = m_DiscussionStore.getBanManager();
    m_SubscriptionManager = m_DiscussionStore.getSubscriptionManager();
    m_LinkManager = m_DiscussionStore.getLinkManager();
    m_EntryManager = m_DiscussionStore.getEntryManager();
    m_NotificationManager = m_DiscussionStore.getNotificationManager();
    m_NotificationManager.activate(bc, m_ServiceAgentProxy);

    //4. Prepare Indices
    final String eipath = Activator.getIndexDir("entries").getAbsolutePath();

    m_EntryIndex = new EntryIndex(eipath);
    try {
      m_EntryIndex.init();
      if (!m_EntryIndex.exists()) {
        m_EntryIndex.rebuildIndex(m_DiscussionStore);
        Activator.log().info(m_LogMarker, m_BundleMessages.get("DiscussionServiceImpl.index.built", "index", "entry"));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("DiscussionServiceImpl.index.error", "index", "entry"), ex);
    }
    Activator.log().info(m_LogMarker, m_BundleMessages.get("DiscussionServiceImpl.index.success", "index", "entry"));

    //Transaction Trackers
    m_CreateForumTracker = new WeakHashMap<ForumIdentifier, EditableForumDescriptorImpl>(10);
    m_CreateSubscriptionTracker = new WeakHashMap<String, SubscriptionImpl>(25);
    m_CreateLinkTracker = new WeakHashMap<String, EditableLinkImpl>(25);
    m_CreateEntryTracker = new WeakHashMap<String, EditableEntryImpl>(50);

    //Anonymous author
    m_AnonymousAuthor = new AgentIdentifier("anonymous");

    return true;
  }//activate

  public boolean deactivate() {
    //Notification
    try {
      m_NotificationManager.deactivate();
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "deactivate()", ex);
    }

    if (m_PolicyProxy != null) {
      m_PolicyProxy.deactivate();
      m_PolicyProxy = null;
    }
    if (m_ServiceAgentProxy != null) {
      m_ServiceAgentProxy.deactivate();
      m_ServiceAgentProxy = null;
    }

    //Managers
    m_ForaManager = null;
    m_BanManager = null;
    m_SubscriptionManager = null;
    m_LinkManager = null;
    m_EntryManager = null;
    m_NotificationManager = null;

    //Trackers
    m_CreateForumTracker = null;
    m_CreateSubscriptionTracker = null;
    m_CreateLinkTracker = null;
    m_CreateEntryTracker = null;

    m_Services = null;
    return true;
  }//deactivate

  //*** Fora Handling ****//

  public EditableForumDescriptor beginForumCreate(Agent caller)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_FORUM);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_FORUM.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //generate
    EditableForumDescriptorImpl edfd =
        new EditableForumDescriptorImpl(new ForumIdentifierImpl(m_Services.getUUIDGeneratorService(ServiceMediator.NO_WAIT).getUID()), caller.getAgentIdentifier());
    //track
    m_CreateForumTracker.put(edfd.getIdentifier(), edfd);

    return edfd;
  }//beginForumCreate

  public void cancelForumCreate(Agent caller, EditableForumDescriptor efd)
      throws SecurityException, IllegalStateException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_FORUM);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_FORUM.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    if (m_CreateForumTracker.remove(efd.getIdentifier()) == null) {
      throw new IllegalStateException();
    }
  }//cancelForumCreate

  public void commitForum(Agent caller, EditableForumDescriptor efd)
      throws SecurityException, IllegalStateException, DiscussionServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_FORUM);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_FORUM.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    if (!m_CreateForumTracker.containsKey(efd.getIdentifier())) {
      throw new IllegalStateException();
    }
    EditableForumDescriptorImpl efdi = (EditableForumDescriptorImpl) efd;

    m_ForaManager.create(efdi);
    //Subscription of Forum Moderator....
    Subscription s = new SubscriptionImpl(efd.getIdentifier(),
        efd.getModerator(), "Moderator", System.currentTimeMillis(), System.currentTimeMillis(), true);
    m_SubscriptionManager.create(s);

    //Notification
    m_NotificationManager.addForum(efd.getMnemonic());

    //if finished remove from tracker
    m_CreateForumTracker.remove(efd.getIdentifier());

  }//commitForum

  public void destroyForum(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), DESTROY_FORUM);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", DESTROY_FORUM.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    ForumDescriptor fd = m_ForaManager.get(fid);
    m_ForaManager.destroy(fid);
    m_NotificationManager.removeForum(fd.getMnemonic());
    fd = null;
  }//destroyForum

  public ForumDescriptor getForumDescriptorBySequenceNumber(Agent caller, int seqnum)
      throws SecurityException, NoSuchForumException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_DESCRIPTOR);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_DESCRIPTOR.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    ForumDescriptor fd = m_ForaManager.getForumBySeqNum(seqnum);
    if (fd == null) {
      throw new NoSuchForumException(
          m_BundleMessages.get("DiscussionServiceImpl.forum.nonexistant", "seqnum", Integer.toString(seqnum))
      );
    }
    return fd;

  }//getForumDescriptorBySequenceNumber

  public ForumDescriptor getForumDescriptorByName(Agent caller, String name)
      throws SecurityException, NoSuchForumException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_DESCRIPTOR);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_DESCRIPTOR.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    ForumDescriptor fd = m_ForaManager.getForumByName(name);
    if (fd == null) {
      throw new NoSuchForumException(
          m_BundleMessages.get("DiscussionServiceImpl.forum.nonexistant", "name", name)
      );
    }
    return fd;
  }//getForumDescriptorByName

  public ForumDescriptor getForumDescriptorByMnemonic(Agent caller, String mnemonic)
      throws SecurityException, NoSuchForumException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_DESCRIPTOR);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_DESCRIPTOR.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    ForumDescriptor fd = m_ForaManager.getForumByMnemonic(mnemonic);
    if (fd == null) {
      throw new NoSuchForumException(
          m_BundleMessages.get("DiscussionServiceImpl.forum.nonexistant", "mnemonic", mnemonic)
      );
    }
    return fd;
  }//getForumDescriptorByMnemonic

  public ForumDescriptor getForumDescriptor(Agent caller, ForumIdentifier fid)
      throws SecurityException, NoSuchForumException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_DESCRIPTOR);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_DESCRIPTOR.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    ForumDescriptor fd = m_ForaManager.get(fid);
    if (fd == null) {
      throw new NoSuchForumException(
          m_BundleMessages.get("DiscussionServiceImpl.forum.nonexistant", "id", fid.getIdentifier())
      );
    }
    return fd;
  }//getForumDescriptor

  public List<String> resolveIdentifiers(Agent caller, List<ForumIdentifier> list, boolean names) {
    if (names) {
      return m_ForaManager.resolveToNames(list);
    } else {
      return m_ForaManager.resolveToMnemonics(list);
    }
  }//resolveIdentifiers

  public List<ForumDescriptor> listFora(Agent caller)
      throws SecurityException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_FORA);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_FORA.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //Note this is a copy
    return m_ForaManager.getForumDescriptors();
  }//listFora

  public List<ForumDescriptor> listPublicFora(Agent caller) {
    return m_ForaManager.getPublicForumDescriptors();
  }//listPublicFora

  public List<String> listForaNames(Agent caller)
      throws SecurityException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_FORA);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_FORA.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //Note this is a copy
    return m_ForaManager.getForaNames();
  }//listForaNames

  public List<String> listForaMnemonics(Agent caller)
      throws SecurityException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_FORA);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_FORA.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //Note this is a copy
    return m_ForaManager.getForaMnemonics();
  }//listForaMnemonics

  public List<ForumIdentifier> listForaWithNewEntries(Agent caller, long timestamp)
      throws SecurityException, DiscussionServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_FORA);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_FORA.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_ForaManager.listForaWithNewEntries(caller.getAgentIdentifier(), timestamp);

  }//hasNewEntries

  public void assignForumModerator(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException, DiscussionServiceException, NoSuchForumException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), ASSIGN_FORUM_MODERATOR);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", ASSIGN_FORUM_MODERATOR.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    m_ForaManager.updateModerator(fid, aid);
  }//assignForumModerator

  public Content getForumInfo(Agent caller, ForumIdentifier fid)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_FORUM_INFO);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_FORUM_INFO.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_ForaManager.getForumInfo(fid);
  }//getForumInfo

  //*** END Fora Handling ***//

  //*** Subscription Handling ***//

  public Subscription beginSubscriptionCreate(Agent caller,
                                              ForumIdentifier fid,
                                              AgentIdentifier aid)
      throws SecurityException, BannedException {
    if (!aid.equals(caller.getAgentIdentifier()) && !m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), SUBSCRIBE);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", SUBSCRIBE.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
      //Invite Check
      if (m_ForaManager.isInviteOnly(fid) && !m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
        throw new SecurityException("inviteonly");
      }
    }
    //Ban check
    if (m_BanManager.isBanned(fid, aid)) {
      throw new BannedException();
    }

    SubscriptionImpl s = new SubscriptionImpl(fid, aid);

    //track
    m_CreateSubscriptionTracker.put(s.getIdentifier(), s);

    return s;
  }//beginSubscriptionCreate

  public void commitSubscription(Agent caller, Subscription s)
      throws SecurityException, IllegalStateException, DiscussionServiceException {

    //implicit policy check, when the transaction was started
    if (!m_CreateSubscriptionTracker.containsKey(s.getIdentifier())) {
      throw new IllegalStateException();
    }

    m_SubscriptionManager.create(s);
    m_NotificationManager.updateNotification(s);

    //if finished remove from tracker
    m_CreateSubscriptionTracker.remove(s.getIdentifier());
  }//commitSubscription

  public Subscription getSubscription(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException, DiscussionServiceException, NoSuchSubscriptionException {

    if (!aid.equals(caller.getAgentIdentifier()) && !m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_SUBSCRIPTION);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_SUBSCRIPTION.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }

    return m_SubscriptionManager.getSubscription(fid, aid);
  }//getSubscription

  public void updateSubscription(Agent caller, Subscription s) {
    if (!s.getSubscriber().equals(caller.getAgentIdentifier())
        && !m_ForaManager.isModerator(s.getForumIdentifier(), caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_SUBSCRIPTION);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_SUBSCRIPTION.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    s.setDirty(true);
    m_NotificationManager.updateNotification(s);
  }//updateSubscription

  public void cancelSubscription(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException, DiscussionServiceException {

    if (!aid.equals(caller.getAgentIdentifier()) && !m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CANCEL_SUBSCRIPTION);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CANCEL_SUBSCRIPTION.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    m_SubscriptionManager.cancel(fid, aid);
  }//cancelSubscription

  public boolean isInviteOnly(Agent caller, ForumIdentifier fid)
      throws NoSuchForumException {

    m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);

    return m_ForaManager.isInviteOnly(fid);
  }//isInviteOnly

  public boolean isSubscribed(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException {

    if (!aid.equals(caller.getAgentIdentifier()) && !m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CHECK_SUBSCRIPTION);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CHECK_SUBSCRIPTION.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    return m_SubscriptionManager.isSubscribed(fid, aid);
  }//isSubscribed

  public List<AgentIdentifier> listSubscribers(Agent caller, ForumIdentifier fid)
      throws SecurityException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_SUBSCRIBERS);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_SUBSCRIBERS.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    return m_SubscriptionManager.listSubscribers(fid);
  }//listSubscribers

  public List<ForumIdentifier> listSubscriptions(Agent caller, AgentIdentifier aid)
      throws SecurityException, DiscussionServiceException {

    if (!aid.equals(caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_SUBSCRIPTIONS);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_SUBSCRIPTIONS.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }

    return m_SubscriptionManager.listSubscriptions(aid);
  }//listSubscriptions

  //*** END Subscription Handling ***//

  //*** Bans (see ForumModeratorServiceImpl for more functionality) ***//

  public boolean isBanned(Agent caller, ForumIdentifier fid, AgentIdentifier aid) throws SecurityException {
    if (!aid.equals(caller.getAgentIdentifier()) && !m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CHECK_BAN);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CHECK_BAN.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    return m_BanManager.isBanned(fid, aid);
  }//isBanned

  //*** END Bans ***//


  private void checkForumAccess(ForumIdentifier fid, AgentIdentifier aid)
      throws NotSubscribedException, BannedException {

    //Ban Check
    if (m_BanManager.isBanned(fid, aid)) {
      throw new BannedException();
    }
    //Subscription check
    if (!m_SubscriptionManager.isSubscribed(fid, aid)) {
      throw new NotSubscribedException();
    }
  }//checkForumAccess

  //** Link Handling **//

  public EditableLink beginLinkCreate(Agent caller, ForumIdentifier fid)
      throws SecurityException, BannedException, NotSubscribedException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_LINK);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_LINK.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //Ensure forum access
    checkForumAccess(fid, caller.getAgentIdentifier());

    //Create and track link
    EditableLinkImpl link = new EditableLinkImpl(fid, m_Services.getUUIDGeneratorService(ServiceMediator.NO_WAIT).getUID(), caller.getAgentIdentifier(), false);
    m_CreateLinkTracker.put(link.getIdentifier(), link);

    return link;
  }//beginLinkCreate

  public void commitLink(Agent caller, EditableLink link)
      throws SecurityException, DiscussionServiceException, IllegalStateException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_LINK);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_LINK.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //Check transaction, access implicit, because it was checked
    //when the transaction was started.
    if (!m_CreateLinkTracker.containsKey(link.getIdentifier())) {
      throw new IllegalStateException();
    }

    //Approval logic
    ForumDescriptor fd = m_ForaManager.get(link.getForumIdentifier());
    if (link.getAuthor().equals(fd.getModerator())) {
      link.setApproved(true);
    }

    //Create and add to caches
    m_LinkManager.create((EditableLinkImpl) link);

    //Remove from tracker
    m_CreateLinkTracker.remove(link.getIdentifier());
  }//commitLink

  public void cancelLinkCreate(Agent caller, EditableLink el)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_LINK);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_LINK.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //Remove tracked link
    if (m_CreateLinkTracker.containsKey(el.getIdentifier())) {
      m_CreateLinkTracker.remove(el.getIdentifier());
    }
  }//beginLinkCreate

  public void removeLink(Agent caller, ForumIdentifier fid, String id)
      throws SecurityException, DiscussionServiceException {

    LinkImpl l = m_LinkManager.get(fid, id);
    if (!l.getAuthor().equals(caller.getAgentIdentifier())
        && !m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), REMOVE_LINK);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", REMOVE_LINK.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    m_LinkManager.remove(fid, id);
  }//removeLink

  public List<String> listLinks(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_LINKS);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_LINKS.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_LinkManager.getLinkList(fid);
  }//listLinks

  public Link getLink(Agent caller, ForumIdentifier fid, String id)
      throws SecurityException, DiscussionServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_LINK);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_LINK.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_LinkManager.get(fid, id);
  }//getLink

  public int getLinkCount(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_LINK_COUNT);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_LINK_COUNT.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_LinkManager.getLinkList(fid).size();
  }//getLinkCount

  //*** END Link Handling ***//

  //*** Entry Handling ***//

  public EditableEntry beginEntryCreate(Agent caller, ForumIdentifier fid)
      throws SecurityException, BannedException, NotSubscribedException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_ENTRY);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_ENTRY.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //check forum access
    checkForumAccess(fid, caller.getAgentIdentifier());

    //Create and track
    EditableEntryImpl eei =
        new EditableEntryImpl(fid, m_Services.getUUIDGeneratorService(ServiceMediator.NO_WAIT).getUID(), caller.getAgentIdentifier());
    m_CreateEntryTracker.put(eei.getIdentifier(), eei);

    return eei;
  }//beginEntryCreate

  public EditableEntry beginOfficialEntryCreate(Agent caller, ForumIdentifier fid)
      throws SecurityException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_OFFICIAL_ENTRY);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_OFFICIAL_ENTRY.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    //Create and track
    EditableEntryImpl eei =
        new EditableEntryImpl(fid, m_Services.getUUIDGeneratorService(ServiceMediator.NO_WAIT).getUID(), caller.getAgentIdentifier());
    eei.setOfficial(true);
    eei.setApproved(true); //approve it

    m_CreateEntryTracker.put(eei.getIdentifier(), eei);

    return eei;
  }//beginOfficialEntryCreate

  public EditableEntry beginAnonymousEntryCreate(Agent caller, ForumIdentifier fid)
      throws SecurityException, AnonymousNotAllowedException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_ANON_ENTRY);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_ANON_ENTRY.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    if (m_ForaManager.get(fid).isAnonymousAllowed()) {
      //Create and track
      EditableEntryImpl eei =
          new EditableEntryImpl(fid, m_Services.getUUIDGeneratorService(ServiceMediator.NO_WAIT).getUID(), m_AnonymousAuthor);

      m_CreateEntryTracker.put(eei.getIdentifier(), eei);

      return eei;
    } else {
      throw new AnonymousNotAllowedException();
    }
  }//beginAnonymousEntryCreate

  public void commitEntry(Agent caller, EditableEntry entry)
      throws SecurityException, IllegalStateException, DiscussionServiceException {
    boolean award = false;
    if (!m_CreateEntryTracker.containsKey(entry.getIdentifier())) {
      throw new IllegalStateException();
    }
    EditableEntryImpl eei = (EditableEntryImpl) entry;

    ForumDescriptor fd = m_ForaManager.get(eei.getForumIdentifier());

    //Approval logic
    if (fd.isModeratedStrict()) {
      if (eei.isOfficial() || eei.getAuthor().equals(fd.getModerator())) {
        eei.setApproved(true);
      } else {
        eei.setApproved(false);
      }
    } else {
      if (eei.getAuthor().equals(m_AnonymousAuthor)) {
        eei.setApproved(false);
      } else {
        eei.setApproved(true);
        award = true;
      }
    }
    //create entry
    m_EntryManager.create(eei);
    if (award) {
      award(eei.getAuthor(), 1);
    }
    //approved notify now
    if (eei.isApproved()) {
      m_NotificationManager.notifySubscribers(eei, false);
    }
    //Remove from tracker
    m_CreateEntryTracker.remove(eei.getIdentifier());
  }//commitEntry

  public void cancelEntryCreate(Agent caller, EditableEntry ee)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_ENTRY);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_ENTRY.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //Remove tracked entry
    if (m_CreateEntryTracker.containsKey(ee.getIdentifier())) {
      m_CreateEntryTracker.remove(ee.getIdentifier());
    }
  }//cancelEntryCreate

  public void removeEntry(Agent caller, ForumIdentifier fid, String id)
      throws SecurityException, DiscussionServiceException {

    EntryDescriptorImpl edi = m_EntryManager.getDescriptor(fid, id);
    if (!edi.getAuthor().equals(caller.getAgentIdentifier())
        && !m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), REMOVE_ENTRY);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", REMOVE_ENTRY.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    m_EntryManager.remove(fid, id);
  }//removeEntry

  public List<String> listEntries(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_ENTRIES);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_ENTRIES.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_EntryManager.getActiveEntryList(fid);
  }//listEntries

  public List<String> listEntriesFrom(Agent caller, ForumIdentifier fid, long timestamp)
      throws SecurityException, DiscussionServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_ENTRIES);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_ENTRIES.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_EntryManager.getActiveEntryList(fid, timestamp);
  }//listEntriesFrom

  public List<String> listEntries(Agent caller, ForumIdentifier fid, int numentries)
      throws SecurityException, DiscussionServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_ENTRIES);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_ENTRIES.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_EntryManager.getActiveEntryList(fid, numentries);
  }//listEntries

  public int getEntryCount(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_ENTRIES);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_ENTRIES.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_EntryManager.getActiveEntryCount(fid);
  }//getEntryCount

  public boolean hasNewEntries(Agent caller, ForumIdentifier fid, long timestamp)
      throws SecurityException, DiscussionServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CHECK_NEW_ENTRIES);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CHECK_NEW_ENTRIES.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_EntryManager.hasNewEntries(fid, timestamp);
  }//hasNewEntries

  public int getNewEntryCount(Agent caller, ForumIdentifier fid, long timestamp)
      throws SecurityException, DiscussionServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CHECK_NEW_ENTRIES);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CHECK_NEW_ENTRIES.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_EntryManager.getActiveEntryList(fid, timestamp).size();
  }//getNewEntryCount

  public EntryDescriptor getEntryDescriptor(Agent caller, ForumIdentifier fid, String id)
      throws SecurityException, DiscussionServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_ENTRY);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_ENTRY.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_EntryManager.getDescriptor(fid, id);
  }//getEntryDescriptor

  public Content getEntryContent(Agent caller, ForumIdentifier fid, String id)
      throws SecurityException, DiscussionServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_ENTRY);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_ENTRY.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_EntryManager.getContent(fid, id);
  }//getEntryContent

  public List<EntryHit> searchEntries(Agent caller, String query, int numhits)
      throws SecurityException, DiscussionServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_ENTRY);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_ENTRY.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_EntryIndex.searchEntries(
        query,
        m_ForaManager.resolveToMnemonics(
            m_SubscriptionManager.listSubscriptions(caller.getAgentIdentifier())
        ),
        numhits
    );
  }//searchEntries

  public void rebuildEntryIndex(Agent caller)
      throws SecurityException, DiscussionServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), REBUILD_ENTRYINDEX);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", REBUILD_ENTRYINDEX.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(
        caller,
        new Runnable() {
          public void run() {
            try {
              m_EntryIndex.rebuildIndex(m_DiscussionStore);
            } catch (Exception ex) {
              Activator.log().error("rebuildEntryIndex()", ex);
            }
          }
        }
    );
  }//rebuildEntryIndex


  public void updateEntryIndex(Agent caller)
      throws SecurityException, DiscussionServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_ENTRYINDEX);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_BundleMessages.get("error.action", "action", UPDATE_ENTRYINDEX.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(
        caller,
        new Runnable() {
          public void run() {
            try {
              m_EntryIndex.updateIndex(m_DiscussionStore);
            } catch (Exception ex) {
              Activator.log().error(m_LogMarker, "updateEntryIndex()", ex);
            }
          }
        }
    );
  }//updateEntryIndex

  //*** END Entry Handling ***//

  protected void award(AgentIdentifier aid, long amount) {
    BankService bank = m_Services.getBankService(ServiceMediator.NO_WAIT);
    if (bank == null) {
      return;
    }
    try {
      if (bank.existsAccount(m_ServiceAgentProxy.getAuthenticPeer(), aid)) {
        bank.deposit(m_ServiceAgentProxy.getAuthenticPeer(), aid, amount);
        Activator.log().info(m_LogMarker, m_BundleMessages.get("DiscussionServiceImpl.award", "sum", "" + amount, "aid", aid.getIdentifier()));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "award", ex);
    }
  }//award

  public void doMaintenance(Agent caller)
      throws SecurityException, MaintenanceException {
    //1. Security
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), Maintainable.DO_MAINTENANCE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", Maintainable.DO_MAINTENANCE.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.start"));

    //maintain store
    Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.store"));
    m_DiscussionStore.maintain();

    //Update entry index
    Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.entryindex"));
    try {
      m_EntryIndex.updateIndex(m_DiscussionStore);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.entryindex"), ex);
    }

    //clear trackers; this means that actually happening transactions will fail
    //however, it will remove any possibly existing stale transactions.
    Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.trackers"));
    m_CreateForumTracker.clear();
    m_CreateSubscriptionTracker.clear();
    m_CreateLinkTracker.clear();
    m_CreateEntryTracker.clear();

    Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.end"));
  }//doMaintenance

  /**
   * Forum Handling Actions
   */
  private static Action CREATE_FORUM = new Action("createForum");
  private static Action ASSIGN_FORUM_MODERATOR = new Action("assignForumModerator");
  private static Action DESTROY_FORUM = new Action("destroyForum");
  private static Action GET_DESCRIPTOR = new Action("getForumDescriptor");
  private static Action LIST_FORA = new Action("listFora");
  private static Action GET_FORUM_INFO = new Action("getForumInfo");


  /**
   * Subscription Handling Actions
   */
  private static Action SUBSCRIBE = new Action("subscribe");
  private static Action GET_SUBSCRIPTION = new Action("getSubscription");
  private static Action UPDATE_SUBSCRIPTION = new Action("updateSubscription");
  private static Action CANCEL_SUBSCRIPTION = new Action("cancelSubscription");
  private static Action CHECK_SUBSCRIPTION = new Action("isSubscribed");
  private static Action LIST_SUBSCRIBERS = new Action("listSubscribers");
  private static Action LIST_SUBSCRIPTIONS = new Action("listSubscriptions");

  /**
   * Ban Handling Actions
   */
  private static Action CHECK_BAN = new Action("isBanned");


  /**
   * Link Handling Actions
   */
  private static Action CREATE_LINK = new Action("createLink");
  private static Action REMOVE_LINK = new Action("removeLink");
  private static Action LIST_LINKS = new Action("listLinks");
  private static Action GET_LINK = new Action("getLink");
  private static Action GET_LINK_COUNT = new Action("getLinkCount");

  /**
   * Entry Handling
   */
  private static Action CREATE_ENTRY = new Action("createEntry");
  private static Action CREATE_OFFICIAL_ENTRY = new Action("createOfficialEntry");
  private static Action CREATE_ANON_ENTRY = new Action("createAnonymousEntry");
  private static Action REMOVE_ENTRY = new Action("removeEntry");
  private static Action LIST_ENTRIES = new Action("listEntries");
  private static Action CHECK_NEW_ENTRIES = new Action("hasNewEntries");
  private static Action GET_ENTRY = new Action("getEntry");
  private static Action REBUILD_ENTRYINDEX = new Action("rebuildEntryIndex");
  private static Action UPDATE_ENTRYINDEX = new Action("updateEntryIndex");

  private static Action[] ACTIONS = {
      CREATE_FORUM, ASSIGN_FORUM_MODERATOR, DESTROY_FORUM, GET_DESCRIPTOR, LIST_FORA,
      GET_FORUM_INFO, SUBSCRIBE, GET_SUBSCRIPTION,
      CANCEL_SUBSCRIPTION, CHECK_SUBSCRIPTION, LIST_SUBSCRIBERS, UPDATE_SUBSCRIPTION,
      CHECK_BAN, CREATE_LINK, REMOVE_LINK, LIST_LINKS, GET_LINK, GET_LINK_COUNT,
      CREATE_ENTRY, CREATE_OFFICIAL_ENTRY, CREATE_ANON_ENTRY, REMOVE_ENTRY,
      LIST_ENTRIES, CHECK_NEW_ENTRIES, GET_ENTRY, REBUILD_ENTRYINDEX,
      Maintainable.DO_MAINTENANCE, Restoreable.DO_BACKUP, Restoreable.DO_RESTORE};


}//class DiscussionServiceImpl
