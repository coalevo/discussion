/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.*;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.text.util.TagUtility;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Implements an in-memory database for the forum descriptors.
 * <p/>
 * The idea of the separation of the infos from the descriptors
 * was to allow listings and lookups.
 * The performance on these operations seems to be essential
 * for the actual user experience.
 * Memory considerations will depend on the amount of foras that
 * are stored in the database, however, it has to be taken into account
 * that this way there will be only one of each instance in memory for
 * all (they are immutable by default).
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ForaManager {

  private Marker m_LogMarker = MarkerFactory.getMarker(ForaManager.class.getName());
  private DiscussionStore m_DiscussionStore;

  //Lists
  private List<ForumDescriptorImpl> m_ForumDescriptors;
  private List<LastEntry> m_LastEntries;

  //Info Cache
  private LRUCacheMap<ForumIdentifier, Content> m_ForumInfoCache;

  //Lookup Maps
  private Map<ForumIdentifier, ForumDescriptorImpl> m_ID2Descriptor;
  private Map<String, ForumDescriptorImpl> m_Mnemonic2Descriptor;
  private Map<String, ForumDescriptorImpl> m_Name2Descriptor;
  private Map<Integer, ForumDescriptorImpl> m_SeqNum2Descriptor;

  //Rebuild Lock
  private ReentrantLock m_ModificationLock;

  public ForaManager(DiscussionStore store, int ficachesize) {
    m_DiscussionStore = store;
    m_ModificationLock = new ReentrantLock();
    m_ForumInfoCache = new LRUCacheMap<ForumIdentifier, Content>(ficachesize);
  }//ForaManager

  public void setForumInfoCacheSize(int size) {
    m_ForumInfoCache.setCeiling(size);
  }//setForumInfoCacheSize

  public void clear() {
    m_ForumInfoCache.clear();
    m_ForumDescriptors.clear();
    m_ID2Descriptor.clear();
    m_Mnemonic2Descriptor.clear();
    m_SeqNum2Descriptor.clear();
  }//clear

  /**
   * Returns a new <tt>List</tt> of {@link ForumDescriptorImpl}.
   * <p/>
   * The returned list can be filtered (e.g. references removed).
   *
   * @return a new list of forum
   */
  public List<ForumDescriptor> getForumDescriptors() {
    isUpToDate();
    return new ArrayList<ForumDescriptor>(m_ForumDescriptors);
  }//getForumDescriptors

  public List<ForumDescriptor> getPublicForumDescriptors() {
    isUpToDate();
    ArrayList<ForumDescriptor> list = new ArrayList<ForumDescriptor>(m_ForumDescriptors);
    for (Iterator<ForumDescriptor> iterator = list.listIterator(); iterator.hasNext();) {
      ForumDescriptor forumDescriptor = iterator.next();
      if (!forumDescriptor.isPublic()) {
        iterator.remove();
      }
    }
    return list;
  }//getPublicForumDescriptors

  public ForumDescriptorImpl get(ForumIdentifier fid) {
    return m_ID2Descriptor.get(fid);
  }//get

  public ForumDescriptorImpl getForumByName(String name) {
    isUpToDate();
    return m_Name2Descriptor.get(name);
  }//getForumByName

  public boolean containsForumName(String name) {
    isUpToDate();
    return m_Name2Descriptor.containsKey(name);
  }//containsForumName

  public List<String> getForaNames() {
    isUpToDate();
    return new ArrayList<String>(m_Name2Descriptor.keySet());
  }//getForaNames

  public ForumDescriptorImpl getForumByMnemonic(String mnemonic) {
    isUpToDate();
    return m_Mnemonic2Descriptor.get(mnemonic);
  }//getForumByMnemonic

  public boolean containsForumMnemonic(String mnemonic) {
    isUpToDate();
    return m_Mnemonic2Descriptor.containsKey(mnemonic);
  }//containsForumMnemonic

  public List<String> getForaMnemonics() {
    isUpToDate();
    return new ArrayList<String>(m_Mnemonic2Descriptor.keySet());
  }//getForaMnemonics

  public ForumDescriptorImpl getForumBySeqNum(int num) {
    isUpToDate();
    return m_SeqNum2Descriptor.get(num);
  }//getForumDescriptorBySeqNum

  public boolean containsForumSeqNum(int num) {
    isUpToDate();
    return m_SeqNum2Descriptor.containsKey(num);
  }//containsForumSeqNum

  public List<Integer> getForaSequenceNumbers() {
    isUpToDate();
    return new ArrayList<Integer>(m_SeqNum2Descriptor.keySet());
  }//getForaSequenceNumbers

  public List<String> resolveToNames(List<ForumIdentifier> fids) {
    isUpToDate();
    List<String> names = new ArrayList<String>(fids.size());
    for (ForumIdentifier fid : fids) {
      ForumDescriptor fd = m_ID2Descriptor.get(fid);
      names.add(fd.getName());
    }
    return names;
  }//resolveToNames

  public String resolveToMnemonic(ForumIdentifier fid) {
    return get(fid).getMnemonic();
  }//resolveToMnemonic

  public List<String> resolveToMnemonics(List<ForumIdentifier> fids) {
    isUpToDate();
    List<String> names = new ArrayList<String>(fids.size());
    for (ForumIdentifier fid : fids) {
      ForumDescriptor fd = m_ID2Descriptor.get(fid);
      if (fd != null) {
        names.add(fd.getMnemonic());
      }
    }
    return names;
  }//resolveToMnemonics

  private void remove(ForumIdentifier fid) {
    try {
      m_ModificationLock.lock();
      ForumDescriptorImpl fd = m_ID2Descriptor.remove(fid);
      m_ForumDescriptors.remove(fd);
      m_Mnemonic2Descriptor.remove(fd.getMnemonic());
      m_Name2Descriptor.remove(fd.getName());
      m_SeqNum2Descriptor.remove(fd.getSequenceNumber());

      //Remove the last entry from the list
      synchronized (m_LastEntries) {
        for (Iterator<LastEntry> iterator = m_LastEntries.iterator(); iterator.hasNext();) {
          LastEntry lastEntry = iterator.next();
          if (lastEntry.getForumIdentifier().equals(fid)) {
            iterator.remove();
          }
        }
      }
    } finally {
      m_ModificationLock.unlock();
    }
  }//remove

  public void create(EditableForumDescriptorImpl efd)
      throws DiscussionServiceException {

    //Add to RDBMS
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", efd.getIdentifier().getIdentifier());
    params.put("seqnum", "" + efd.getSequenceNumber());
    params.put("mnemonic", efd.getMnemonic());
    params.put("name", efd.getName());
    params.put("language", efd.getLanguage().getLanguage());
    params.put("creator_id", efd.getCreator().getIdentifier());
    params.put("moderator_id", efd.getModerator().getIdentifier());
    params.put("tags", TagUtility.fromSet(efd.getTags()));
    params.put("infoformat", efd.getInfoFormat());
    params.put("created", "" + efd.getCreated());
    params.put("modified", "" + efd.getModified());
    params.put("inviteonly", (efd.isInviteOnly()) ? "1" : "0");
    params.put("strictmod", (efd.isModeratedStrict()) ? "1" : "0");
    params.put("anonymous", (efd.isAnonymousAllowed()) ? "1" : "0");
    params.put("pubaccess", (efd.isPublic()) ? "1" : "0");
    params.put("maxactive", Integer.toString(efd.getMaxActive()));

    HashMap<String, String> params1 = new HashMap<String, String>();
    params1.put("forum_id", efd.getIdentifier().getIdentifier());
    params1.put("document", "");

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("createForumDescriptor", params);
      lc.executeUpdate("createForumInfo", params1);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "commitForum()::", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("ForaManager.createfailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }

    //Add to Caches
    add(efd.toForumDescriptor());
  }//create

  public void updateModerator(ForumIdentifier fid, AgentIdentifier aid)
      throws DiscussionServiceException, NoSuchForumException {

    ForumDescriptorImpl fd = (ForumDescriptorImpl) get(fid);
    if (fd == null) {
      throw new NoSuchForumException(fid.getIdentifier());
    }

    //Update RDBMS:
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    params.put("moderator_id", aid.getIdentifier());

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("assignForumModerator", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "assignForumModerator()::", ex);
      throw new DiscussionServiceException(ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }

    //Update MMDB
    EditableForumDescriptorImpl edf =
        (EditableForumDescriptorImpl) fd.toEditableForumDescriptor();
    edf.setModerator(aid);
    update(edf.toForumDescriptor());
  }//updateModerator

  public void updateForumDescriptor(EditableForumDescriptor fd)
      throws DiscussionServiceException {

    EditableForumDescriptorImpl efd = (EditableForumDescriptorImpl) fd;
    efd.setModified();

    //Update RDBMS:
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", efd.getIdentifier().getIdentifier());
    params.put("seqnum", "" + efd.getSequenceNumber());
    params.put("mnemonic", efd.getMnemonic());
    params.put("name", efd.getName());
    params.put("language", efd.getLanguage().getLanguage());
    params.put("creator_id", efd.getCreator().getIdentifier());
    params.put("moderator_id", efd.getModerator().getIdentifier());
    params.put("tags", TagUtility.fromSet(efd.getTags()));
    params.put("infoformat", efd.getInfoFormat());
    params.put("created", "" + efd.getCreated());
    params.put("modified", "" + efd.getModified());
    params.put("inviteonly", (efd.isInviteOnly()) ? "1" : "0");
    params.put("strictmod", (efd.isModeratedStrict()) ? "1" : "0");
    params.put("anonymous", (efd.isAnonymousAllowed()) ? "1" : "0");
    params.put("pubaccess", (efd.isPublic()) ? "1" : "0");
    params.put("maxactive", Integer.toString(efd.getMaxActive()));

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("updateForumDescriptor", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "updateForumDescriptor()::", ex);
      throw new DiscussionServiceException(ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }

    //Update caches
    update(efd.toForumDescriptor());

  }//updateForum

  public void destroy(ForumIdentifier fid)
      throws DiscussionServiceException {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("destroyForum", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "destroyForum()::", ex);
      throw new DiscussionServiceException(ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }
    //remove from cache
    remove(fid);
    m_ForumInfoCache.remove(fid);
  }//destroy

  public boolean isModerator(ForumIdentifier fid, AgentIdentifier aid) {
    ForumDescriptorImpl fd = get(fid);
    return fd.getModerator().equals(aid);
  }//isModerator

  public boolean isInviteOnly(ForumIdentifier fid) {
    ForumDescriptorImpl fd = get(fid);
    if (fd == null) {
      throw new NoSuchForumException(fid.toString());
    } else {
      return fd.isInviteOnly();
    }
  }//isInviteOnly


  private void update(ForumDescriptorImpl fd) {
    try {
      m_ModificationLock.lock();
      fd.invalidate();
      //Remove
      ForumDescriptorImpl oldfd = m_ID2Descriptor.remove(fd.getIdentifier());
      m_ForumDescriptors.remove(oldfd);
      m_Mnemonic2Descriptor.remove(fd.getMnemonic());
      m_Name2Descriptor.remove(fd.getName());
      m_SeqNum2Descriptor.remove(fd.getSequenceNumber());

      //Add new
      m_ForumDescriptors.add(fd);
      m_ID2Descriptor.put(fd.getIdentifier(), fd);
      m_Mnemonic2Descriptor.put(fd.getMnemonic(), fd);
      m_Name2Descriptor.put(fd.getName(), fd);
      m_SeqNum2Descriptor.put(fd.getSequenceNumber(), fd);

    } finally {
      m_ModificationLock.unlock();
    }
  }//update

  /**
   * Adds a newly created forum to the cache.
   *
   * @param fd a {@link ForumDescriptorImpl}.
   */
  private void add(ForumDescriptorImpl fd) {
    try {
      m_ModificationLock.lock();
      m_ForumDescriptors.add(fd);
      m_ID2Descriptor.put(fd.getIdentifier(), fd);
      m_Mnemonic2Descriptor.put(fd.getMnemonic(), fd);
      m_Name2Descriptor.put(fd.getName(), fd);
      m_SeqNum2Descriptor.put(fd.getSequenceNumber(), fd);
    } finally {
      m_ModificationLock.unlock();
    }
  }//add

  public void prepare() throws Exception {
    try {
      m_ModificationLock.lock();
      //List
      m_ForumDescriptors = new ArrayList<ForumDescriptorImpl>();

      //Build list
      ResultSet rs = null;

      LibraryConnection lc = null;
      try {
        lc = m_DiscussionStore.leaseConnection();
        rs = lc.executeQuery("listFora", null);
        while (rs.next()) {
          ForumDescriptorImpl fd = new ForumDescriptorImpl(
              m_DiscussionStore.getForumIdentifier(rs.getString(1)), //fid
              rs.getInt(2), //seqnum
              rs.getString(3), //mnemonic
              rs.getString(4), //name
              new Locale(rs.getString(5)), //language
              m_DiscussionStore.getAgentIdentifier(rs.getString(6)), //creator
              m_DiscussionStore.getAgentIdentifier(rs.getString(7)), //moderator
              TagUtility.toSet(rs.getString(8)), //tags
              rs.getString(9), //info format
              rs.getLong(10), //created
              rs.getLong(11), //modified
              rs.getShort(12) == 1, //inviteonly
              rs.getShort(13) == 1, //strictmod
              rs.getShort(14) == 1, //anonymous allowed
              rs.getShort(15) == 1,  //public
              rs.getInt(16) //maxactive
          );
          m_ForumDescriptors.add(fd);
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "prepare()::", ex);
        throw new DiscussionServiceException(ex);
      } finally {
        SqlUtils.close(rs);
        m_DiscussionStore.releaseConnection(lc);
      }

      //build lookup maps
      //Lookup maps
      m_ID2Descriptor =
          new HashMap<ForumIdentifier, ForumDescriptorImpl>(Math.round(m_ForumDescriptors.size() * 1.25f), 0.75f);
      m_Mnemonic2Descriptor =
          new HashMap<String, ForumDescriptorImpl>(Math.round(m_ForumDescriptors.size() * 1.25f), 0.75f);
      m_Name2Descriptor =
          new HashMap<String, ForumDescriptorImpl>(Math.round(m_ForumDescriptors.size() * 1.25f), 0.75f);
      m_SeqNum2Descriptor =
          new HashMap<Integer, ForumDescriptorImpl>(Math.round(m_ForumDescriptors.size() * 1.25f), 0.75f);

      for (Iterator<ForumDescriptorImpl> iterator = m_ForumDescriptors.iterator(); iterator.hasNext();) {
        ForumDescriptorImpl fd = iterator.next();
        m_ID2Descriptor.put(fd.getIdentifier(), fd);
        m_Mnemonic2Descriptor.put(fd.getMnemonic(), fd);
        m_Name2Descriptor.put(fd.getName(), fd);
        m_SeqNum2Descriptor.put(fd.getSequenceNumber(), fd);
      }

      //Build last entry list
      m_LastEntries = new ArrayList<LastEntry>(m_ForumDescriptors.size());
      for (Iterator<ForumDescriptorImpl> iterator = m_ForumDescriptors.iterator(); iterator.hasNext();) {
        ForumDescriptorImpl fd = iterator.next();
        rs = null;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("forum_id", fd.getIdentifier().getIdentifier());
        try {
          lc = m_DiscussionStore.leaseConnection();
          rs = lc.executeQuery("listLastActiveEntry", params);
          if (rs.next()) {
            m_LastEntries.add(new LastEntry(fd.getIdentifier(), rs.getLong(2)));
          }
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "prepare()::", ex);
          throw new DiscussionServiceException(ex);
        } finally {
          SqlUtils.close(rs);
          m_DiscussionStore.releaseConnection(lc);
        }
      }//for

      Collections.sort(m_LastEntries, LASTENTRY_COMPARATOR);

    } finally {
      m_ModificationLock.unlock();
    }
  }//prepare

  private void isUpToDate() {
    if (m_ModificationLock.isLocked()) {
      try {
        //wait for lock
        m_ModificationLock.lock();
      } finally {
        //unlock
        m_ModificationLock.unlock();
      }
    }
  }//isUpToDate

  //*** Forum Info Handling ***//

  public Content getForumInfo(ForumIdentifier fid) {

    Content c = m_ForumInfoCache.get(fid);
    if (c == null) {
      ResultSet rs = null;
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("forum_id", fid.getIdentifier());
      LibraryConnection lc = null;
      try {
        lc = m_DiscussionStore.leaseConnection();
        rs = lc.executeQuery("getForumInfo", params);
        if (rs.next()) {
          c = new ContentImpl(rs.getString(1));
        }
        if (c == null || c.isEmpty()) {
          return EMPTY_CONTENT;
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, Activator.getBundleMessages().get("ForaManager.foruminfo.getfailed"), ex);
      } finally {
        SqlUtils.close(rs);
        m_DiscussionStore.releaseConnection(lc);
      }
      m_ForumInfoCache.put(fid, c);
    }
    return c;
  }//getForumInfo

  public void createForumInfo(ForumIdentifier fid, EditableContent ec) {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    params.put("document", ec.getContent());
    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("createForumInfo", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, Activator.getBundleMessages().get("ForaManager.foruminfo.createfailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }
    m_ForumInfoCache.put(fid, ec.toContent());
  }//createForumInfo

  public void updateForumInfo(ForumIdentifier fid, EditableContent ec) {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    params.put("document", ec.getContent());
    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("updateForumInfo", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, Activator.getBundleMessages().get("ForaManager.foruminfo.updatefailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }
    //update cache
    m_ForumInfoCache.put(fid, ec.toContent());
  }//updateForumInfo

  public void updateLastEntry(ForumIdentifier fid, long timestamp) {
    synchronized (m_LastEntries) {
      for (Iterator<LastEntry> iterator = m_LastEntries.iterator(); iterator.hasNext();) {
        LastEntry lastEntry = iterator.next();
        if (lastEntry.getForumIdentifier().equals(fid)) {
          iterator.remove();
          lastEntry.m_Timestamp = timestamp;
          m_LastEntries.add(0, lastEntry); //should be first now
          break;//because the iterator was invalidated anyway
        }
      }
    }
  }//updateLastEntry

  public List<ForumIdentifier> listForaWithNewEntries(AgentIdentifier aid, long timestamp) {
    int idx = Collections.binarySearch(m_LastEntries, timestamp);
    ArrayList<ForumIdentifier> ids = new ArrayList<ForumIdentifier>(m_LastEntries.size() - idx);
    for (Iterator<LastEntry> iterator = m_LastEntries.listIterator(idx); iterator.hasNext();) {

      ids.add(iterator.next().getForumIdentifier());
    }
    return ids;
  }//listForaWithNewEntries


  class LastEntry implements Comparable<Long> {

    ForumIdentifier m_ForumIdentifier;
    Long m_Timestamp;

    public LastEntry(ForumIdentifier fid, long timestamp) {
      m_ForumIdentifier = fid;
      m_Timestamp = new Long(timestamp);
    }//constructor

    public ForumIdentifier getForumIdentifier() {
      return m_ForumIdentifier;
    }//getForumIdentifier

    public int compareTo(Long l) {
      if (this.m_Timestamp > l) {
        return 1;
      } else if (this.m_Timestamp < l) {
        return -1;
      } else {
        return 0;
      }
    }//compareTo

  }//inner class LastEntry

  public static final Content EMPTY_CONTENT = new ContentImpl();

  public static final Comparator<LastEntry> LASTENTRY_COMPARATOR = new Comparator<LastEntry>() {

    public int compare(LastEntry o1, LastEntry o2) {
      return o2.compareTo(o1.m_Timestamp);
    }//compare
  };

}//class ForaManager
