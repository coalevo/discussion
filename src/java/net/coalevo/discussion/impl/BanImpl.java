/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.Ban;
import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.BaseIdentifiable;

/**
 * Class that implements a {@link Ban}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BanImpl
    extends BaseIdentifiable
    implements Ban {

  private AgentIdentifier m_Who;
  private AgentIdentifier m_ByWhom;
  private ForumIdentifier m_ForumIdentifier;
  private String m_Reason;
  private long m_Until;
  private long m_Created;
  private boolean m_Dirty;

  public BanImpl(ForumIdentifier fid, AgentIdentifier who, AgentIdentifier bywhom) {
    super(new StringBuilder().append(fid.getIdentifier()).append(who.getIdentifier()).toString());
    m_ForumIdentifier = fid;
    m_Who = who;
    m_ByWhom = bywhom;
    m_Until = -1;
    m_Created = System.currentTimeMillis();
    m_Dirty = false;
  }//constructor

  public BanImpl(ForumIdentifier fid, AgentIdentifier who, AgentIdentifier bywhom, String reason, long created, long until) {
    super(new StringBuilder().append(fid.getIdentifier()).append(who.getIdentifier()).toString());
    m_ForumIdentifier = fid;
    m_Who = who;
    m_ByWhom = bywhom;
    m_Reason = reason;
    m_Created = created;
    m_Until = until;
    m_Dirty = false;
  }//constructor


  public AgentIdentifier getWho() {
    return m_Who;
  }//getWho

  public ForumIdentifier getForumIdentifier() {
    return m_ForumIdentifier;
  }//getForumIdentifier

  public AgentIdentifier getByWhom() {
    return m_ByWhom;
  }//getByWhom

  public void setByWhom(AgentIdentifier aid) {
    m_ByWhom = aid;
    m_Dirty = true;
  }//setByWhom

  public String getReason() {
    return m_Reason;
  }//getReason

  public void setReason(String reason) {
    m_Reason = reason;
    m_Dirty = true;
  }//setReason

  public long getCreated() {
    return m_Created;
  }//getCreated

  public long getUntil() {
    return m_Until;
  }//getUtil

  public void setUntil(long timestamp) {
    m_Until = timestamp;
    m_Dirty = true;
  }//setUntil

  public boolean isActive() {
    return System.currentTimeMillis() > m_Until;
  }//isActive

  public boolean isDirty() {
    return m_Dirty;
  }//isDirty

  public void setDirty(boolean b) {
    m_Dirty = b;
  }//setDirty

}//class BanImpl
