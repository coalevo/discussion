/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.bank.service.BankService;
import net.coalevo.discussion.model.*;
import net.coalevo.discussion.service.ForumModerationService;
import net.coalevo.foundation.model.*;
import net.coalevo.security.model.NoSuchActionException;
import net.coalevo.security.model.PolicyProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.system.service.ExecutionService;
import net.coalevo.postoffice.service.PostOfficeService;
import net.coalevo.postoffice.model.EditableMessage;
import net.coalevo.postoffice.model.PostOfficeServiceException;
import net.coalevo.text.util.TagUtility;
import net.coalevo.text.util.DateFormatter;
import org.osgi.framework.BundleContext;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.List;
import java.util.WeakHashMap;

/**
 * Implements {@link ForumModerationService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ForumModerationServiceImpl
    extends BaseService
    implements ForumModerationService {

  private Marker m_LogMarker = MarkerFactory.getMarker(ForumModerationServiceImpl.class.getName());

  private DiscussionStore m_DiscussionStore;
  private Messages m_BundleMessages;

  //Service Refs
  private ServiceMediator m_Services;
  //Security Context
  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;

  //Managers
  private ForaManager m_ForaManager;
  private BanManager m_BanManager;
  private LinkManager m_LinkManager;
  private EntryManager m_EntryManager;
  private SubscriptionManager m_SubscriptionManager;

  //TransactionTrackers
  private WeakHashMap<ForumIdentifier, EditableForumDescriptor> m_UpdateForumTracker;
  private WeakHashMap<String, Ban> m_CreateBanTracker;
  private WeakHashMap<String, EditableLink> m_UpdateLinkTracker;
  private WeakHashMap<ForumIdentifier, EditableContent> m_UpdateForumInfoTracker;

  public ForumModerationServiceImpl(DiscussionStore ds) {
    super(ForumModerationService.class.getName(), ACTIONS);
    m_DiscussionStore = ds;
  }//constructor

  public boolean activate(BundleContext bc) {
    m_Services = Activator.getServices();
    m_BundleMessages = Activator.getBundleMessages();

    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/ForumModerationService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bc);

    //3. References
    m_ForaManager = m_DiscussionStore.getForaManager();
    m_BanManager = m_DiscussionStore.getBanManager();
    m_SubscriptionManager = m_DiscussionStore.getSubscriptionManager();
    m_LinkManager = m_DiscussionStore.getLinkManager();
    m_EntryManager = m_DiscussionStore.getEntryManager();

    //4. Transaction Trackers
    m_UpdateForumTracker = new WeakHashMap<ForumIdentifier, EditableForumDescriptor>(10);
    m_CreateBanTracker = new WeakHashMap<String, Ban>(10);
    m_UpdateLinkTracker = new WeakHashMap<String, EditableLink>(10);
    m_UpdateForumInfoTracker = new WeakHashMap<ForumIdentifier, EditableContent>(10);

    return true;
  }//activate

  public boolean deactivate() {

    if (m_PolicyProxy != null) {
      m_PolicyProxy.deactivate();
      m_PolicyProxy = null;
    }
    if (m_ServiceAgentProxy != null) {
      m_ServiceAgentProxy.deactivate();
      m_ServiceAgentProxy = null;
    }

    //Managers
    m_ForaManager = null;
    m_BanManager = null;
    m_SubscriptionManager = null;
    m_LinkManager = null;
    m_EntryManager = null;

    //Trackers
    m_UpdateForumTracker = null;
    m_CreateBanTracker = null;
    m_UpdateLinkTracker = null;
    m_UpdateForumInfoTracker = null;


    m_Services = null;
    return true;
  }//deactivate

  //*** Forum Handling ***//

  public EditableForumDescriptor beginForumUpdate(Agent caller, ForumIdentifier fid)
      throws SecurityException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_FORUM);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_FORUM.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    if (m_UpdateForumTracker.containsKey(fid)) {
      throw new IllegalStateException();
    }

    EditableForumDescriptorImpl efd =
        ((ForumDescriptorImpl) m_ForaManager.get(fid)).toEditableForumDescriptor();

    //Track
    m_UpdateForumTracker.put(efd.getIdentifier(), efd);

    return efd;
  }//beginForumUpdate

  public void commitForumUpdate(Agent caller, EditableForumDescriptor efd)
      throws SecurityException, IllegalStateException, DiscussionServiceException {

    if (!m_ForaManager.isModerator(efd.getIdentifier(), caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_FORUM);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_FORUM.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    if (!m_UpdateForumTracker.containsKey(efd.getIdentifier())) {
      throw new IllegalStateException();
    }
    m_ForaManager.updateForumDescriptor(efd);

    //Remove from tracker
    m_UpdateForumTracker.remove(efd.getIdentifier());
  }//commitForumUpdate

  public void cancelForumUpdate(Agent caller, EditableForumDescriptor efd)
      throws SecurityException {

    if (!m_ForaManager.isModerator(efd.getIdentifier(), caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_FORUM);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_FORUM.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    //Remove from tracker
    m_UpdateForumTracker.remove(efd.getIdentifier());
  }//cancelForumUpdate

  public EditableContent beginForumInfoUpdate(Agent caller, ForumIdentifier fid)
      throws SecurityException, IllegalStateException, DiscussionServiceException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_FORUM_INFO);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_FORUM_INFO.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    if (m_UpdateForumInfoTracker.containsKey(fid)) {
      throw new IllegalStateException();
    }

    EditableContentImpl ec = null;
    Content c = m_ForaManager.getForumInfo(fid);
    if (c == null || c == ForaManager.EMPTY_CONTENT) {
      ec = new EditableContentImpl();
    } else {
      ec = ((ContentImpl) c).toEditableContent();
    }

    m_UpdateForumInfoTracker.put(fid, ec);

    return ec;
  }//beginForumInfoUpdate


  public void commitForumInfoUpdate(Agent caller, ForumIdentifier fid, EditableContent ec)
      throws SecurityException, IllegalStateException, DiscussionServiceException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_FORUM_INFO);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_FORUM_INFO.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    if (!m_UpdateForumInfoTracker.containsKey(fid)) {
      throw new IllegalStateException();
    }

    m_ForaManager.updateForumInfo(fid, ec);

    m_UpdateForumInfoTracker.remove(fid);
  }//commitForumInfoUpdate

  public void cancelForumInfoUpdate(Agent caller, ForumIdentifier fid)
      throws SecurityException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_FORUM_INFO);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_FORUM_INFO.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    m_UpdateForumInfoTracker.remove(fid);
  }//cancelForumInfoUpdate

  //*** END Forum Handling ***//

  //*** Ban Handling ***//

  public Ban beginBanCreate(Agent caller,
                            ForumIdentifier fid,
                            AgentIdentifier aid)
      throws SecurityException, BannedException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_BAN);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_BAN.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    //Ban check
    if (m_BanManager.isBanned(fid, aid)) {
      throw new BannedException();
    }

    BanImpl s = new BanImpl(fid, aid, caller.getAgentIdentifier());

    //track
    m_CreateBanTracker.put(s.getIdentifier(), s);

    return s;
  }//beginBanCreate

  public void commitBan(Agent caller, Ban b)
      throws SecurityException, IllegalStateException, DiscussionServiceException {

    if (!m_ForaManager.isModerator(b.getForumIdentifier(), caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_BAN);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_BAN.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    if (!m_CreateBanTracker.containsKey(b.getIdentifier())) {
      throw new IllegalStateException();
    }

    m_BanManager.create(b);
    //unsubscribe? 
    m_SubscriptionManager.cancel(b.getForumIdentifier(), b.getWho());

    //if finished remove from tracker
    m_CreateBanTracker.remove(b.getIdentifier());
  }//commitBan

  public void cancelBanCreate(Agent caller, Ban b)
      throws SecurityException {

    if (!m_ForaManager.isModerator(b.getForumIdentifier(), caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_BAN);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_BAN.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    m_CreateBanTracker.remove(b.getIdentifier());
  }//cancelForumInfoUpdate


  public Ban getBan(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException, DiscussionServiceException, NoSuchBanException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_BAN);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_BAN.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    return m_BanManager.getBan(fid, aid);
  }//getBan

  public void removeBan(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException, DiscussionServiceException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), REMOVE_BAN);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", REMOVE_BAN.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    m_BanManager.remove(fid, aid);
  }//cancelBan

  public boolean isBanned(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CHECK_BAN);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CHECK_BAN.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    return m_BanManager.isBanned(fid, aid);
  }//isBanned

  public List<AgentIdentifier> listBanned(Agent caller, ForumIdentifier fid)
      throws SecurityException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_BANNED);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_BANNED.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    return m_BanManager.listBanned(fid);
  }//listBanned

  //*** END Ban Handling ***//

  //*** Link Handling ***//

  public EditableLink beginLinkUpdate(Agent caller, ForumIdentifier fid, String id)
      throws SecurityException, DiscussionServiceException {

    EditableLinkImpl l = m_LinkManager.get(fid, id).toEditableLink();
    if (!m_ForaManager.isModerator(l.getForumIdentifier(), caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_LINK);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_LINK.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    m_UpdateLinkTracker.put(l.getIdentifier(), l);

    return l;
  }//beginLinkUpdate

  public void cancelLinkUpdate(Agent caller, EditableLink el)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_LINK);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_LINK.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //Remove tracked link
    if (m_UpdateLinkTracker.containsKey(el.getIdentifier())) {
      m_UpdateLinkTracker.remove(el.getIdentifier());
    }
  }//beginLinkUpdate

  public void commitLinkUpdate(Agent caller, EditableLink link)
      throws SecurityException, IllegalStateException, DiscussionServiceException {
    if (!m_ForaManager.isModerator(link.getForumIdentifier(), caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_LINK);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_LINK.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    if (!m_UpdateLinkTracker.containsKey(link.getIdentifier())) {
      throw new IllegalStateException();
    }
    EditableLinkImpl el = (EditableLinkImpl) link;
    el.setModified();
    m_LinkManager.update(el);

    m_UpdateLinkTracker.remove(link.getIdentifier());
  }//commitLinkUpdate

  public List<String> listPendingLinks(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_PENDING_LINKS);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_PENDING_LINKS.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    return m_LinkManager.listPendingLinks(fid);
  }//listPendingLinks

  public void approveLink(Agent caller, ForumIdentifier fid, String linkid, boolean notify)
      throws SecurityException, DiscussionServiceException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), APPROVE_LINK);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", APPROVE_LINK.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    m_LinkManager.approveLink(fid, linkid);
  }//approveLink

  public void disapproveLink(Agent caller, ForumIdentifier fid, String linkid, boolean notify)
      throws SecurityException, DiscussionServiceException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), DISAPPROVE_LINK);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", DISAPPROVE_LINK.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    Link l = m_LinkManager.get(fid,linkid);
    m_LinkManager.disapproveLink(fid, linkid);
    notifyLinkDisapproval(caller.getAgentIdentifier(), l);
  }//disapproveLink

  public void moveLink(Agent caller, ForumIdentifier from,
                       ForumIdentifier to, String id)
      throws SecurityException, DiscussionServiceException {

    if (!m_ForaManager.isModerator(from, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), MOVE_LINK);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", MOVE_LINK.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    m_LinkManager.moveLink(from, to, id);
  }//moveLink

  public void removeLink(Agent caller, ForumIdentifier fid, String id)
      throws SecurityException, DiscussionServiceException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), REMOVE_LINK);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", REMOVE_LINK.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }

    m_LinkManager.remove(fid, id);
  }//removeLink

  public void checkLinks(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException {
    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CHECK_LINK);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CHECK_LINK.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    m_LinkManager.checkLinks(fid);
  }//checkLinks

  //*** END Link Handling ***/

  public List<String> listPendingEntries(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_PENDING_ENTRIES);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", LIST_PENDING_ENTRIES.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    return m_EntryManager.listPendingEntries(fid);
  }//listPendingEntries

  public void approveEntry(Agent caller, ForumIdentifier fid, String eid, boolean notify)
      throws SecurityException, DiscussionServiceException {
    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), APPROVE_ENTRY);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", APPROVE_ENTRY.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    m_EntryManager.approveEntry(fid, eid);
    award(m_EntryManager.getAuthor(fid, eid), 1);
    //Notification
    m_DiscussionStore.getNotificationManager().notifySubscribers(
        m_EntryManager.getDescriptor(fid, eid), true);
  }//approveEntry

  public void disapproveEntry(Agent caller, ForumIdentifier fid, String eid, boolean notify)
      throws SecurityException, DiscussionServiceException {

    if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), DISAPPROVE_ENTRY);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", DISAPPROVE_ENTRY.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    EntryDescriptor ed = m_EntryManager.getDescriptor(fid,eid);
    m_EntryManager.disapproveEntry(fid, eid);
    notifyEntryDisapproval(caller.getAgentIdentifier(),ed);
  }//disapproveEntry

  public void moveEntry(Agent caller, ForumIdentifier from, ForumIdentifier to, String id)
      throws SecurityException, DiscussionServiceException {
    if (!m_ForaManager.isModerator(from, caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), MOVE_ENTRY);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", MOVE_ENTRY.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    m_EntryManager.moveEntry(from, to, id);
  }//moveEntry

  public void removeEntry(Agent caller, EntryDescriptor ed, boolean notify)
      throws SecurityException, DiscussionServiceException {
    if (!m_ForaManager.isModerator(ed.getForumIdentifier(), caller.getAgentIdentifier())) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), REMOVE_ENTRY);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", REMOVE_ENTRY.getIdentifier(), "caller", caller.getIdentifier()), ex);
      }
    }
    m_EntryManager.remove(ed.getForumIdentifier(), ed.getIdentifier());
  }//removeEntry

  public boolean isAllowedToModerate(Agent caller, ForumIdentifier fid) {
    try {
      if (!m_ForaManager.isModerator(fid, caller.getAgentIdentifier())) {
        try {
          m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), MODERATE);
        } catch (NoSuchActionException ex) {
          Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", MODERATE.getIdentifier(), "caller", caller.getIdentifier()), ex);
        }
      }
      return true;
    } catch (SecurityException ex) {
      return false;
    }
  }//isAllowedToModerate

  protected void award(AgentIdentifier aid, long amount) {
    if (aid == null) {
      return;
    }
    BankService bank = m_Services.getBankService(ServiceMediator.NO_WAIT);
    try {
      if (bank.existsAccount(m_ServiceAgentProxy.getAuthenticPeer(), aid)) {
        bank.deposit(m_ServiceAgentProxy.getAuthenticPeer(), aid, amount);
        Activator.log().info(m_LogMarker, m_BundleMessages.get("DiscussionServiceImpl.award", "sum", "" + amount, "aid", aid.getIdentifier()));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "award", ex);
    }
  }//award

  protected void notifyEntryDisapproval(final AgentIdentifier who, final EntryDescriptor ed) {
    ExecutionService es = m_Services.getExecutionService(ServiceMediator.NO_WAIT);
    final PostOfficeService ps = m_Services.getPostOfficeService(ServiceMediator.NO_WAIT);

    if(es != null && ps != null) {
      es.execute(m_ServiceAgentProxy.getAuthenticPeer(),new Runnable() {
        public void run() {
          try {
            //1. Forum
            ForumDescriptor fd = m_ForaManager.get(ed.getForumIdentifier());
            //1. Prepare subject and body
            String subject = m_BundleMessages.get(
                "ForumModerationServiceImpl.entrydisapproval.subject",
                "forumname", fd.getName(),
                "forumseqn", Integer.toString(fd.getSequenceNumber()));
            MessageAttributes attr = m_BundleMessages.leaseAttributes();
            attr.add("forumname", fd.getName());
            attr.add("forumseqn", Integer.toString(fd.getSequenceNumber()));
            attr.add("msgid", ed.getIdentifier());
            attr.add("who", who.getIdentifier());
            String tmp = ed.getCaption();
            if(tmp != null && tmp.length() > 0) {
              attr.add("caption", tmp);
            }
            tmp = TagUtility.fromSet(ed.getTags());
            if(tmp != null && tmp.length() > 0) {
              attr.add("tags", tmp);
            }
            tmp = DateFormatter.getISOInstance().format(ed.getCreated());
            attr.add("created", tmp);
            String body = m_BundleMessages.get("ForumModerationServiceImpl.entrydisapproval.body",attr);

            //2. Deposit Message
            EditableMessage msg = ps.beginMessage(m_ServiceAgentProxy.getAuthenticPeer(), ed.getAuthor());
            msg.setSubject(subject);
            msg.setBodyFormat("");
            msg.getMessageBody().setContent(body);
            ps.depositMessage(m_ServiceAgentProxy.getAuthenticPeer(),msg);
          } catch (Exception ex) {
            Activator.log().error("notifyEntryDisapproval()",ex);
          }
        }//run
      });
    }
  }//notifyEntryDisapproval

  protected void notifyLinkDisapproval(final AgentIdentifier who, final Link l) {
    ExecutionService es = m_Services.getExecutionService(ServiceMediator.NO_WAIT);
    final PostOfficeService ps = m_Services.getPostOfficeService(ServiceMediator.NO_WAIT);

    if(es != null && ps != null) {
      es.execute(m_ServiceAgentProxy.getAuthenticPeer(),new Runnable() {
        public void run() {
          try {
            //1. Forum
            ForumDescriptor fd = m_ForaManager.get(l.getForumIdentifier());
            //1. Prepare subject and body
            String subject = m_BundleMessages.get(
                "ForumModerationServiceImpl.linkdisapproval.subject",
                "forumname", fd.getName(),
                "forumseqn", Integer.toString(fd.getSequenceNumber()));
            MessageAttributes attr = m_BundleMessages.leaseAttributes();
            attr.add("forumname", fd.getName());
            attr.add("forumseqn", Integer.toString(fd.getSequenceNumber()));
            attr.add("linkid", l.getIdentifier());
            attr.add("who", who.getIdentifier());
            String tmp = l.getCaption();
            if(tmp != null && tmp.length() > 0) {
              attr.add("caption", tmp);
            }
            tmp = TagUtility.fromSet(l.getTags());
            if(tmp != null && tmp.length() > 0) {
              attr.add("tags", tmp);
            }
            tmp = DateFormatter.getISOInstance().format(l.getCreated());
            attr.add("created", tmp);
            String body = m_BundleMessages.get("ForumModerationServiceImpl.linkdisapproval.body",attr);

            //2. Deposit Message
            EditableMessage msg = ps.beginMessage(m_ServiceAgentProxy.getAuthenticPeer(), l.getAuthor());
            msg.setSubject(subject);
            msg.getMessageBody().setContent(body);
            ps.depositMessage(m_ServiceAgentProxy.getAuthenticPeer(),msg);
          } catch (PostOfficeServiceException ex) {
            Activator.log().error("notifyLinkDisapproval()",ex);
          }
        }//run
      });
    }
  }//notifyEntryDisapproval

  private static Action UPDATE_FORUM = new Action("updateForum");
  private static Action UPDATE_FORUM_INFO = new Action("updateForumInfo");

  private static Action CREATE_BAN = new Action("createBan");
  private static Action GET_BAN = new Action("getBan");
  private static Action REMOVE_BAN = new Action("removeBan");
  private static Action CHECK_BAN = new Action("checkBan");
  private static Action LIST_BANNED = new Action("listBanned");

  private static Action UPDATE_LINK = new Action("updateLink");
  private static Action LIST_PENDING_LINKS = new Action("listPendingLinks");
  private static Action APPROVE_LINK = new Action("approveLink");
  private static Action DISAPPROVE_LINK = new Action("disapproveLink");
  private static Action MOVE_LINK = new Action("moveLink");
  private static Action REMOVE_LINK = new Action("removeLink");
  private static Action CHECK_LINK = new Action("checkLink");


  private static Action LIST_PENDING_ENTRIES = new Action("listPendingEntries");
  private static Action APPROVE_ENTRY = new Action("approveEntry");
  private static Action DISAPPROVE_ENTRY = new Action("disapproveEntry");
  private static Action MOVE_ENTRY = new Action("moveEntry");
  private static Action REMOVE_ENTRY = new Action("removeEntry");

  private static Action MODERATE = new Action("moderate");

  private static Action[] ACTIONS = {
      UPDATE_FORUM,
      UPDATE_FORUM_INFO,
      CREATE_BAN,
      GET_BAN,
      REMOVE_BAN,
      CHECK_BAN,
      LIST_BANNED,
      UPDATE_LINK,
      LIST_PENDING_LINKS,
      APPROVE_LINK,
      DISAPPROVE_LINK,
      MOVE_LINK,
      CHECK_LINK,
      REMOVE_LINK,
      LIST_PENDING_ENTRIES,
      APPROVE_ENTRY,
      DISAPPROVE_ENTRY,
      MOVE_ENTRY,
      REMOVE_ENTRY,
      MODERATE
  };

}//class ForumModerationServiceImpl
