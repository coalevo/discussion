/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.discussion.model.Link;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.BaseIdentifiable;

import java.util.Set;

/**
 * Implements {@link Link}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class LinkImpl
    extends BaseIdentifiable
    implements Link {

  protected ForumIdentifier m_ForumIdentifier;
  protected AgentIdentifier m_Author;
  protected String m_Caption;
  protected String m_DescriptionFormat;
  protected String m_Description;
  protected String m_Link;
  protected Set<String> m_Tags; //should be set non-modifiable!
  protected boolean m_Approved;
  protected boolean m_Functional;
  protected long m_Created;
  protected long m_Modified;
  protected long m_LastChecked;

  public LinkImpl(ForumIdentifier fid, String id, AgentIdentifier author,
                  String caption, String descformat,
                  String description, String link,
                  Set<String> tags, boolean approved,
                  boolean functional, long created,
                  long modified, long lastchecked) {
    super(id);
    m_ForumIdentifier = fid;
    m_Author = author;
    m_Caption = caption;
    m_DescriptionFormat = descformat;
    m_Description = description;
    m_Link = link;
    m_Tags = tags;
    m_Approved = approved;
    m_Functional = functional;
    m_Created = created;
    m_Modified = modified;
    m_LastChecked = lastchecked;
  }//constructor

  public LinkImpl(String id) {
    super(id);
  }//constructor

  public ForumIdentifier getForumIdentifier() {
    return m_ForumIdentifier;
  }//getForumIdentifier

  public AgentIdentifier getAuthor() {
    return m_Author;
  }//getAuthor

  public String getCaption() {
    return m_Caption;
  }//getCaption

  public String getDescriptionFormat() {
    return m_DescriptionFormat;
  }//getDescriptionFormat

  public String getDescription() {
    return m_Description;
  }//getDescription

  public String getLink() {
    return m_Link;
  }//get

  public Set<String> getTags() {
    return m_Tags;
  }//getTags

  public boolean isApproved() {
    return m_Approved;
  }//isApproved

  public boolean isFunctional() {
    return m_Functional;
  }//isFunctional

  public long getCreated() {
    return m_Created;
  }//getCreated

  public long getModified() {
    return m_Modified;
  }//getModified

  public long getLastChecked() {
    return m_LastChecked;
  }//getLastChecked

  public EditableLinkImpl toEditableLink() {
    return new EditableLinkImpl(
        this.m_ForumIdentifier,
        this.m_Identifier,
        this.m_Author,
        this.m_Caption,
        this.m_DescriptionFormat,
        this.m_Description,
        this.m_Link,
        this.m_Tags,
        this.m_Approved,
        this.m_Functional,
        this.m_Created,
        this.m_Modified,
        this.m_LastChecked
    );
  }//toLink

}//class LinkImpl
