/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.EntryDescriptor;
import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.BaseIdentifiable;

import java.util.Set;

/**
 * Implements {@link EntryDescriptor}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class EntryDescriptorImpl
    extends BaseIdentifiable
    implements EntryDescriptor {

  protected ForumIdentifier m_ForumIdentifier;
  protected AgentIdentifier m_Author;
  protected String m_Ancestor;
  protected String m_Caption;
  protected String m_ContentFormat;
  protected Set<String> m_Tags;
  protected long m_Created;
  protected long m_Approved;
  protected boolean m_Official;
  protected boolean m_DescendantAllowed;
  protected boolean m_Archived;

  protected EntryDescriptorImpl(ForumIdentifier fid, String id) {
    super(id);
    m_ForumIdentifier = fid;
  }//constructor

  public EntryDescriptorImpl(ForumIdentifier fid, String id, String ancestor,
                             AgentIdentifier author, String caption,
                             Set<String> tags, String contentformat,
                             long created, long approved,
                             boolean official, boolean descendants,
                             boolean archived) {
    super(id);
    m_ForumIdentifier = fid;
    m_Ancestor = ancestor;
    m_Author = author;
    m_Caption = caption;
    m_Tags = tags;
    m_ContentFormat = contentformat;
    m_Created = created;
    m_Approved = approved;
    m_Official = official;
    m_DescendantAllowed = descendants;
    m_Archived = archived;
  }//EntryDescriptorImpl

  public ForumIdentifier getForumIdentifier() {
    return m_ForumIdentifier;
  }//getForumIdentifier

  public String getAncestor() {
    return m_Ancestor;
  }//getAncestor

  public AgentIdentifier getAuthor() {
    return m_Author;
  }//getAuthor

  public String getCaption() {
    return m_Caption;
  }//getCaption

  public String getContentFormat() {
    return m_ContentFormat;
  }//getContentFormat

  public Set<String> getTags() {
    return m_Tags;
  }//getTags

  public long getCreated() {
    return m_Created;
  }//getCreated

  public long getApproved() {
    return m_Approved;
  }//getApproved

  public boolean isApproved() {
    return m_Approved > 0;
  }//isApproved

  public boolean isDescendant() {
    return m_Ancestor != null;
  }//isDescendant

  public boolean isDescendantAllowed() {
    return m_DescendantAllowed;
  }//isDescendantAllowed

  public boolean isOfficial() {
    return m_Official;
  }//isOfficial

  public boolean isArchived() {
    return m_Archived;
  }//isArchived

  public void setApproved(boolean b) {
    if (b) {
      m_Approved = System.currentTimeMillis();
    } else {
      m_Approved = -1;
    }
  }//setApproved

  public void setOfficial(boolean b) {
    m_Official = b;
  }//setOfficial

  public void setDescendantAllowed(boolean b) {
    m_DescendantAllowed = b;
  }//setDescendantAllowed
  
}//class EntryDescriptorImpl
