/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.Link;
import net.coalevo.discussion.model.DiscussionTokens;
import net.coalevo.discussion.service.DiscussionXMLService;
import net.coalevo.foundation.model.BaseService;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.stax.service.StAXFactoryService;
import net.coalevo.text.util.DateFormatter;
import net.coalevo.text.util.TagUtility;
import net.coalevo.text.service.TransformationService;
import net.coalevo.text.model.TransformationException;
import org.osgi.framework.BundleContext;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;

/**
 * This class implements the {@link DiscussionXMLService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class DiscussionXMLServiceImpl
    extends BaseService
    implements DiscussionXMLService {

  private Marker m_LogMarker = MarkerFactory.getMarker(DiscussionXMLServiceImpl.class.getName());

  private XMLInputFactory m_XMLInputFactory;
  private XMLOutputFactory m_XMLOutputFactory;
  private ServiceMediator m_Services;
  private DateFormatter m_XBELDateFormatter;

  public DiscussionXMLServiceImpl() {
    super(DiscussionXMLServiceImpl.class.getName(), null);
  }//constructor

  public boolean activate(BundleContext bc) {
    m_Services = Activator.getServices();
    m_XBELDateFormatter = new DateFormatter("yyyy-MM-dd'T'hh:mm:ss'Z'","UTC",5);
    try {
      StAXFactoryService stax = m_Services.getStAXFactoryService(ServiceMediator.WAIT_UNLIMITED);
      m_XMLInputFactory = stax.getXMLInputFactory();
      m_XMLOutputFactory = stax.getXMLOutputFactory();
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"activate()", ex);
    }

    return true;
  }//activate

  public boolean deactivate() {
    m_XMLInputFactory = null;
    m_XMLOutputFactory = null;
    return true;
  }//deactivate

  //**** Marshalling and Unmarshallig Links (XBEL mainly) ****//

  public void toXML(Writer w, Link l)
      throws IOException {
     try {
      toXML(forOutput(w), l);
    } catch (XMLStreamException ex) {
      throw (IOException) new IOException().initCause(ex);
    }
  }//toXML

  public void toXML(OutputStream out, Link l) throws IOException {
     try {
      toXML(forOutput(out), l);
    } catch (XMLStreamException ex) {
      throw (IOException) new IOException().initCause(ex);
    }
  }//toXML

  public String toXML(Link l) throws IOException {
    final StringWriter w = new StringWriter();
    try {
      toXML(forOutput(w), l);
      return w.toString();
    } catch (XMLStreamException ex) {
      throw (IOException) new IOException().initCause(ex);
    }
  }//toXML

  public void toXML() 
      throws IOException {

  }//toXML

  /**
   * This is the actual place where the action happens.
   * @param w a Stax <tt>XMLStreamWriter</tt>.
   * @param l the link to be written
   * @throws XMLStreamException if writing the link fails.
   */
  private void toXML(XMLStreamWriter w, Link l)
      throws XMLStreamException {

    TransformationService transforms = m_Services.getTransformationService(
        ServiceMediator.NO_WAIT);
    w.writeStartElement(DiscussionTokens.ELEMENT_XBEL_BOOKMARK);

    //1. Attributes
    w.writeAttribute(DiscussionTokens.ATTR_ID,l.getIdentifier());
    w.writeAttribute(DiscussionTokens.ATTR_XBEL_HREF,l.getLink());

    //todo: check dates, if -1 probably don't add it
    w.writeAttribute(DiscussionTokens.ATTR_XBEL_ADDED,m_XBELDateFormatter.format(l.getCreated()));
    w.writeAttribute(DiscussionTokens.ATTR_XBEL_VISITED,m_XBELDateFormatter.format(l.getLastChecked()));
    w.writeAttribute(DiscussionTokens.ATTR_XBEL_MODIFIED,m_XBELDateFormatter.format(l.getModified()));

    //2. Title
    w.writeStartElement(DiscussionTokens.ELEMENT_XBEL_TITLE);
    w.writeCharacters(l.getCaption());
    w.writeEndElement();

    //3. Info, add metadata that belongs to coalevo bookmarks
    w.writeStartElement(DiscussionTokens.ELEMENT_XBEL_INFO);
    w.writeStartElement(DiscussionTokens.ELEMENT_XBEL_METADATA);
    w.writeAttribute(DiscussionTokens.ATTR_XBEL_OWNER,DiscussionTokens.ATTR_XBEL_OWNER_VALUE);
    w.writeStartElement(DiscussionTokens.ELEMENT_XBEL_THISMETA_NS,
        DiscussionTokens.ELEMENT_XBEL_THISMETA);
    w.writeAttribute(DiscussionTokens.ATTR_FORUM,l.getForumIdentifier().getIdentifier());
    w.writeAttribute(DiscussionTokens.ATTR_AUTHOR,l.getAuthor().getIdentifier());
    w.writeAttribute(DiscussionTokens.ATTR_TAGS, TagUtility.fromSet(l.getTags()));

    w.writeEndElement(); // THIS META
    w.writeEndElement(); // METADATA
    w.writeEndElement(); //INFO

    //4. Description
    w.writeStartElement(DiscussionTokens.ELEMENT_XBEL_DESC);
    try {
      w.writeCharacters(transforms.transform(l.getDescriptionFormat(),"plain",l.getDescription()));
    } catch (TransformationException e) {
      Activator.log().error(m_LogMarker,"toXML()", e);
      w.writeCharacters(l.getDescription());
    }
    w.writeEndElement();
  }//toXML


  //**** END: Marshalling and Unmarshallig Links (XBEL mainly) ****//


  private final XMLStreamWriter forOutput(Writer output)
      throws XMLStreamException {
    return m_XMLOutputFactory.createXMLStreamWriter(output);
  }//forOutput

  private final XMLStreamWriter forOutput(OutputStream output)
      throws XMLStreamException {
    return m_XMLOutputFactory.createXMLStreamWriter(output);
  }//forOutput

}//class DiscussionXMLServiceImpl
