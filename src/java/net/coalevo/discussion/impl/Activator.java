/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.service.DiscussionConfiguration;
import net.coalevo.discussion.service.DiscussionService;
import net.coalevo.discussion.service.DiscussionXMLService;
import net.coalevo.discussion.service.ForumModerationService;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.model.Maintainable;
import net.coalevo.foundation.util.BundleConfiguration;
import net.coalevo.foundation.util.DummyMessages;
import net.coalevo.logging.model.LogProxy;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.File;

/**
 * Provides a <tt>BundleActivator</tt> implementation for
 * the discussion bundle.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {

  private static Marker c_LogMarker;
  private static LogProxy c_Log;
  
  private static Messages c_BundleMessages = new DummyMessages();
  private static ServiceMediator c_Services;
  private static DiscussionStore c_DiscussionStore;
  private static DiscussionService c_DiscussionService;
  private static DiscussionXMLService c_DiscussionXMLService;
  private static ForumModerationService c_ForumModerationService;
  private static File c_IndexStore;

  private static BundleConfiguration c_BundleConfiguration;
  private Thread m_StartThread;

  public void start(final BundleContext bundleContext)
      throws Exception {
    if(m_StartThread!=null && m_StartThread.isAlive()) {
      throw new Exception();
    }    
    m_StartThread = new Thread(
        new Runnable() {
          public void run() {
            try {
              //1. Log
             c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
             c_Log = new LogProxy();
             c_Log.activate(bundleContext);

              //2. Services
              c_Services = new ServiceMediator();
              c_Services.activate(bundleContext);

              //3 Bundle Messages
              c_BundleMessages =
                  c_Services.getMessageResourceService(ServiceMediator.WAIT_UNLIMITED)
                      .getBundleMessages(bundleContext.getBundle());

              //4. Bundle Configuration
              c_BundleConfiguration = new BundleConfiguration(DiscussionConfiguration.class.getName());
              c_BundleConfiguration.activate(bundleContext);
              c_Services.setConfigMediator(c_BundleConfiguration.getConfigurationMediator());
              c_IndexStore = new File(bundleContext.getDataFile(""), "didx_store");

              //5. Discussion Store
              c_DiscussionStore = new DiscussionStore();
              c_DiscussionStore.activate(bundleContext);
              log().debug(c_LogMarker,c_BundleMessages.get("Activator.activation.store"));

              //6. Discussion Services
              c_DiscussionService = new DiscussionServiceImpl(c_DiscussionStore);
              if (!c_DiscussionService.activate(bundleContext)) {
                log().error(c_LogMarker,c_BundleMessages.get("Activator.activation.exception", "service", "DiscussionService"));
              }
              String[] classes = {DiscussionService.class.getName(), Maintainable.class.getName()};
              bundleContext.registerService(
                  classes,
                  c_DiscussionService,
                  null);
              log().debug(c_LogMarker,c_BundleMessages.get("Activator.activation.service", "service", "DiscussionService"));

              c_DiscussionXMLService = new DiscussionXMLServiceImpl();
              if (!c_DiscussionXMLService.activate(bundleContext)) {
                log().error(c_LogMarker,c_BundleMessages.get("Activator.activation.exception", "service", "DiscussionXMLService"));
              }
              bundleContext.registerService(
                  DiscussionXMLService.class.getName(),
                  c_DiscussionXMLService,
                  null);
              log().debug(c_LogMarker,c_BundleMessages.get("Activator.activation.service", "service", "DiscussionXMLService"));

              c_ForumModerationService = new ForumModerationServiceImpl(c_DiscussionStore);
              if (!c_ForumModerationService.activate(bundleContext)) {
                log().error(c_LogMarker,c_BundleMessages.get("Activator.activation.exception", "service", "ForumModerationService"));
              }
              bundleContext.registerService(
                  ForumModerationService.class.getName(),
                  c_ForumModerationService,
                  null);
              log().debug(c_LogMarker,c_BundleMessages.get("Activator.activation.service", "service", "ForumModerationService"));
            } catch (Exception ex) {
              log().error(c_LogMarker,"start(BundleContext)",ex);
            }
          }//run
        }//Runnable
    );//Thread
    m_StartThread.setContextClassLoader(this.getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext)
      throws Exception {

    //wait start
    if(m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }

    if (c_ForumModerationService != null) {
      c_ForumModerationService.deactivate();
      c_ForumModerationService = null;
    }
    if (c_DiscussionService != null) {
      c_DiscussionService.deactivate();
      c_DiscussionService = null;
    }
    if (c_DiscussionXMLService != null) {
      c_DiscussionXMLService.deactivate();
      c_DiscussionXMLService = null;
    }
    if (c_DiscussionStore != null) {
      c_DiscussionStore.deactivate();
      c_DiscussionStore = null;
    }
    if (c_BundleConfiguration != null) {
      c_BundleConfiguration.deactivate();
      c_BundleConfiguration = null;
    }

    if (c_Services != null) {
      c_Services.deactivate();
      c_Services = null;
    }

    if(c_Log != null) {
      c_Log.deactivate();
      c_Log = null;
    }

    c_LogMarker = null;
    c_BundleMessages = null;
  }//stop

  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  public static Messages getBundleMessages() {
    return c_BundleMessages;
  }//getBundleMessages

  public static File getIndexDir(String indexname) {
    return new File(c_IndexStore, indexname);
  }//getIndexDir

    /**
   * Return the bundles logger.
   *
   * @return the <tt>Logger</tt>.
   */
  public static Logger log() {
    return c_Log;
  }//log

}//class Activator
