/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.EditableContent;
import net.coalevo.discussion.model.EditableEntry;
import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.foundation.model.AgentIdentifier;

import java.util.Set;

/**
 * Implements an {@link EditableEntry}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class EditableEntryImpl
    extends EntryDescriptorImpl
    implements EditableEntry {

  private EditableContent m_Content;

  public EditableEntryImpl(ForumIdentifier fid, String id, AgentIdentifier author) {
    super(fid, id);
    m_ForumIdentifier = fid;
    m_Author = author;
    m_Created = System.currentTimeMillis();
    m_Approved = -1;
    m_Official = false;
    m_Ancestor = null;
    m_Archived = false;
  }//constructor

  public void setAncestor(String anc) {
    m_Ancestor = anc;
  }//setAncestor

  public void setCaption(String str) {
    m_Caption = str;
  }//setCaption

  public void setContentFormat(String contentFormat) {
    m_ContentFormat = contentFormat;
  }//setContentFormat

  public void addTag(String tag) {
    m_Tags.add(tag);
  }//addTag

  public void removeTag(String tag) {
    m_Tags.remove(tag);
  }//removeTag

  public void setTags(Set<String> tags) {
    m_Tags = tags;
  }//setTags

  public EditableContent getContent() {
    if (m_Content == null) {
      m_Content = new EditableContentImpl();
    }
    return m_Content;
  }//getContent

  public EntryDescriptorImpl toEntryDescriptor() {
    return new EntryDescriptorImpl(
        m_ForumIdentifier,
        m_Identifier,
        m_Ancestor,
        m_Author,
        m_Caption,
        m_Tags,
        m_ContentFormat,
        m_Created,
        m_Approved,
        m_Official,
        m_DescendantAllowed,
        m_Archived
    );
  }//toEntryDescriptor

}//class EditableEntryImpl
