/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.datasource.service.DataSourceService;
import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.discussion.service.DiscussionConfiguration;
import net.coalevo.foundation.model.*;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.osgi.framework.BundleContext;
import org.slamb.axamol.library.Library;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Collections;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Implements an RDBMS based store for discussion data.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class DiscussionStore
    implements ConfigurationUpdateHandler {

  private static Marker c_LogMarker = MarkerFactory.getMarker(DiscussionStore.class.getName());
  private Library m_SQLLibrary;
  private DataSource m_DataSource;
  private GenericObjectPool m_ConnectionPool;
  private boolean m_New = false;

  //Concurrency handling for backup/restore
  private AtomicInteger m_NumLeased = new AtomicInteger(0);
  private CountDownLatch m_LeaseLatch;
  private CountDownLatch m_RestoreLatch;

  private Messages m_BundleMessages;

  //Managers
  private ForaManager m_ForaManager;
  private SubscriptionManager m_SubscriptionManager;
  private BanManager m_BanManager;
  private LinkManager m_LinkManager;
  private EntryManager m_EntryManager;
  private NotificationManager m_NotificationManager;

  //Caches
  private LRUCacheMap<String, ForumIdentifier> m_ForumIdentifierCache;
  private AgentIdentifierInstanceCache m_AgentIdentifierCache;

  public DiscussionStore() {

  }//constructor

  public boolean isNew() {
    return m_New;
  }//isNew

  /**
   * Lease a connection to the underlying database.
   *
   * @return a {@link LibraryConnection} instance.
   * @throws Exception if the connection pool fails or a connection cannot be created.
   */
  public LibraryConnection leaseConnection()
      throws Exception {
    //If a lease latch is set, wait for countdown
    if (m_LeaseLatch != null) {
      m_LeaseLatch.await();
    }
    LibraryConnection lc = (LibraryConnection) m_ConnectionPool.borrowObject();
    m_NumLeased.addAndGet(1);
    return lc;
  }//leaseConnection

  public void releaseConnection(LibraryConnection lc) {
    try {
      m_ConnectionPool.returnObject(lc);
      if (m_NumLeased.decrementAndGet() == 0 && m_RestoreLatch != null) {
        m_RestoreLatch.countDown();
      }
    } catch (Exception e) {
      Activator.log().error(c_LogMarker, "releaseConnection()", e);
    }
  }//releaseConnection

  private synchronized void prepareDataSource() {
    LibraryConnection lc = null;
    try {
      lc = leaseConnection();
      //1. check select
      try {
        lc.executeQuery("existsSchema", null);
      } catch (SQLException ex) {
        m_New = true;
        createSchema(lc);
      }
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "prepareDataSource()", ex);
    } finally {
      releaseConnection(lc);
    }
  }//prepareDataSource

  private boolean createSchema(LibraryConnection lc) {
    try {
      lc = leaseConnection();
      //1. Create schema, tables and index
      lc.executeCreate("createDiscussionSchema");
      lc.executeCreate("createForumDescriptorTable");
      lc.executeCreate("createForumInfoTable");
      lc.executeCreate("createSubscriptionTable");
      lc.executeCreate("createBanTable");
      lc.executeCreate("createLinkTable");
      lc.executeCreate("createEntryDescriptorTable");
      lc.executeCreate("createEntryTable");
      lc.executeCreate("createEntryMoveTrigger");
      return true;
    } catch (Exception ex) {
      Activator.log().error("createSchema()", ex);
    } finally {
      releaseConnection(lc);
    }
    return false;
  }//createSchema


  public boolean activate(BundleContext bc) {
    m_BundleMessages = Activator.getBundleMessages();
    //1. Configure from persistent configuration
    String ds = "default";
    int cpoolsize = 5;
    int fidcachesize = 25;
    int aidcachesize = 25;
    int ficachesize = 25;
    int subcachesize = 25;
    int agsubcachesize = 25;
    int bancachesize = 25;
    int linklistcachesize = 25;
    int linkcachesize = 25;
    int entrydescriptorcachesize = 25;
    int entrycontentcachesize = 25;
    int entrylistcachesize = 25;
    ConfigurationMediator cm = Activator.getServices().getConfigMediator();
    MetaTypeDictionary mtd = cm.getConfiguration();

    //Retrieve config
    try {
      ds = mtd.getString(DiscussionConfiguration.DATA_SOURCE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.DATA_SOURCE_KEY),
          ex
      );
    }
    try {
      cpoolsize = mtd.getInteger(DiscussionConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      fidcachesize = mtd.getInteger(DiscussionConfiguration.FORUMIDENTIFIERS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.FORUMIDENTIFIERS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      aidcachesize = mtd.getInteger(DiscussionConfiguration.AGENTIDENTIFIERS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.AGENTIDENTIFIERS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      ficachesize = mtd.getInteger(DiscussionConfiguration.FORUMINFOS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.FORUMINFOS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      subcachesize = mtd.getInteger(DiscussionConfiguration.SUBSCRIPTIONS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.SUBSCRIPTIONS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      agsubcachesize = mtd.getInteger(DiscussionConfiguration.AGENTSUBSCRIPTIONS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.AGENTSUBSCRIPTIONS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      bancachesize = mtd.getInteger(DiscussionConfiguration.BANS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.BANS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      linklistcachesize = mtd.getInteger(DiscussionConfiguration.FORUMLINKLISTS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.FORUMLINKLISTS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      linkcachesize = mtd.getInteger(DiscussionConfiguration.LINKS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.LINKS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      entrydescriptorcachesize = mtd.getInteger(DiscussionConfiguration.ENTRYDESCRIPTORS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.ENTRYDESCRIPTORS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      entrycontentcachesize = mtd.getInteger(DiscussionConfiguration.ENTRYCONTENT_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.ENTRYCONTENT_CACHESIZE_KEY),
          ex
      );
    }
    try {
      entrylistcachesize = mtd.getInteger(DiscussionConfiguration.FORUMENTRYLISTS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.FORUMENTRYLISTS_CACHESIZE_KEY),
          ex
      );
    }


    GenericObjectPool.Config poolcfg = new GenericObjectPool.Config();
    poolcfg.maxActive = cpoolsize;
    poolcfg.maxIdle = cpoolsize;
    poolcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    poolcfg.testOnBorrow = true;
    m_ConnectionPool = new GenericObjectPool(new ConnectionFactory(), poolcfg);

    //prepare SQL library and datasource
    try {
      m_SQLLibrary =
          new Library(DiscussionStore.class, "net/coalevo/discussion/impl/discussionstore-sql.xml");
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "activate()", ex);
      return false;
    }

    DataSourceService dss =
        Activator.getServices().getDataSourceService(ServiceMediator.WAIT_UNLIMITED);
    try {
      m_DataSource = dss.waitForDataSource(ds, -1);
    } catch (NoSuchElementException nse) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("BankStore.activation.datasource"),
          nse
      );
      return false;
    }

    //prepare store
    prepareDataSource();
    Activator.log().info(c_LogMarker,
        m_BundleMessages.get("DiscussionStore.database.info", "source", m_DataSource.toString())
    );

    //Create Caches
    m_AgentIdentifierCache = new AgentIdentifierInstanceCache(aidcachesize);
    m_ForumIdentifierCache = new LRUCacheMap<String, ForumIdentifier>(fidcachesize);

    //Create Managers
    m_ForaManager = new ForaManager(this, ficachesize);
    m_SubscriptionManager = new SubscriptionManager(this, subcachesize, agsubcachesize);
    m_BanManager = new BanManager(this, bancachesize);
    m_LinkManager = new LinkManager(this, linklistcachesize, linkcachesize);
    m_EntryManager = new EntryManager(this, entrydescriptorcachesize, entrycontentcachesize, entrylistcachesize);
    m_NotificationManager = new NotificationManager(this);
    //Prepare
    try {
      m_ForaManager.prepare();
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, m_BundleMessages.get("DiscussionStore.foramanager.preparefailed"));
      return false;
    }

    //add handler config updates
    cm.addUpdateHandler(this);
    return true;
  }//activate

  public synchronized boolean deactivate() {

    //sync and clear caches
    if (m_SubscriptionManager != null) {
      //will sync
      m_SubscriptionManager.clear(true);
    }
    if (m_BanManager != null) {
      //will sync
      m_BanManager.clear(true);
    }

    if (m_ForaManager != null) {
      m_ForaManager.clear();
    }
    if (m_LinkManager != null) {
      m_LinkManager.clear();
    }
    if (m_EntryManager != null) {
      m_EntryManager.clear();
    }
    if (m_ForumIdentifierCache != null) {
      m_ForumIdentifierCache.clear();
    }
    //1. close all leased connections
    if (m_ConnectionPool != null) {
      try {
        m_ConnectionPool.close();
      } catch (Exception e) {
        Activator.log().error(c_LogMarker, "deactivate()", e);
      }

    }
    m_AgentIdentifierCache = null;
    m_ForumIdentifierCache = null;
    m_SubscriptionManager = null;
    m_BanManager = null;
    m_EntryManager = null;
    m_LinkManager = null;
    m_ForaManager = null;

    m_DataSource = null;
    m_SQLLibrary = null;
    m_ConnectionPool = null;
    m_BundleMessages = null;
    return true;
  }//deactivate

  public void update(MetaTypeDictionary mtd) {
    //1. Configure from persistent configuration
    int cpoolsize = 3;
    int fidcachesize = 25;
    int aidcachesize = 25;
    int ficachesize = 25;
    int subcachesize = 25;
    int agsubcachesize = 25;
    int bancachesize = 25;
    int linklistcachesize = 25;
    int linkcachesize = 25;
    int entrydescriptorcachesize = 25;
    int entrycontentcachesize = 25;
    int entrylistcachesize = 25;

    //Retrieve config
    try {
      cpoolsize = mtd.getInteger(DiscussionConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      fidcachesize = mtd.getInteger(DiscussionConfiguration.FORUMIDENTIFIERS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.FORUMIDENTIFIERS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      aidcachesize = mtd.getInteger(DiscussionConfiguration.AGENTIDENTIFIERS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.AGENTIDENTIFIERS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      ficachesize = mtd.getInteger(DiscussionConfiguration.FORUMINFOS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.FORUMINFOS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      subcachesize = mtd.getInteger(DiscussionConfiguration.SUBSCRIPTIONS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.SUBSCRIPTIONS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      agsubcachesize = mtd.getInteger(DiscussionConfiguration.AGENTSUBSCRIPTIONS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.AGENTSUBSCRIPTIONS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      bancachesize = mtd.getInteger(DiscussionConfiguration.BANS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.BANS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      linklistcachesize = mtd.getInteger(DiscussionConfiguration.FORUMLINKLISTS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.FORUMLINKLISTS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      linkcachesize = mtd.getInteger(DiscussionConfiguration.LINKS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.LINKS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      entrydescriptorcachesize = mtd.getInteger(DiscussionConfiguration.ENTRYDESCRIPTORS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.ENTRYDESCRIPTORS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      entrycontentcachesize = mtd.getInteger(DiscussionConfiguration.ENTRYCONTENT_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.ENTRYCONTENT_CACHESIZE_KEY),
          ex
      );
    }
    try {
      entrylistcachesize = mtd.getInteger(DiscussionConfiguration.FORUMENTRYLISTS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(c_LogMarker,
          m_BundleMessages.get("DiscussionStore.activation.configexception", "attribute", DiscussionConfiguration.FORUMENTRYLISTS_CACHESIZE_KEY),
          ex
      );
    }

    //Update settings
    m_ConnectionPool.setMaxActive(cpoolsize);
    m_ConnectionPool.setMaxIdle(cpoolsize);
    m_ForumIdentifierCache.setCeiling(fidcachesize);
    m_AgentIdentifierCache.setCeiling(aidcachesize);
    m_ForaManager.setForumInfoCacheSize(ficachesize);
    m_SubscriptionManager.setCeiling(subcachesize);
    m_SubscriptionManager.setAgentSubscriptionsCacheCeiling(agsubcachesize);
    m_BanManager.setCeiling(bancachesize);
    m_LinkManager.setLinkListCacheCeiling(linklistcachesize);
    m_LinkManager.setLinkCacheCeiling(linkcachesize);
    m_EntryManager.setEntryDescriptorCacheCeiling(entrydescriptorcachesize);
    m_EntryManager.setEntryContentCacheCeiling(entrycontentcachesize);
    m_EntryManager.setEntryListCacheCeiling(entrylistcachesize);
  }//update

  //*** Caches and Managers ***//

  public ForaManager getForaManager() {
    return m_ForaManager;
  }//getForaManager

  public BanManager getBanManager() {
    return m_BanManager;
  }//getBanManager

  public LinkManager getLinkManager() {
    return m_LinkManager;
  }//getLinkManager

  public EntryManager getEntryManager() {
    return m_EntryManager;
  }//getEntryManager

  public SubscriptionManager getSubscriptionManager() {
    return m_SubscriptionManager;
  }//getSubscriptionManager

  public NotificationManager getNotificationManager() {
    return m_NotificationManager;
  }//getNotificationManager

  public ForumIdentifier getForumIdentifier(String fidstr) {
    ForumIdentifier fid = m_ForumIdentifierCache.get(fidstr);
    if (fid == null) {
      fid = new ForumIdentifierImpl(fidstr);
      m_ForumIdentifierCache.put(fidstr, fid);
    }
    return fid;
  }//getForumIdentifier

  public AgentIdentifier getAgentIdentifier(String aid) {
    return m_AgentIdentifierCache.get(aid);
  }//getAgentIdentifier


  /**
   * Backups the database of userdata while running.
   * The database will be frozen for writes, but no interruption
   * for reads is required.
   *
   * @param f   the directory to write the backup to.
   * @param tag a possible tag for the backup.
   * @throws SecurityException
   * @throws BackupException   g
   */
  public void backup(File f, String tag)
      throws BackupException {

    /*
    //prepare file
    File backup = null;
    if (tag != null && tag.length() > 0) {
      backup = new File(f, tag);
    } else {
      backup = f;
    }
    //Get connection
    Connection conn = null;
    try {
      conn = m_DataSource.getConnection();
    } catch (SQLException e) {
      Activator.log().error("doBackup()", e);
      throw new BackupException("UserdataStore:: Could not obtain db connection.", e);
    }

    //1. Backup Database
    try {
      CallableStatement cs =
          conn.prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)");
      cs.setString(1, backup.getAbsolutePath());
      cs.execute();
      cs.close();
      Activator.log().info(
          m_BundleMessages.get("UserdataStore.backup.done", "path",
              backup.getAbsolutePath())
      );
    } catch (Exception ex) {
      Activator.log().error("doBackup()", ex);
    }
    */
  }//doBackup

  public void restore(File f, String tag)
      throws RestoreException {

    /*
    //1. Latch for future leases
    m_RestoreLatch = new CountDownLatch(1);
    m_LeaseLatch = new CountDownLatch(1);

    try {
      m_RestoreLatch.await();
    } catch (InterruptedException ex) {
      Activator.log().error("restore()", ex);
    }

    //prepare file
    File bdir = new File(f, DiscussionStore.class.getName());
    if (!bdir.exists()) {
      throw new RestoreException("DiscussionStore:: Failed obtain directory of backup.");
    }
    File backup = null;
    if (tag != null && tag.length() > 0) {
      backup = new File(bdir, tag);
    } else {
      backup = bdir;
    }
    //Restore from connection
    //Connection conn = null;
    m_DataSource.setConnectionAttributes(";restoreFrom=" + backup.getAbsolutePath());

    try {
      maintain();
    } catch (MaintenanceException ex) {
      Activator.log().error("restore()", ex);
    } finally {
      m_DataSource.setConnectionAttributes("");
    }

    m_RestoreLatch = null;
    m_LeaseLatch.countDown();
    m_LeaseLatch = null;
    Activator.log().info(m_BundleMessages.get("DiscussionStore.restore.done"));
    */
  }//restore

  public void maintain() throws MaintenanceException {

    //clear caches
    Activator.log().info(c_LogMarker, m_BundleMessages.get("maintenance.store.caches"));
    m_ForumIdentifierCache.clear();
    m_AgentIdentifierCache.clear();

    //Entry archiving and link checking
    Activator.log().info(c_LogMarker, m_BundleMessages.get("maintenance.store.entries"));
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    int linkcheck = 30;
    try {
      linkcheck = Activator.getServices().getConfigMediator().getConfiguration().getInteger(
          DiscussionConfiguration.LINKS_CHECKINTERVAL_KEY
      );
    } catch (Exception e) {
      Activator.log().error(c_LogMarker, "maintain()", e);
    }
    long due = linkcheck * 86400000;  //days * 24 * 60 * 60 * 1000 = millis

    LibraryConnection lc = null;
    try {
      List<ForumDescriptor> foras = m_ForaManager.getForumDescriptors();
      lc = leaseConnection();

      for (ForumDescriptor fd : foras) {
        //entries
        List<String> eids = m_EntryManager.getActiveEntryList(fd.getIdentifier());
        int to = eids.size() - fd.getMaxActive();
        if(to > 0) {
          for (int i = 0; i < to; i++) {
            EntryDescriptorImpl entry = m_EntryManager.getDescriptor(fd.getIdentifier(), eids.get(i));
            params.put("forum_id", entry.getForumIdentifier().getIdentifier());
            params.put("entry_id", entry.getIdentifier());
            try {
              lc.executeUpdate("archiveEntry", params);
              Activator.log().debug(c_LogMarker, m_BundleMessages.get("maintenance.store.entryarchived", "eid", entry.getIdentifier()));
            } catch (Exception ex) {
              Activator.log().error(c_LogMarker, m_BundleMessages.get("maintenance.error.entryarchive", "eid", entry.getIdentifier()));
            }
          }
        }
        //link checking
        try {
          m_LinkManager.checkLinks(fd.getIdentifier(), due);
        } catch (Exception ex) {
          Activator.log().error(
              c_LogMarker,
              m_BundleMessages.get("maintenance.error.linkscheck", "fid", fd.getIdentifier().getIdentifier())
          );
        }
      }
    } catch (Exception ex) {
      Activator.log().error(
          c_LogMarker,
          m_BundleMessages.get("maintenance.error.fora")
      );
    } finally {
      SqlUtils.close(rs);
      releaseConnection(lc);
    }
    //clear out entries; updated active entry lists will be loaded
    m_EntryManager.clear();

    //Clean up fora
    Activator.log().info(c_LogMarker, m_BundleMessages.get("maintenance.store.fora"));
    try {
      m_ForaManager.prepare();
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "maintain()", ex);
    }

    //clear and maintain all managers
    Activator.log().info(c_LogMarker, m_BundleMessages.get("maintenance.store.sync"));
    m_SubscriptionManager.sync();
    m_BanManager.maintain();
    m_LinkManager.clear();

  }//maintain


  private class ConnectionFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      Connection c = m_DataSource.getConnection();
      c.setAutoCommit(true);
      return new LibraryConnection(m_SQLLibrary, c);
    }//makeObject

    public boolean validateObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      try {
        lc.executeQuery("validate", null);
      } catch (Exception ex) {
        return false;
      }
      return true;
    }//validateObject

    public void destroyObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      if (!lc.isClosed()) {
        SqlUtils.close(lc);
      }
    }//destroyObject

  }//ConnectionFactory

}//class DiscussionStore
