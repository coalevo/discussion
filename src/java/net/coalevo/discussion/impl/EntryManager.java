/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.*;
import net.coalevo.text.util.TagUtility;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.foundation.model.AgentIdentifier;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.*;

/**
 * Manages entries.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class EntryManager {

  private Marker m_LogMarker = MarkerFactory.getMarker(EntryManager.class.getName());

  private DiscussionStore m_DiscussionStore;
  private ForaManager m_ForaManager;

  //Caches
  private LRUCacheMap<String, EntryDescriptorImpl> m_EntryDescriptorCache;
  private LRUCacheMap<String, Content> m_EntryContentCache;
  private LRUCacheMap<ForumIdentifier, List<EntryProxy>> m_EntryListCache;

  public EntryManager(DiscussionStore ds, int edcs, int eccs, int elcs) {
    m_DiscussionStore = ds;
    m_ForaManager = ds.getForaManager();
    m_EntryDescriptorCache = new LRUCacheMap<String, EntryDescriptorImpl>(edcs);
    m_EntryContentCache = new LRUCacheMap<String, Content>(eccs);
    m_EntryListCache = new LRUCacheMap<ForumIdentifier, List<EntryProxy>>(elcs);
  }//constructor

  public void setEntryDescriptorCacheCeiling(int edcs) {
    m_EntryDescriptorCache.setCeiling(edcs);
  }//setEntryDescriptorCacheCeiling

  public void setEntryContentCacheCeiling(int eccs) {
    m_EntryContentCache.setCeiling(eccs);
  }//setEntryContentCacheCeiling

  public void setEntryListCacheCeiling(int elcs) {
    m_EntryListCache.setCeiling(elcs);
  }//setEntryListCacheCeiling


  public void clear() {
    m_EntryDescriptorCache.clear();
    m_EntryContentCache.clear();
    m_EntryListCache.clear();
  }//clear

  public EntryDescriptorImpl getDescriptor(ForumIdentifier fid, String id)
      throws DiscussionServiceException {
    EntryDescriptorImpl ed = m_EntryDescriptorCache.get(id);

    if (ed == null) {
      //load from RDBMS
      //Build list
      ResultSet rs = null;
      HashMap<String, String> params = new HashMap<String, String>();
      if(fid != null) {
        params.put("forum_id", fid.getIdentifier());
      }
      params.put("entry_id", id);

      LibraryConnection lc = null;
      try {
        lc = m_DiscussionStore.leaseConnection();
        if(fid != null) {
          rs = lc.executeQuery("getEntryDescriptor", params);
        } else {
          rs = lc.executeQuery("getByEntryDescriptor", params);
        }
        if (rs.next()) {
          ed = new EntryDescriptorImpl(
              m_DiscussionStore.getForumIdentifier(rs.getString(1)),
              rs.getString(2),
              rs.getString(3),
              m_DiscussionStore.getAgentIdentifier(rs.getString(4)),
              rs.getString(5),
              TagUtility.toSet(rs.getString(6)),
              rs.getString(7),
              rs.getLong(8),
              rs.getLong(9),
              rs.getShort(10) == 1,
              rs.getShort(11) == 1,
              rs.getShort(12) == 1
          );
          m_EntryDescriptorCache.put(id, ed);
        } else {
          throw new NoSuchEntryException(id);
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"getDescriptor()", ex);
        //TODO: Add message
        throw new DiscussionServiceException();
      } finally {
        SqlUtils.close(rs);
        m_DiscussionStore.releaseConnection(lc);
      }
    }

    return ed;
  }//getDescriptor

  public AgentIdentifier getAuthor(ForumIdentifier fid, String entryid) {
    try {
      return getDescriptor(fid,entryid).getAuthor();
    } catch (Exception ex) {
      return null;
    }
  }//getAuthor

  public void create(EditableEntryImpl entry)
      throws DiscussionServiceException {

    //Add to RDBMS
    HashMap<String, String> params1 = new HashMap<String, String>();
    params1.put("forum_id", entry.getForumIdentifier().getIdentifier());
    params1.put("entry_id", entry.getIdentifier());
    params1.put("ancestor_id", entry.getAncestor());
    params1.put("author_id", entry.getAuthor().getIdentifier());
    params1.put("caption", entry.getCaption());
    params1.put("tags", TagUtility.fromSet(entry.getTags()));
    params1.put("contentformat", entry.getContentFormat());
    params1.put("created", "" + entry.getCreated());
    params1.put("approved", "" + entry.getApproved());
    params1.put("official", (entry.isOfficial()) ? "1" : "0");
    params1.put("descendants", (entry.isDescendantAllowed()) ? "1" : "0");
    params1.put("archived", (entry.isArchived()) ? "1" : "0");

    HashMap<String, String> params2 = new HashMap<String, String>();
    params2.put("forum_id", entry.getForumIdentifier().getIdentifier());
    params2.put("entry_id", entry.getIdentifier());
    params2.put("document", entry.getContent().getContent());

    LibraryConnection lc = null;
    try {
      try {
        lc = m_DiscussionStore.leaseConnection();
        lc.executeUpdate("createEntryDescriptor", params1);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"create()", ex);
        throw new DiscussionServiceException(Activator.getBundleMessages().get("EntryManager.createfailed"), ex);
      }
      try {
        lc.executeUpdate("createEntry", params2);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"create()", ex);
        //if failed rollback
        remove(entry.getForumIdentifier(),entry.getIdentifier());
        throw new DiscussionServiceException(Activator.getBundleMessages().get("EntryManager.createfailed"), ex);
      }
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }

    //Add to Caches
    if (!entry.isArchived() && entry.isApproved()) {
      m_EntryDescriptorCache.put(entry.getIdentifier(), entry.toEntryDescriptor());
      m_EntryContentCache.put(entry.getIdentifier(), entry.getContent().toContent());
      //if the list is in the cache then we need to add the entry, if not, it will
      //happen when the list is loaded from the database. This prevents duplication (see CDB-7).
      if(m_EntryListCache.containsKey(entry.getForumIdentifier())){
        List<EntryProxy> entries = getActiveEntryProxyList(entry.getForumIdentifier());
        if (entries != null) {
          synchronized (entries) {
            entries.add(new EntryProxy(entry.getIdentifier(), entry.getApproved()));
          }
        }
      }

      //update fm
      m_ForaManager.updateLastEntry(entry.getForumIdentifier(), entry.getApproved());
    }
  }//create

  public void remove(ForumIdentifier fid, String identifier)
      throws DiscussionServiceException {

    //Remove from database, should clean up entry as well
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    params.put("entry_id", identifier);

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("removeEntryDescriptor", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"remove()", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("EntryManager.removefailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }

    //remove from caches
    m_EntryDescriptorCache.remove(identifier);
    m_EntryContentCache.remove(identifier);
    List<EntryProxy> entries = getActiveEntryProxyList(fid);
    synchronized (entries) {
      if (entries != null) {
        entries.remove(new EntryProxy(identifier, 0));
      }
    }

  }//remove

  public List<String> getActiveEntryList(ForumIdentifier fid)
      throws DiscussionServiceException {

    List<EntryProxy> entries = getActiveEntryProxyList(fid);
    synchronized (entries) {
      ArrayList<String> ids = new ArrayList<String>(entries.size());
      for (EntryProxy ep : entries) {
        ids.add(ep.m_Identifier);
      }
      return ids;
    }
  }//getActiveEntryList

  public List<String> getActiveEntryList(ForumIdentifier fid, long timestamp)
      throws DiscussionServiceException {
    List<EntryProxy> entries = getActiveEntryProxyList(fid);
    synchronized (entries) {
      int idx = this.findNextMoreRecent(entries, timestamp);
      if (idx == -1 || entries.size() == idx) {
        return new ArrayList<String>(0);
      }
      ArrayList<String> ids = new ArrayList<String>(entries.size() - idx);
      for (Iterator<EntryProxy> iterator = entries.listIterator(idx); iterator.hasNext();) {
        ids.add(iterator.next().m_Identifier);
      }
      return ids;
    }
  }//getEntryList

  public List<String> getActiveEntryList(ForumIdentifier fid, int numentries)
      throws DiscussionServiceException {
    List<EntryProxy> entries = getActiveEntryProxyList(fid);
    synchronized (entries) {
      numentries = Math.min(entries.size(), numentries); //ensure its not more than there actually is.
      ArrayList<String> ids = new ArrayList<String>(numentries);
      int to = entries.size();
      int from = to - numentries;
      for (int i = from; i < to; i++) {
        ids.add(entries.get(i).m_Identifier);
      }
      return ids;
    }
  }//getActiveEntryList

  public List<String> getActiveEntryList(ForumIdentifier fid, int from, int to)
      throws DiscussionServiceException {
    List<EntryProxy> entries = getActiveEntryProxyList(fid);
    synchronized (entries) {
      int size = to - from;
      if (size <= 0 || to > entries.size()) {
        //error
        throw new IllegalArgumentException();
      }
      ArrayList<String> ids = new ArrayList<String>(size);
      for (int i = from; i < to; i++) {
        ids.add(entries.get(i).m_Identifier);
      }
      return ids;
    }
  }//getActiveEntry

  public int getActiveEntryCount(ForumIdentifier fid)
      throws DiscussionServiceException {
    List<EntryProxy> entries = getActiveEntryProxyList(fid);
    synchronized (entries) {
      return entries.size();
    }
  }//getActiveEntryCount

  private List<EntryProxy> getActiveEntryProxyList(ForumIdentifier fid)
      throws DiscussionServiceException {

    //List
    List<EntryProxy> px = m_EntryListCache.get(fid);
    if (px == null) {
      Activator.log().debug(m_LogMarker,"========> Cache miss for: " + fid.toString());
      px = new ArrayList<EntryProxy>();

      //Build list
      ResultSet rs = null;
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("forum_id", fid.getIdentifier());
      LibraryConnection lc = null;
      try {
        lc = m_DiscussionStore.leaseConnection();
        rs = lc.executeQuery("listActiveEntries", params);
        while (rs.next()) {
          px.add(new EntryProxy(rs.getString(1), rs.getLong(2)));
        }
        m_EntryListCache.put(fid, px);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"getActiveEntryProxyList()", ex);
        //TODO: Add message
        throw new DiscussionServiceException();
      } finally {
        SqlUtils.close(rs);
        m_DiscussionStore.releaseConnection(lc);
      }
    }
    return px;
  }//getEntryList

  public boolean hasNewEntries(ForumIdentifier fid, long timestamp)
      throws DiscussionServiceException {
    List<EntryProxy> entries = getActiveEntryProxyList(fid);
    synchronized (entries) {
      if (entries == null || entries.isEmpty()) {
        return false;
      }
      return (entries.get(entries.size() - 1).compareTo(timestamp) > 0);
    }
  }//hasNewEntries

  public Content getContent(ForumIdentifier fid, String identifier)
      throws DiscussionServiceException, NoSuchEntryException {

    Content c = m_EntryContentCache.get(identifier);
    if (c == null) {
      //retrieve from rdbms
      ResultSet rs = null;
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("forum_id", fid.getIdentifier());
      params.put("entry_id", identifier);
      LibraryConnection lc = null;
      try {
        lc = m_DiscussionStore.leaseConnection();
        rs = lc.executeQuery("getEntry", params);
        if (rs.next()) {
          c = new ContentImpl(rs.getString(1));

        } else {
          throw new NoSuchEntryException(identifier);
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"getEntryContent()", ex);
        //TODO: Add message
        throw new DiscussionServiceException();
      } finally {
        SqlUtils.close(rs);
        m_DiscussionStore.releaseConnection(lc);
      }
    }
    return c;
  }//getContent

  public List<String> listPendingEntries(ForumIdentifier fid)
      throws DiscussionServiceException {

    List<String> entries = new ArrayList<String>();

    //Build list
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      rs = lc.executeQuery("listPendingEntries", params);
      while (rs.next()) {
        entries.add(rs.getString(1));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"listPendingEntries()", ex);
      //TODO: Add message
      throw new DiscussionServiceException();
    } finally {
      SqlUtils.close(rs);
      m_DiscussionStore.releaseConnection(lc);
    }

    return entries;
  }//listPendingEntries

  public void approveEntry(ForumIdentifier fid, String eid)
      throws DiscussionServiceException {

    EntryDescriptorImpl ed = getDescriptor(fid, eid);
    ed.setApproved(true);
    //RDBMS
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    params.put("entry_id", eid);
    params.put("approved", Long.toString(ed.getApproved()));

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("approveEntry", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"approveEntry()", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("EntryManager.approvefailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }

    //Add to Caches
    if (!ed.isArchived() && ed.isApproved()) {
      List<EntryProxy> entries = getActiveEntryProxyList(ed.getForumIdentifier());
      synchronized (entries) {
        if (entries != null) {
          entries.add(new EntryProxy(ed.getIdentifier(), ed.getApproved()));
        }
      }
      //update fm
      m_ForaManager.updateLastEntry(ed.getForumIdentifier(), ed.getApproved());
    }
  }//approveEntry

  public void disapproveEntry(ForumIdentifier fid, String eid)
      throws DiscussionServiceException {

    //swiftly delete it
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    params.put("entry_id", eid);

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("removeEntryDescriptor", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"disapproveEntry()", ex);
      throw new DiscussionServiceException(
          Activator.getBundleMessages().get("EntryManager.disapprovefailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }
  }//disapproveEntry

  public void moveEntry(ForumIdentifier from, ForumIdentifier to, String id)
      throws DiscussionServiceException {

    //RDBMS
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", from.getIdentifier());
    params.put("entry_id", id);
    params.put("to_id", to.getIdentifier());

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("moveEntry", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"moveEntry()", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("EntryManager.movefailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }

    //TODO: Update to caches....!?
    //drop cached descriptor
    m_EntryDescriptorCache.remove(id);
    //remove from list cache?
    List<EntryProxy> entries = getActiveEntryProxyList(from);
    synchronized (entries) {
      entries.remove(new EntryProxy(id, 0));
    }
  }//moveEntry

  private int findNextMoreRecent(List<EntryProxy> entries, long timestamp) {
    if (timestamp < entries.get(0).m_Approved) {
      return 0;
    }
    for (int i = 0; i < entries.size() - 1;) {
      long t1 = entries.get(i).m_Approved;
      long t2 = entries.get(++i).m_Approved;

      if (timestamp >= t1 && timestamp < t2) {
        return i;
      }
    }
    return -1;
  }//findNextMoreRecent;


  public class EntryProxy implements Comparable<Long> {

    String m_Identifier;
    Long m_Approved;

    public EntryProxy(String id, long app) {
      m_Identifier = id;
      m_Approved = app;
    }//EntryProxy

    public int compareTo(Long l) {
      if (this.m_Approved > l) {
        return 1;
      } else if (this.m_Approved < l) {
        return -1;
      } else {
        return 0;
      }
    }//compareTo

    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      EntryProxy that = (EntryProxy) o;

      if (m_Identifier != null ? !m_Identifier.equals(that.m_Identifier) : that.m_Identifier != null) return false;

      return true;
    }//equals

    public int hashCode() {
      return (m_Identifier != null ? m_Identifier.hashCode() : 0);
    }//hashCode

  }//inner class EntryProxy

  private static final Comparator<EntryDescriptor> BY_APPROVAL = new Comparator<EntryDescriptor>() {

    public int compare(EntryDescriptor o1, EntryDescriptor o2) {
      long t1 = o1.getApproved();
      long t2 = o2.getApproved();
      if (t1 < t2) {
        return -1;
      } else if (t1 == t2) {
        return 0;
      } else {
        return 1;
      }
    }//compare
  };

}//class EntryManager
