/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.foundation.model.AgentIdentifier;

import java.util.Locale;
import java.util.Set;

/**
 * Implements a {@link ForumDescriptor}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ForumDescriptorImpl
    implements ForumDescriptor {

  protected ForumIdentifier m_ForumIdentifier;
  protected int m_SequenceNumber;
  protected String m_Mnemonic;
  protected String m_Name;
  protected Locale m_Language;
  protected AgentIdentifier m_Creator;
  protected AgentIdentifier m_Moderator;
  protected Set<String> m_Tags;
  protected String m_InfoFormat;
  protected long m_Created;
  protected long m_Modified;
  protected int m_MaxActive;
  protected boolean m_InviteOnly;
  protected boolean m_ModeratedStrict;
  protected boolean m_AnonymousAllowed;
  protected boolean m_Invalidated = false;
  protected boolean m_Public = false;

  public ForumDescriptorImpl() {

  }//constructor

  public ForumDescriptorImpl(ForumIdentifier fid, int seqnum,
                             String mnemonic, String name,
                             Locale lang, AgentIdentifier creator,
                             AgentIdentifier moderator, Set<String> tags,
                             String infoformat, long created, long modified,
                             boolean inviteonly, boolean strict,
                             boolean anonallowed,boolean pub, int maxactive) {

    m_ForumIdentifier = fid;
    m_SequenceNumber = seqnum;
    m_Mnemonic = mnemonic;
    m_Name = name;
    m_Language = lang;
    m_Creator = creator;
    m_Moderator = moderator;
    m_Tags = tags;
    m_InfoFormat = infoformat;
    m_Created = created;
    m_Modified = modified;
    m_InviteOnly = inviteonly;
    m_ModeratedStrict = strict;
    m_AnonymousAllowed = anonallowed;
    m_Public = pub;
    m_MaxActive = maxactive;
  }//

  public ForumIdentifier getIdentifier() {
    return m_ForumIdentifier;
  }//getIdentifier

  public int getSequenceNumber() {
    return m_SequenceNumber;
  }//getSequenceNumber

  public String getMnemonic() {
    return m_Mnemonic;
  }//getMnemonic

  public String getName() {
    return m_Name;
  }//getName

  public Locale getLanguage() {
    return m_Language;
  }//getLanguage

  public AgentIdentifier getCreator() {
    return m_Creator;
  }//getCreator

  public AgentIdentifier getModerator() {
    return m_Moderator;
  }//getModerator

  public Set<String> getTags() {
    return m_Tags;
  }//getTags

  public String getInfoFormat() {
    return m_InfoFormat;
  }//getInfoFormat

  public boolean isModeratedStrict() {
    return m_ModeratedStrict;
  }//isModeratedStrict

  public boolean isAnonymousAllowed() {
    return m_AnonymousAllowed;
  }//isAnonymousAllowed

  public boolean isInviteOnly() {
    return m_InviteOnly;
  }//isInviteOnly

  public boolean isPublic() {
    return m_Public;
  }//isPublic

  public long getCreated() {
    return m_Created;
  }//getCreated

  public long getModified() {
    return m_Modified;
  }//getModified

  public int getMaxActive() {
    return m_MaxActive;
  }//getMaxActive
  
  public boolean isInvalidated() {
    return m_Invalidated;
  }//isInvalidated

  public void invalidate() {
    m_Invalidated = true;
  }//invalidate

  public EditableForumDescriptorImpl toEditableForumDescriptor() {
    EditableForumDescriptorImpl fdi = new EditableForumDescriptorImpl(m_ForumIdentifier, m_Creator);
    fdi.m_SequenceNumber = this.m_SequenceNumber;
    fdi.m_Mnemonic = this.m_Mnemonic;
    fdi.m_Name = this.m_Name;
    fdi.m_Language = this.m_Language;
    fdi.m_Moderator = this.m_Moderator;
    fdi.m_Tags = this.m_Tags;
    fdi.m_InfoFormat = this.m_InfoFormat;
    fdi.m_Created = this.m_Created;
    fdi.m_Modified = this.m_Modified;
    fdi.m_InviteOnly = this.m_InviteOnly;
    fdi.m_ModeratedStrict = this.m_ModeratedStrict;
    fdi.m_AnonymousAllowed = this.m_AnonymousAllowed;
    fdi.m_Public = this.m_Public;
    fdi.m_MaxActive = this.m_MaxActive;
    return fdi;
  }//toEditableForumDescriptor

}//class ForumDescriptorImpl
