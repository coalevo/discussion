/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.*;
import net.coalevo.text.util.TagUtility;
import net.coalevo.foundation.util.LRUCacheMap;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Implements a LinkManager.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class LinkManager {

  private Marker m_LogMarker = MarkerFactory.getMarker(LinkManager.class.getName());

  private DiscussionStore m_DiscussionStore;

  private LRUCacheMap<ForumIdentifier, List<String>> m_LinkListCache;
  private LRUCacheMap<String, LinkImpl> m_LinkCache;

  public LinkManager(DiscussionStore ds, int llcs, int lcs) {
    m_DiscussionStore = ds;
    m_LinkListCache = new LRUCacheMap<ForumIdentifier, List<String>>(llcs);
    m_LinkCache = new LRUCacheMap<String, LinkImpl>(lcs);
  }//constructor

  public void setLinkListCacheCeiling(int llcs) {
    m_LinkListCache.setCeiling(llcs);
  }//setLinkListCacheCeiling

  public void setLinkCacheCeiling(int lcs) {
    m_LinkCache.setCeiling(lcs);
  }//setLinkCacheCeiling

  public void clear() {
    m_LinkCache.clear();
    m_LinkListCache.clear();
  }//clear

  public LinkImpl get(ForumIdentifier fid, String identifier)
      throws DiscussionServiceException, NoSuchLinkException {

    LinkImpl l = m_LinkCache.get(identifier);

    if (l == null) {
      //get from RDBMS
      ResultSet rs = null;
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("forum_id", fid.getIdentifier());
      params.put("link_id", identifier);

      LibraryConnection lc = null;
      try {
        lc = m_DiscussionStore.leaseConnection();
        rs = lc.executeQuery("getLink", params);
        if (rs.next()) {
          l = new LinkImpl(
              m_DiscussionStore.getForumIdentifier(rs.getString(1)),
              rs.getString(2),
              m_DiscussionStore.getAgentIdentifier(rs.getString(3)),
              rs.getString(4),
              rs.getString(5),
              rs.getString(6),
              rs.getString(7),
              TagUtility.toSet(rs.getString(8)),
              (rs.getShort(9) == 1),
              (rs.getShort(10) == 1),
              rs.getLong(11),
              rs.getLong(12),
              rs.getLong(13)
          );
        } else {
          throw new NoSuchLinkException();
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"get()::", ex);
        //TODO: Add message
        throw new DiscussionServiceException();
      } finally {
        SqlUtils.close(rs);
        m_DiscussionStore.releaseConnection(lc);
      }
      //store in cache
      m_LinkCache.put(identifier, l);
    }
    return l;
  }//get

  public void create(EditableLinkImpl el)
      throws DiscussionServiceException {

    //Add to RDBMS
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("link_id", el.getIdentifier());
    params.put("forum_id", el.getForumIdentifier().getIdentifier());
    params.put("author_id", el.getAuthor().getIdentifier());
    params.put("caption", el.getCaption());
    params.put("descformat", el.getDescriptionFormat());
    params.put("description", el.getDescription());
    params.put("link", el.getLink());
    params.put("tags", TagUtility.fromSet(el.getTags()));
    params.put("approved", (el.isApproved()) ? "1" : "0");
    params.put("functional", (el.isFunctional()) ? "1" : "0");
    params.put("created", "" + el.getCreated());
    params.put("modified", "" + el.getModified());
    params.put("lastchecked", "" + el.getLastChecked());

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("createLink", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"create()", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("LinkManager.createfailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }

    //add to cache
    m_LinkCache.put(el.getIdentifier(), el.toLink());
    //add to cached list
    List<String> ids = m_LinkListCache.get(el.getForumIdentifier());
    if (ids != null && el.isApproved()) {
      synchronized(ids) {
        ids.add(el.getIdentifier());
      }
    }
  }//create

  public void update(EditableLinkImpl el)
      throws DiscussionServiceException {

    el.setModified();
    //Add to RDBMS
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("link_id", el.getIdentifier());    
    params.put("forum_id", el.getForumIdentifier().getIdentifier());
    params.put("author_id", el.getAuthor().getIdentifier());
    params.put("caption", el.getCaption());
    params.put("descformat", el.getDescriptionFormat());
    params.put("description", el.getDescription());
    params.put("link", el.getLink());
    params.put("tags", TagUtility.fromSet(el.getTags()));
    params.put("approved", (el.isApproved()) ? "1" : "0");
    params.put("functional", (el.isFunctional()) ? "1" : "0");
    params.put("modified", "" + el.getModified());
    params.put("lastchecked", "" + el.getLastChecked());

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("updateLink", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"update()", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("LinkManager.updatefailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }
    //Update caches
    List<String> ids = m_LinkListCache.get(el.getForumIdentifier());
     if (ids != null) {
       synchronized(ids) {
         if(!ids.contains(el.getIdentifier()) && el.isApproved()){
           ids.add(el.getIdentifier());
         }
      }
    }
    //add link to cache (updated state)
    m_LinkCache.put(el.getIdentifier(), el.toLink());
  }//update

  public void remove(ForumIdentifier fid, String id)
      throws DiscussionServiceException {
    //remove from RDBMS
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    params.put("link_id", id);

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("removeLink", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"remove()", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("LinkManager.removefailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }

    //remove from caches
    m_LinkCache.remove(id);
    List<String> ids = m_LinkListCache.get(fid);
    if (ids != null) {
      synchronized(ids) {
        ids.remove(id);
      }
    }
  }//remove

  public List<String> getLinkList(ForumIdentifier fid)
      throws DiscussionServiceException {

    //List
    List<String> ids = m_LinkListCache.get(fid);
    if (ids == null) {
      ids = new ArrayList<String>();

      //Build list
      ResultSet rs = null;
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("forum_id", fid.getIdentifier());
      LibraryConnection lc = null;
      try {
        lc = m_DiscussionStore.leaseConnection();
        rs = lc.executeQuery("listLinkIDs", params);
        while (rs.next()) {
          ids.add(rs.getString(1));
        }
        m_LinkListCache.put(fid, ids);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"getLinkList()", ex);
        //TODO: Add message
        throw new DiscussionServiceException();
      } finally {
        SqlUtils.close(rs);
        m_DiscussionStore.releaseConnection(lc);
      }
    }
    synchronized(fid) {
      return new ArrayList<String>(ids); //return a shallow copy
    }
  }//getLinkList

  public List<String> listPendingLinks(ForumIdentifier fid)
      throws DiscussionServiceException {

    List<String> links = new ArrayList<String>();

    //Build list
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      rs = lc.executeQuery("listPendingLinks", params);
      while (rs.next()) {
        links.add(rs.getString(1));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"listPendingLinks()", ex);
      //TODO: Add message
      throw new DiscussionServiceException();
    } finally {
      SqlUtils.close(rs);
      m_DiscussionStore.releaseConnection(lc);
    }

    return links;
  }//getPendingLinks

  public void approveLink(ForumIdentifier fid, String linkid)
      throws DiscussionServiceException {

    //RDBMS
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    params.put("link_id", linkid);
    params.put("approved", "1");

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("approveLink", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"approveLink()", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("LinkManager.approvefailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }
    //Update caches
    List<String> ids = m_LinkListCache.get(fid);
     if (ids != null) {
       synchronized(ids) {
         if(!ids.contains(linkid)){
           ids.add(linkid);
         }
      }
    }
    //remove from cache (updated state)
    m_LinkCache.remove(linkid);
  }//approveLink

  public void disapproveLink(ForumIdentifier fid, String linkid)
      throws DiscussionServiceException {

    //swiftly delete it
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    params.put("link_id", linkid);

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("removeLink", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"disapproveLink()", ex);
      throw new DiscussionServiceException(
          Activator.getBundleMessages().get("LinkManager.disapprovefailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }
  }//disapproveLink

  public void moveLink(ForumIdentifier from, ForumIdentifier to, String id)
      throws DiscussionServiceException {

    //RDBMS
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", from.getIdentifier());
    params.put("link_id", id);
    params.put("to_id", to.getIdentifier());

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("moveLink", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"moveLink()", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("LinkManager.movefailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }

    //update caches:
    m_LinkCache.remove(id);
    List<String> ids = m_LinkListCache.get(from);
    if (ids != null) {
      synchronized(ids) {
        ids.remove(id);
      }
    }
  }//moveLink

  public void checkLinks(ForumIdentifier fid, long due)
      throws DiscussionServiceException {
    List<String> lids = getLinkList(fid);
    for(String id:lids) {
      checkLink(fid,id, due);
    }
  }//checkLinks

  public void checkLinks(ForumIdentifier fid) throws DiscussionServiceException {
    checkLinks(fid,-1);
  }//checkLinks

  public void checkLink(ForumIdentifier fid, String id, long due)
      throws DiscussionServiceException {
    LinkImpl l = get(fid, id);
    if(l.getLastChecked() > 0 &&  (System.currentTimeMillis() - l.getLastChecked()) < due) {
      return;
    }
    EditableLinkImpl el = l.toEditableLink();
    try {
      try {
        URL url = new URL(l.getLink());
        URLConnection connection = url.openConnection();
        if (connection instanceof HttpURLConnection) {
          HttpURLConnection httpConnection = (HttpURLConnection) connection;
          //follow redirects if possible, as long as they work
          httpConnection.setInstanceFollowRedirects(true);
          httpConnection.connect();
          int rcode = httpConnection.getResponseCode();
          Activator.log().info(m_LogMarker,"checkLink()::Checked ->" + l.getLink() + "<- Result=" + rcode);
          if (rcode == 200) {
            el.setFunctional(true);
          } else {
            el.setFunctional(false);
          }
          //empty stream
          InputStream is = httpConnection.getInputStream();
          byte[] buffer = new byte [256];
          while (is.read(buffer) != -1) {}
          is.close();
          httpConnection.disconnect();
        }
      } catch (IOException e) {
        el.setFunctional(false);
        Activator.log().error(m_LogMarker,"checkLink",e);
      }
      el.updateLastChecked();
      update(el);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"checkLink", ex);
    }
  }//checkLink


}//class LinkManager
