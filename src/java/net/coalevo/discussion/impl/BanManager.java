/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.Ban;
import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.util.CacheMapExpelHandler;
import net.coalevo.foundation.util.LRUCacheMap;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.*;

/**
 * Implements a manager for bans.
 * <p/>
 * This manager will handle bans, working directly
 * with the backing store. For performance reasons bans
 * will be cached, and updates propagated to the backing store
 * when a cache entry is expelled (LRU; e.g. when it is outdated).
 * <p/>
 * To force syncronization with the backing store, use {@link #sync()}.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BanManager
    extends LRUCacheMap<String, Ban> {

  private Marker m_LogMarker = MarkerFactory.getMarker(BanManager.class.getName());
  private DiscussionStore m_DiscussionStore;

  public BanManager(DiscussionStore ds, int ceiling) {
    super(ceiling);
    m_ExpelHandler = new BanManager.ExpelHandler();
    m_DiscussionStore = ds;
  }//constructor

  private void add(Ban b) {
    put(b.getIdentifier(), b);
  }//add

  public boolean isBanned(ForumIdentifier fid, AgentIdentifier aid) {
    return (getBan(fid, aid) != null);
  }//isBanned

  public Ban getBan(ForumIdentifier fid, AgentIdentifier aid) {
    Ban ban = get(toID(fid, aid));
    if (ban == null) {
      //get from DS
      ResultSet rs = null;
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("forum_id", fid.getIdentifier());
      params.put("who_id", aid.getIdentifier());
      LibraryConnection lc = null;
      try {
        lc = m_DiscussionStore.leaseConnection();
        rs = lc.executeQuery("getBan", params);
        if (rs.next()) {
          ban = new BanImpl(
              fid,
              aid,
              m_DiscussionStore.getAgentIdentifier(rs.getString(1)),
              rs.getString(2),
              rs.getLong(3),
              rs.getLong(4));
        } else {
          return null;
        }
      } catch (Exception ex) {
        Activator.log().error("getBan()::", ex);
      } finally {
        SqlUtils.close(rs);
        m_DiscussionStore.releaseConnection(lc);
      }
      //add to cache
      add(ban);
    }
    return ban;
  }//getBan

  public void create(Ban b) throws DiscussionServiceException {
    //Add to RDBMS
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", b.getForumIdentifier().getIdentifier());
    params.put("who_id", b.getWho().getIdentifier());
    params.put("bywhom_id", b.getByWhom().getIdentifier());
    params.put("reason", b.getReason());
    params.put("created", "" + b.getCreated());
    params.put("until", "" + b.getUntil());

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("createBan", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "create()", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("BanManager.createfailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }
    add(b);
  }//create

  public void remove(ForumIdentifier fid, AgentIdentifier aid)
      throws DiscussionServiceException {

    //Remove from RDBMS
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    params.put("who_id", aid.getIdentifier());
    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("removeBan", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "removeBan()", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("BanManager.removefailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }
    remove(toID(fid, aid));
  }//remove

  public List<AgentIdentifier> listBanned(ForumIdentifier fid) {

    List<AgentIdentifier> bans = new ArrayList<AgentIdentifier>(50);
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      rs = lc.executeQuery("listBanned", params);
      while (rs.next()) {
        bans.add(m_DiscussionStore.getAgentIdentifier(rs.getString(1)));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, Activator.getBundleMessages().get("BanManager.listfailed"), ex);
    } finally {
      SqlUtils.close(rs);
      m_DiscussionStore.releaseConnection(lc);
    }

    return bans;
  }//listBanned

  public void sync() {
    synchronized (this) {
      for (Iterator iterator = entrySet().iterator(); iterator.hasNext();) {
        Map.Entry entry = (Map.Entry) iterator.next();
        update((Ban) entry.getValue());
      }
    }
    //clear caches
    clear();
  }//sync

  public void maintain() {
    synchronized (this) {
      sync();
      LibraryConnection lc = null;
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("until", Long.toString(System.currentTimeMillis()));
      try {
        lc = m_DiscussionStore.leaseConnection();
        lc.executeUpdate("cleanOutdatedBans", params);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, Activator.getBundleMessages().get("BanManager.maintainfailed"), ex);
      } finally {
        m_DiscussionStore.releaseConnection(lc);
      }
    }
  }//maintain

  private void update(Ban b) {
    if (b.isDirty()) {
      //TODO: commit asynchronous through service?
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("forum_id", b.getForumIdentifier().getIdentifier());
      params.put("who_id", b.getWho().getIdentifier());
      params.put("bywhom_id", b.getByWhom().getIdentifier());
      params.put("reason", b.getReason());
      params.put("until", "" + b.getUntil());
      LibraryConnection lc = null;
      try {
        lc = m_DiscussionStore.leaseConnection();
        lc.executeUpdate("updateBan", params);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "update(Ban)", ex);
      } finally {
        m_DiscussionStore.releaseConnection(lc);
      }
      b.setDirty(false);
    }
  }//update

  private String toID(ForumIdentifier fid, AgentIdentifier aid) {
    return new StringBuilder().append(fid.getIdentifier()).append(aid.getIdentifier()).toString();
  }//toID

  class ExpelHandler implements CacheMapExpelHandler<String, Ban> {

    public void expelled(Map.Entry<String, Ban> entry) {
      Ban b = entry.getValue();
      update(b);
    }//expelled

  }//inner class ExpelHandler


}//class BanManager
