/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.discussion.model.EntryDescriptor;
import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.Subscription;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Messages;
import net.coalevo.messaging.model.EditableInteractiveMessage;
import net.coalevo.messaging.model.InteractiveMessageTypes;
import net.coalevo.messaging.model.MessagingException;
import net.coalevo.messaging.model.ServiceMessagingProxy;
import net.coalevo.presence.model.AgentIdentifierList;
import net.coalevo.presence.model.PresenceProxy;
import net.coalevo.presence.model.PresenceServiceListenerAdapter;
import net.coalevo.presence.model.ServicePresenceProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import org.osgi.framework.BundleContext;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.*;

/**
 * Provides an implementation for the notification management of the
 * discussion service.
 * <p/>
 * The manager will keep track of present subscribers and multicast
 * notifications when entries have been approved for a forum to
 * all interested parties (e.g. subscribed with notification enabled).
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class NotificationManager {

  private Marker m_LogMarker = MarkerFactory.getMarker(NotificationManager.class.getName());

  //Store & Managers
  private DiscussionStore m_Store;
  private SubscriptionManager m_SubscriptionManager;
  private ForaManager m_ForaManager;

  private ServiceMediator m_Services;
  private ServiceAgentProxy m_Agent;
  private Messages m_BundleMessages;

  private ServicePresenceProxy m_ServicePresenceProxy;
  private ServiceMessagingProxy m_ServiceMessagingProxy;

  private Set<String> m_Resources;
  private HashMap<String, Set<AgentIdentifier>> m_ResourceReceivers;

  private Set<AgentIdentifier> m_PresentSubscribers;

  public NotificationManager(DiscussionStore ds) {
    m_Store = ds;
    m_SubscriptionManager = ds.getSubscriptionManager();
    m_ForaManager = ds.getForaManager();
  }//constructor

  public void activate(BundleContext bc, ServiceAgentProxy agent) {
    m_Agent = agent;
    m_BundleMessages = Activator.getBundleMessages();
    m_Services = Activator.getServices();

    //1. prepare resources to be mnemonics
    List<String> res = m_Store.getForaManager().getForaMnemonics();
    m_Resources = new HashSet<String>();
    m_ResourceReceivers = new HashMap<String, Set<AgentIdentifier>>();
    synchronized (m_ResourceReceivers) {
      for (String r : res) {
        m_Resources.add(r);
        m_ResourceReceivers.put(r, Collections.synchronizedSet(new HashSet<AgentIdentifier>()));
      }
    }
    m_PresentSubscribers = new HashSet<AgentIdentifier>();

    //1. Presence Handling
    m_ServicePresenceProxy = new ServicePresenceProxy(new NotificationPresenceListener(), m_Agent, Activator.log());
    m_ServicePresenceProxy.activate(bc);

    //2. Messaging Handling
    m_ServiceMessagingProxy = new ServiceMessagingProxy(m_ServicePresenceProxy, Activator.log());
    m_ServiceMessagingProxy.activate(bc);
    m_ServicePresenceProxy.getPresence().setResources((String[]) res.toArray(new String[res.size()]));

    //3. Run Presence Subscription task
    m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_Agent.getAuthenticPeer(), new Runnable() {
      public void run() {
        try {
          List<AgentIdentifier> subscribers = m_SubscriptionManager.listAllNotifyingSubscribers();
          AgentIdentifierList subscribedto = m_ServicePresenceProxy.getPresence().getSubscriptionsTo();
          for (Iterator<AgentIdentifier> iterator = subscribers.listIterator(); iterator.hasNext();) {
            AgentIdentifier aid = iterator.next();
            if (!subscribedto.contains(aid)) {
              m_ServicePresenceProxy.getPresenceService().requestSubscriptionTo(m_ServicePresenceProxy.getPresence(), aid);
            }
          }
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "activate()", ex);
        }
      }//run
    });
  }//activate

  public void deactivate() {
    if (m_ServiceMessagingProxy != null) {
      m_ServiceMessagingProxy.deactivate();
      m_ServiceMessagingProxy = null;
    }
    if (m_ServicePresenceProxy != null) {
      m_ServicePresenceProxy.deactivate();
      m_ServicePresenceProxy = null;
    }
  }//deactivate

  public void notifySubscribers(final EntryDescriptor ed, final boolean author) {
    m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_Agent.getAuthenticPeer(), new Runnable() {
      public void run() {
        try {

          //1. Prepare Message
          EditableInteractiveMessage im = m_ServiceMessagingProxy.getMessagingService().create(
              m_ServicePresenceProxy.getPresence(), InteractiveMessageTypes.NOTIFICATION,
              m_Services.getUUIDGeneratorService(ServiceMediator.NO_WAIT).getUID());

          ForumDescriptor fd = m_ForaManager.get(ed.getForumIdentifier());
          im.setConfirmationRequired(false);
          Locale lang = fd.getLanguage();
          im.setThread(fd.getMnemonic());
          im.setSubject(
              m_BundleMessages.get(
                  lang,
                  "NotificationManager.notification.subject",
                  "forumname",
                  fd.getName(),
                  "forumnum",
                  Integer.toString(fd.getSequenceNumber())
              )
          );
          im.setBody(lang,
              m_BundleMessages.get(
                  lang,
                  "NotificationManager.notification.body",
                  "author",
                  ed.getAuthor().getIdentifier(),
                  "caption",
                  ed.getCaption()
              )
          );

          //2. Prepare receivers per copy
          Set<AgentIdentifier> r = new HashSet<AgentIdentifier>(
              m_ResourceReceivers.get(fd.getMnemonic())
          );

          //Don't notify the author.
          if (!author) {
            r.remove(ed.getAuthor());
          }
          //3. Multicast (will also run through the execution service)
          m_ServiceMessagingProxy.getMessagingService().multicast(m_ServicePresenceProxy.getPresence(), im, r);

        } catch (MessagingException ex) {
          ex.printStackTrace();
          Activator.log().error(m_LogMarker, "notifySubscribers()", ex);
        }
      }//run
    });
  }//notifySubscribers

  //*** Handlers for adding elements that are created or changed at runtime ***//

  /**
   * Adds a forum that is created to the resources.
   *
   * @param mnemonic the forum mnemonic.
   */
  public void addForum(String mnemonic) {
    if (!m_Resources.contains(mnemonic)) {
      m_Resources.add(mnemonic);
      m_ServicePresenceProxy.getPresence().addResource(mnemonic);
      synchronized (m_ResourceReceivers) {
        m_ResourceReceivers.put(mnemonic, Collections.synchronizedSet(new HashSet<AgentIdentifier>()));
      }
    }
  }//addForum

  public void removeForum(String mnemonic) {
    if (!m_Resources.contains(mnemonic)) {
      m_Resources.remove(mnemonic);
      m_ServicePresenceProxy.getPresence().removeResource(mnemonic);
      synchronized (m_ResourceReceivers) {
        m_ResourceReceivers.remove(mnemonic);
      }
    }
  }//removeForum

  /**
   * Updates the notification state.
   *
   * @param s a subscription.
   */
  public void updateNotification(Subscription s) {
    boolean notify = s.isNotifying();
    AgentIdentifier aid = s.getSubscriber();
    //RequestSubscription if not subscribed
    if (notify) {
      if (!m_ServicePresenceProxy.getPresence().getSubscriptionsTo().contains(aid)) {
        m_ServicePresenceProxy.getPresenceService().requestSubscriptionTo(m_ServicePresenceProxy.getPresence(), aid);
      }
    }
    String mnemonic = m_ForaManager.resolveToMnemonic(s.getForumIdentifier());
    Set<AgentIdentifier> r = null;
    synchronized (m_ResourceReceivers) {
      r = m_ResourceReceivers.get(mnemonic);
    }
    if (r == null) {
      return;
    }
    boolean isnotified = r.contains(aid);
    if (notify && !isnotified) {
      if (m_PresentSubscribers.contains(aid)) {
        r.add(aid);
      }
    }
    if (!notify && isnotified) {
      r.remove(aid);
    }
  }//updateNotification

  //*** Keep track of incoming presences ***//
  protected void handlePresence(final AgentIdentifier aid) {
    m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_Agent.getAuthenticPeer(), new Runnable() {
      public void run() {
        if (m_PresentSubscribers.contains(aid)) {
          return;
        } else {
          m_PresentSubscribers.add(aid);
        }
        //1.Retrieve resources that should be notified for this agent
        try {
          List<String> mnemonics = m_SubscriptionManager.listNotifyingSubscriptions(aid);
          for (String mnemonic : mnemonics) {
            synchronized (m_ResourceReceivers) {
              Set<AgentIdentifier> r = m_ResourceReceivers.get(mnemonic);
              if (!r.contains(aid)) {
                r.add(aid);
              }
            }
          }
        } catch (DiscussionServiceException dex) {
          Activator.log().error(m_LogMarker, "handlePresence()", dex);
        }
      }//run
    }//runnable
    );//execute
  }//handlePresence

  protected void handleAbsence(final AgentIdentifier aid) {
    m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_Agent.getAuthenticPeer(), new Runnable() {
      public void run() {
        if (!m_PresentSubscribers.remove(aid)) {
          return;
        }
        //Remove from all
        synchronized (m_ResourceReceivers) {
          for (String mnem : m_ResourceReceivers.keySet()) {
            Set<AgentIdentifier> r = m_ResourceReceivers.get(mnem);
            r.remove(aid);
          }
        }
      }//run
    }//runnable
    );//execute
  }//handleAbsence

  protected void handleNoSubscription(final AgentIdentifier aid) {
    m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_Agent.getAuthenticPeer(), new Runnable() {
      public void run() {
        try {
          //cancel all notifications
          m_SubscriptionManager.cancelAllNotifications(aid);

        } catch (DiscussionServiceException dex) {
          Activator.log().error(m_LogMarker, "handleNoSubscription()", dex);
        }
      }//run
    }//runnable
    );//execute
  }//handleNoSubscription

  class NotificationPresenceListener
      extends PresenceServiceListenerAdapter {

    public void found(PresenceProxy p) {
      NotificationManager.this.handlePresence(p.getAgentIdentifier());
    }//found

    public void receivedPresence(PresenceProxy p) {
      NotificationManager.this.handlePresence(p.getAgentIdentifier());
    }//receivedPresence

    public void becamePresent(PresenceProxy p) {
      NotificationManager.this.handlePresence(p.getAgentIdentifier());
    }//becamePresent

    public void becameAbsent(PresenceProxy p) {
      NotificationManager.this.handleAbsence(p.getAgentIdentifier());
    }//becameAbsent

    public void statusUpdated(PresenceProxy p) {
      if (p.isAvailable()) {
        NotificationManager.this.handlePresence(p.getAgentIdentifier());
      }
      if (p.isUnavailable()) {
        NotificationManager.this.handleAbsence(p.getAgentIdentifier());
      }
    }//statusUpdated

    public void subscriptionDenied(AgentIdentifier aid, String reason) {
      //means that all notification needs to be cancelled
      NotificationManager.this.handleNoSubscription(aid);
    }//subscriptionDenied

    public void subscriptionCanceled(AgentIdentifier aid) {
      //means that all notification needs to be cancelled
      NotificationManager.this.handleNoSubscription(aid);
      //if cancelled, the service should also notify absence
    }//subscriptionCanceled

    public void requestedSubscription(PresenceProxy p) {
      m_ServicePresenceProxy.getPresenceService().denySubscription(m_ServicePresenceProxy.getPresence(), p.getAgentIdentifier(), "service");
    }//requestedSubscription

  }//inner class NotificationPresenceListener

}//class NotificationManager
