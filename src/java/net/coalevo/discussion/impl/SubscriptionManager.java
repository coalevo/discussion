/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.*;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.util.CacheMapExpelHandler;
import net.coalevo.foundation.util.LRUCacheMap;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.*;

/**
 * Implements a manager for subscriptions.
 * <p/>
 * This manager will handle subscriptions, working directly
 * with the backing store. For performance reasons subscriptions
 * will be cached, and updates propagated to the backing store (lastread)
 * when a cache entry is expelled (LRU; e.g. when it is outdated).
 * To force syncronization with the backing store, use {@link #sync()}.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class SubscriptionManager
    extends LRUCacheMap<String, Subscription> {

  private Marker m_LogMarker = MarkerFactory.getMarker(SubscriptionManager.class.getName());
  private DiscussionStore m_DiscussionStore;
  private LRUCacheMap<AgentIdentifier, List<ForumIdentifier>> m_AgentSubscriptionsCache;

  public SubscriptionManager(DiscussionStore ds, int ceiling, int ascceil) {
    super(ceiling);
    m_ExpelHandler = new ExpelHandler();
    m_DiscussionStore = ds;
    m_AgentSubscriptionsCache =
        new LRUCacheMap<AgentIdentifier, List<ForumIdentifier>>(ascceil);
  }//constructor

  public void setAgentSubscriptionsCacheCeiling(int size) {
    m_AgentSubscriptionsCache.setCeiling(size);
  }//setAgentSubscriptionCacheCeiling

  public void add(Subscription s) {
    put(s.getIdentifier(), s);
  }//add

  public void remove(Subscription s) {
    remove(s.getIdentifier());
  }//remove

  public boolean isSubscribed(ForumIdentifier fid, AgentIdentifier aid) {
    return (getSubscription(fid, aid) != null);
  }//isSubscribed

  public void updateLastRead(ForumIdentifier fid, AgentIdentifier aid, long time) {
    Subscription s = getSubscription(fid, aid);
    s.setLastRead(time);
  }//updateLastRead

  public Subscription getSubscription(ForumIdentifier fid, AgentIdentifier aid)
      throws NoSuchSubscriptionException {
    Subscription s = get(toID(fid, aid));
    if (s == null) {
      //get from DS
      ResultSet rs = null;
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("forum_id", fid.getIdentifier());
      params.put("subscriber_id", aid.getIdentifier());
      LibraryConnection lc = null;
      try {
        lc = m_DiscussionStore.leaseConnection();
        rs = lc.executeQuery("getSubscription", params);
        if (rs.next()) {
          s = new SubscriptionImpl(
              fid,
              aid,
              rs.getString(1),
              rs.getLong(2),
              rs.getLong(3),
              (rs.getShort(4) == 1));
        } else {
          return null;
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,Activator.getBundleMessages().get("SubscriptionManager.getfailed"), ex);
      } finally {
        SqlUtils.close(rs);
        m_DiscussionStore.releaseConnection(lc);
      }
      //add to cache
      if(s!=null) {
        add(s);
      }
    }
    return s;
  }//getSubscription

  public List<AgentIdentifier> listSubscribers(ForumIdentifier fid) {

    List<AgentIdentifier> subs = new ArrayList<AgentIdentifier>(50);
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      rs = lc.executeQuery("listSubscribers", params);
      while (rs.next()) {
        subs.add(m_DiscussionStore.getAgentIdentifier(rs.getString(1)));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,Activator.getBundleMessages().get("SubscriptionManager.listsubscribersfailed"), ex);
    } finally {
      SqlUtils.close(rs);
      m_DiscussionStore.releaseConnection(lc);
    }

    return subs;
  }//listSubscribers

  public List<ForumIdentifier> listSubscriptions(AgentIdentifier aid)
      throws DiscussionServiceException {

    List<ForumIdentifier> fids = m_AgentSubscriptionsCache.get(aid);
    if(fids == null) {
      fids = new ArrayList<ForumIdentifier>();
      //get from DS
      ResultSet rs = null;
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("subscriber_id", aid.getIdentifier());
      LibraryConnection lc = null;
      try {
        lc = m_DiscussionStore.leaseConnection();
        rs = lc.executeQuery("listSubscriptions", params);
        while (rs.next()) {
          fids.add(m_DiscussionStore.getForumIdentifier(rs.getString(1)));
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"listSubscriptions", ex);
        throw new DiscussionServiceException(Activator.getBundleMessages().get("SubscriptionManager.listsubscriptionsfailed"));
      } finally {
        SqlUtils.close(rs);
        m_DiscussionStore.releaseConnection(lc);
      }
      //cache
      m_AgentSubscriptionsCache.put(aid,fids);
    }
    //Activator.log().debug("listSubscriptions()::FIDS=" + fids.toString());
    return new ArrayList<ForumIdentifier>(fids); //return shallow copy
  }//listSubscriptions

  public void create(Subscription s) throws DiscussionServiceException {
    //Add to RDBMS
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", s.getForumIdentifier().getIdentifier());
    params.put("subscriber_id", s.getSubscriber().getIdentifier());
    params.put("note", s.getNote());
    params.put("created", "" + s.getCreated());
    params.put("lastread", "" + s.getLastRead());
    params.put("notifying", (s.isNotifying()) ? "1" : "0");

    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("createSubscription", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"create(Subscription)", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("SubscriptionManager.createfailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }
    add(s);
    List<ForumIdentifier> fids = m_AgentSubscriptionsCache.get(s.getSubscriber());
    if(fids !=null) {
      fids.add(s.getForumIdentifier());
    }    
  }//create

  public List<String> listNotifyingSubscriptions(AgentIdentifier aid)
      throws DiscussionServiceException {

      List<String> mnemonics = new ArrayList<String>();
      //get from DS
      ResultSet rs = null;
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("subscriber_id", aid.getIdentifier());
      LibraryConnection lc = null;
      try {
        lc = m_DiscussionStore.leaseConnection();
        rs = lc.executeQuery("listNotifyingSubscriptions", params);
        while (rs.next()) {
          mnemonics.add(rs.getString(1));
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"listNotifyingSubscriptions", ex);
        throw new DiscussionServiceException(Activator.getBundleMessages().get("SubscriptionManager.listnotifyingfailed"));
      } finally {
        SqlUtils.close(rs);
        m_DiscussionStore.releaseConnection(lc);
      }
    return mnemonics;
  }//listNotifyingSubscriptions

  public List<AgentIdentifier> listAllNotifyingSubscribers()
       throws DiscussionServiceException {

       List<AgentIdentifier> ids = new ArrayList<AgentIdentifier>();
       //get from DS
       ResultSet rs = null;
       LibraryConnection lc = null;
       try {
         lc = m_DiscussionStore.leaseConnection();
         rs = lc.executeQuery("listAllNotifyingSubscribers", null);
         while (rs.next()) {
          ids.add(m_DiscussionStore.getAgentIdentifier(rs.getString(1)));
         }
       } catch (Exception ex) {
         Activator.log().error(m_LogMarker,"listAllNotifyingSubscribers", ex);
         throw new DiscussionServiceException(Activator.getBundleMessages().get("SubscriptionManager.listallnotifyingfailed"));
       } finally {
         SqlUtils.close(rs);
         m_DiscussionStore.releaseConnection(lc);
       }
     return ids;
   }//listNotifyingSubscribers

  public void cancelAllNotifications(AgentIdentifier aid)
    throws DiscussionServiceException {

    //Remove from RDBMS
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("subscriber_id", aid.getIdentifier());
    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("cancelAllNotifications", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"cancelAllNotifications()::", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("SubscriptionManager.cancelnotificationsfailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }
  }//cancelAllNotifications

  public void cancel(ForumIdentifier fid, AgentIdentifier aid)
      throws DiscussionServiceException {

    //Remove from RDBMS
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("forum_id", fid.getIdentifier());
    params.put("subscriber_id", aid.getIdentifier());
    LibraryConnection lc = null;
    try {
      lc = m_DiscussionStore.leaseConnection();
      lc.executeUpdate("cancelSubscription", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"cancel(ForumIdentifier,AgentIdentifier)", ex);
      throw new DiscussionServiceException(Activator.getBundleMessages().get("SubscriptionManager.cancelfailed"), ex);
    } finally {
      m_DiscussionStore.releaseConnection(lc);
    }
    //update caches
    remove(toID(fid, aid));
    List<ForumIdentifier> fids = m_AgentSubscriptionsCache.get(aid);
    //Activator.log().debug("cancel()::FIDS BEFORE=" + fids.toString());
    if(fids !=null) {
      //Activator.log().debug("cancel()::REMOVING FID=" + fid.getIdentifier());
      fids.remove(fid);
      //Activator.log().debug("cancel()::FIDS AFTER=" + fids.toString());
    }
    //Activator.log().debug("cancel()::CANCELLED=" + toID(fid,aid));
  }//cancel

  public void sync() {
    synchronized (this) {
      for (Iterator iterator = entrySet().iterator(); iterator.hasNext();) {
        Map.Entry entry = (Map.Entry) iterator.next();
        update((Subscription) entry.getValue());
      }
    }
    
    //Clear out caches
    clear();
    m_AgentSubscriptionsCache.clear();
  }//sync

  private void update(Subscription s) {
    if(s == null) {
      return;
    }
    if (s.isDirty()) {
      //TODO: commit asynchronous through service?
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("forum_id", s.getForumIdentifier().getIdentifier());
      params.put("subscriber_id", s.getSubscriber().getIdentifier());
      params.put("note", s.getNote());
      params.put("lastread", "" + s.getLastRead());
      params.put("notifying", (s.isNotifying()) ? "1" : "0");
      LibraryConnection lc = null;
      try {
        lc = m_DiscussionStore.leaseConnection();
        lc.executeUpdate("updateSubscription", params);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"update(Subscription)", ex);
      } finally {
        m_DiscussionStore.releaseConnection(lc);
      }
      s.setDirty(false);
    }
  }//update

  private String toID(ForumIdentifier fid, AgentIdentifier aid) {
    return new StringBuilder().append(fid.getIdentifier()).append(aid.getIdentifier()).toString();
  }//toID

  class ExpelHandler
      implements CacheMapExpelHandler<String,Subscription> {

    public void expelled(Map.Entry<String,Subscription> entry) {
      final Subscription s = entry.getValue();

      update(s);
    }//expelled
  }//inner class ExpelHandler

}//class SubscriptionManager
