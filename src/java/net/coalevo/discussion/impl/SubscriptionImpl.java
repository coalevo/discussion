/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.discussion.model.Subscription;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.BaseIdentifiable;

/**
 * Implements {@link Subscription}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class SubscriptionImpl
    extends BaseIdentifiable
    implements Subscription {

  protected AgentIdentifier m_Subscriber;
  protected ForumIdentifier m_ForumIdentifier;
  protected String m_Note;
  protected long m_LastRead;
  protected boolean m_Notifying;
  protected long m_Created;
  private boolean m_Dirty;

  public SubscriptionImpl(ForumIdentifier fid, AgentIdentifier subscriber) {
    super(new StringBuilder().append(fid.getIdentifier()).append(subscriber.getIdentifier()).toString());
    m_Subscriber = subscriber;
    m_ForumIdentifier = fid;
    m_LastRead = -1;
    m_Notifying = false;
    m_Created = System.currentTimeMillis();
    m_Dirty = false;
  }//constructor

  public SubscriptionImpl(ForumIdentifier fid, AgentIdentifier subscriber, String note, long created, long lastread, boolean notifying) {
    super(new StringBuilder().append(fid.getIdentifier()).append(subscriber.getIdentifier()).toString());
    m_Subscriber = subscriber;
    m_ForumIdentifier = fid;
    m_LastRead = lastread;
    m_Notifying = notifying;
    m_Created = created;
    m_Note = note;
    m_Dirty = false;
  }//constructor


  public AgentIdentifier getSubscriber() {
    return m_Subscriber;
  }//getSubscriber

  public ForumIdentifier getForumIdentifier() {
    return m_ForumIdentifier;
  }//getForumIdentifier

  public String getNote() {
    return m_Note;
  }//getNote

  public void setNote(String note) {
    m_Note = note;
    m_Dirty = true;
  }//setNote

  public long getLastRead() {
    return m_LastRead;
  }//getLastRead

  public void setLastRead(long timestamp) {
    m_LastRead = timestamp;
    m_Dirty = true;
  }//setLastRead

  public boolean isNotifying() {
    return m_Notifying;
  }//isNotifying

  public void setNotifying(boolean b) {
    m_Notifying = b;
    m_Dirty = true;
  }//setNotifying

  public long getCreated() {
    return m_Created;
  }//getCreated

  public boolean isDirty() {
    return m_Dirty;
  }//isDirty

  public void setDirty(boolean b) {
    m_Dirty = b;
  }//setDirty

}//class SubscriptionImpl
