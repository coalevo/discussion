/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.*;
import net.coalevo.text.model.TransformationException;
import net.coalevo.text.model.UnsupportedTransformException;
import net.coalevo.text.service.TransformationService;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.KeywordAnalyzer;
import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.*;

/**
 * This class provides a Lucene based fulltext index for discussion
 * entries.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class EntryIndex {

  private Marker m_LogMarker = MarkerFactory.getMarker(EntryIndex.class.getName());
  private String m_Path;

  private Directory m_Directory;
  private Analyzer m_Analyzer;
  private QueryParser m_QueryParser;
  private IndexSearcher m_IndexSearcher;

  /**
   * Constructs this <tt>EntryIndex</tt>.
   *
   * @param path the path to the index store.
   */
  public EntryIndex(String path) {
    m_Path = path;
  }//constructor

  /**
   * Initializes this <tt>EntryIndex</tt>.
   *
   * @throws IOException if the iinitialization fails.
   */
  public void init() throws IOException {
    prepareAnalyzer();
    final File f = new File(m_Path);
    m_Directory = FSDirectory.getDirectory(m_Path, !f.exists());
    m_QueryParser = new QueryParser("body", m_Analyzer);
  }//init

  public synchronized void updateIndex(DiscussionStore ds)
      throws IOException {
    if (m_Directory == null) {
      init();
    }
    //get date when last modified
    long lastupdate = IndexReader.lastModified(m_Directory);
    Activator.log().debug(m_LogMarker,"updateIndex()::starting from " + lastupdate + " (" + new Date(lastupdate).toString() + ")");
     //open the index writer, just for updates
    IndexWriter iw = new IndexWriter(m_Directory, m_Analyzer, false);
    //get the forum manager for mnemonics
    ForaManager fm = ds.getForaManager();
    //use a hashmap to translate strings to fids
    HashMap<String, ForumIdentifier> fids = new HashMap<String, ForumIdentifier>();
    //TransformationService
    TransformationService transforms = Activator.getServices().getTransformationService(ServiceMediator.NO_WAIT);
    if (transforms == null) {
      return;
    }

    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = ds.leaseConnection();
      HashMap<String, String> params = new HashMap<String, String>();
      params.put("lastupdate", Long.toString(lastupdate));
      rs = lc.executeQuery("listEntriesFrom", params);

      String tmp = "";
      while (rs.next()) {

        try {
          Document d = new Document();

          //1.Forum id
          tmp = rs.getString(1);
          addField(d, "forum_id", tmp, true, false);
          if (!fids.containsKey(tmp)) {
            fids.put(tmp, new ForumIdentifierImpl(tmp));
          }
          tmp = fm.resolveToMnemonic(fids.get(tmp));
          addField(d, DiscussionTokens.ATTR_FORUM, tmp, true, false);

          //2. Entry id
          tmp = rs.getString(2);
          addField(d, DiscussionTokens.ATTR_ID, tmp, true, false);

          //3. Ancestor id
          tmp = rs.getString(3);
          addField(d, DiscussionTokens.ATTR_ANCESTOR, tmp, true, false);

          //4. Author id
          tmp = rs.getString(4);
          addField(d, DiscussionTokens.ATTR_AUTHOR, tmp, true, false);

          //5. Caption
          tmp = rs.getString(5);
          addField(d, DiscussionTokens.ELEMENT_CAPTION, tmp, false, true);

          //6. Tags
          tmp = rs.getString(6);
          addField(d, DiscussionTokens.ELEMENT_TAGS, tmp, false, true);

          //7. Day resolution times/dates
          addDayField(d, DiscussionTokens.ELEMENT_CREATED, rs.getLong(8));
          addDayField(d, DiscussionTokens.ELEMENT_APPROVED, rs.getLong(9));

          //8. Bool fields
          addBoolField(d, DiscussionTokens.ELEMENT_OFFICIAL, rs.getBoolean(10));
          addBoolField(d, DiscussionTokens.ELEMENT_ARCHIVED, rs.getBoolean(11));

          //9. Content
          String contentformat = rs.getString(7);
          tmp = rs.getString(13);

          //Activator.log().debug("INDEXING (" + contentformat + "):" + tmp);

          if (contentformat != null && contentformat.length() > 0) {
            try {
              tmp = transforms.transform(contentformat, "plain", tmp);
            } catch (UnsupportedTransformException ex) {
              Activator.log().error(m_LogMarker,"updateIndex()", ex);
            } catch (TransformationException ex) {
              Activator.log().error(m_LogMarker,"updateIndex()", ex);
            }
          }
          addField(d, "body", tmp, false, true);
          bulkAdd(iw, d);
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker,"updateIndex()", ex);
        }
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"updateIndex()", ex);
    } finally {
      SqlUtils.close(rs);
      ds.releaseConnection(lc);
    }

    //optimize
    iw.optimize();
    iw.close();

    //invalidate searcher
    m_IndexSearcher = null;
  }//updateIndex

  public synchronized void rebuildIndex(DiscussionStore ds)
      throws IOException {
    if (m_Directory == null) {
      init();
    }
    //open the index writer, deleting the old stuff
    IndexWriter iw = new IndexWriter(m_Directory, m_Analyzer, true);
    //get the forum manager for mnemonics
    ForaManager fm = ds.getForaManager();
    //use a hashmap to translate strings to fids
    HashMap<String, ForumIdentifier> fids = new HashMap<String, ForumIdentifier>();
    //TransformationService
    TransformationService transforms = Activator.getServices().getTransformationService(ServiceMediator.NO_WAIT);
    if (transforms == null) {
      return;
    }

    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = ds.leaseConnection();
      rs = lc.executeQuery("listEntries", null);

      String tmp = "";
      while (rs.next()) {

        try {
          Document d = new Document();

          //1.Forum id
          tmp = rs.getString(1);
          addField(d, "forum_id", tmp, true, false);
          if (!fids.containsKey(tmp)) {
            fids.put(tmp, new ForumIdentifierImpl(tmp));
          }
          tmp = fm.resolveToMnemonic(fids.get(tmp));
          addField(d, DiscussionTokens.ATTR_FORUM, tmp, true, false);

          //2. Entry id
          tmp = rs.getString(2);
          addField(d, DiscussionTokens.ATTR_ID, tmp, true, false);

          //3. Ancestor id
          tmp = rs.getString(3);
          addField(d, DiscussionTokens.ATTR_ANCESTOR, tmp, true, false);

          //4. Author id
          tmp = rs.getString(4);
          addField(d, DiscussionTokens.ATTR_AUTHOR, tmp, true, false);

          //5. Caption
          tmp = rs.getString(5);
          addField(d, DiscussionTokens.ELEMENT_CAPTION, tmp, false, true);

          //6. Tags
          tmp = rs.getString(6);
          addField(d, DiscussionTokens.ELEMENT_TAGS, tmp, false, true);

          //7. Day resolution times/dates
          addDayField(d, DiscussionTokens.ELEMENT_CREATED, rs.getLong(8));
          addDayField(d, DiscussionTokens.ELEMENT_APPROVED, rs.getLong(9));

          //8. Bool fields
          addBoolField(d, DiscussionTokens.ELEMENT_OFFICIAL, rs.getBoolean(10));
          addBoolField(d, DiscussionTokens.ELEMENT_ARCHIVED, rs.getBoolean(11));

          //9. Content
          String contentformat = rs.getString(7);
          tmp = rs.getString(13);

          //Activator.log().debug("INDEXING (" + contentformat + "):" + tmp);

          if (contentformat != null && contentformat.length() > 0) {
            try {
              tmp = transforms.transform(contentformat, "plain", tmp);
            } catch (UnsupportedTransformException ex) {
              Activator.log().error(m_LogMarker,"rebuildIndex()", ex);
            } catch (TransformationException ex) {
              Activator.log().error(m_LogMarker,"rebuildIndex()", ex);
            }
          }
          addField(d, "body", tmp, false, true);
          bulkAdd(iw, d);
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker,"rebuildIndex()", ex);
        }
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"rebuildIndex()", ex);
    } finally {
      SqlUtils.close(rs);
      ds.releaseConnection(lc);
    }

    //optimize
    iw.optimize();
    iw.close();

    //invalidate searcher
    m_IndexSearcher = null;
  }//rebuildIndex

  private void addField(Document udd, String name, String txt, boolean store, boolean tokenize) {
    if (txt != null) {
      Field f = new Field(
          name,
          txt,
          (store) ? Field.Store.YES : Field.Store.NO,
          (tokenize) ? Field.Index.TOKENIZED : Field.Index.UN_TOKENIZED
      );
      udd.add(f);
    }
  }//addField

  private void addDayField(Document d, String name, long ts) {
    String tmp = DateTools.timeToString(ts, DateTools.Resolution.DAY);
    d.add(
        new Field(name, tmp, Field.Store.NO, Field.Index.UN_TOKENIZED)
    );
  }//addDayField

  private void addBoolField(Document d, String name, Boolean b) {
    String tmp = b.toString();
    d.add(
        new Field(name, tmp, Field.Store.NO, Field.Index.UN_TOKENIZED)
    );
  }//addBoolField

  public boolean exists() throws IOException {
    return IndexReader.indexExists(m_Directory);
  }//exists

  /**
   * Adds a {@link EntryDescriptor} and the corresponding {@link Content}
   * instance to this index, using an already open writer.
   * </p>
   *
   * @param iw the IndexWriter.
   * @param d  the Document to be added.
   * @throws IOException if an I/O Operation fails.
   */
  private void bulkAdd(IndexWriter iw, Document d)
      throws IOException {
    iw.addDocument(d);
  }//bulkAdd

  /**
   * Returns an <tt>IndexReader</tt> for the wrapped index.
   *
   * @return an <tt>IndexReader</tt> instance.
   * @throws IOException if an I/O error occurs.
   */
  public IndexReader getReader() throws IOException {
    return IndexReader.open(m_Directory);
  }//getReader

  /**
   * Returns an <tt>IndexSearcher</tt> for the wrapped index.
   *
   * @return an <tt>IndexSearcher</tt> instance.
   * @throws IOException if an I/O error occurs.
   */
  public IndexSearcher getSearcher() throws IOException {
    return new IndexSearcher(m_Directory);
  }//getSearcher

  /**
   * Prepares the analyzer.
   */
  private void prepareAnalyzer() {
    PerFieldAnalyzerWrapper pa = new PerFieldAnalyzerWrapper(
        new StandardAnalyzer());
    //use certain analyzers for specific fields
    pa.addAnalyzer(DiscussionTokens.ATTR_ID, new KeywordAnalyzer());
    pa.addAnalyzer(DiscussionTokens.ATTR_ANCESTOR, new KeywordAnalyzer());
    pa.addAnalyzer(DiscussionTokens.ATTR_AUTHOR, new KeywordAnalyzer());
    pa.addAnalyzer(DiscussionTokens.ATTR_FORUM, new KeywordAnalyzer());

    m_Analyzer = pa;
  }//prepareAnalyzer

  public List<EntryHit> searchEntries(String query,
                                      List<String> fora,
                                      int numhits) {
    List<EntryHit> s = new ArrayList<EntryHit>(numhits);
    StringBuilder fqstr = new StringBuilder();
    fqstr.append(DiscussionTokens.ATTR_FORUM);
    fqstr.append(":(");

    for (Iterator<String> iter = fora.listIterator(); iter.hasNext();) {
      fqstr.append(iter.next());
      if (iter.hasNext()) {
        fqstr.append(" OR ");
      } else {
        fqstr.append(")");
      }
    }
    //Activator.log().debug("FILTER FOR QUERY: " + fqstr.toString());
    try {
      Query q = null;
      Query qf = null;
      //TODO: probably pool may not scale
      synchronized (m_QueryParser) {
        q = m_QueryParser.parse(query);
        qf = m_QueryParser.parse(fqstr.toString());
      }


      FilteredQuery fq = new FilteredQuery(q, new QueryFilter(qf));
      //Activator.log().debug("QUERY: "  + fq.toString());

      if (m_IndexSearcher == null) {
        m_IndexSearcher = getSearcher();
      }
      Hits hits = m_IndexSearcher.search(fq);
      //Activator.log().debug("HITS: " + hits.length());

      //Min of max result number and hits length.
      int to = Math.min(numhits, hits.length());
      for (Iterator iterator = hits.iterator(); iterator.hasNext(); to--) {
        Hit h = (Hit) iterator.next();
        s.add(
            new SimpleEntryHit(
                h.get("forum_id"),
                h.get(DiscussionTokens.ATTR_ID),
                h.getScore()
            )
        );
        if (to == 0) {
          break;
        }
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"searchEntries()", ex);
    }
    return s;
  }//searchEntries

  static class SimpleEntryHit implements EntryHit {

    private String m_EntryIdentifier;
    private ForumIdentifier m_ForumIdentifier;
    private float m_Score;

    public SimpleEntryHit(String fid, String eid, float score) {
      m_ForumIdentifier = new ForumIdentifierImpl(fid);
      m_EntryIdentifier = eid;
      m_Score = score;
    }//constructor

    public String getEntryIdentifier() {
      return m_EntryIdentifier;
    }//getEntryIdentifier

    public ForumIdentifier getForumIdentifier() {
      return m_ForumIdentifier;
    }//getForumIdentifier

    public float getScore() {
      return m_Score;
    }//getScore

    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      SimpleEntryHit that = (SimpleEntryHit) o;

      if (m_EntryIdentifier != null ? !m_EntryIdentifier.equals(that.m_EntryIdentifier) : that.m_EntryIdentifier != null)
        return false;
      if (m_ForumIdentifier != null ? !m_ForumIdentifier.equals(that.m_ForumIdentifier) : that.m_ForumIdentifier != null)
        return false;

      return true;
    }//equals

    public int hashCode() {
      int result;
      result = (m_EntryIdentifier != null ? m_EntryIdentifier.hashCode() : 0);
      result = 31 * result + (m_ForumIdentifier != null ? m_ForumIdentifier.hashCode() : 0);
      return result;
    }//hashCode

  }//inner class SimpleEntryHit

}//class EntryIndex
