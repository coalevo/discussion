/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.impl;

import net.coalevo.discussion.model.EditableForumDescriptor;
import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.foundation.model.AgentIdentifier;

import java.util.Locale;
import java.util.Set;

/**
 * Implemens {@link EditableForumDescriptor}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class EditableForumDescriptorImpl
    extends ForumDescriptorImpl
    implements EditableForumDescriptor {

  public EditableForumDescriptorImpl(ForumIdentifier fid, AgentIdentifier creator) {
    m_ForumIdentifier = fid;
    m_Language = Locale.getDefault();
    m_Creator = creator;
    m_Moderator = creator;
    m_Created = System.currentTimeMillis();
    m_Modified = m_Created;
    m_InviteOnly = false;
    m_ModeratedStrict = false;
    m_AnonymousAllowed = false;
    m_Public = false;
    m_MaxActive = 200;
  }//EditableForumDescriptor

  public void setName(String str) {
    m_Name = str;
  }//setName

  public void setMnemonic(String str) {
    m_Mnemonic = str;
  }//setMnemonic

  public void setLanguage(Locale l) {
    m_Language = l;
  }//setLanguage

  public void setInfoFormat(String str) {
    m_InfoFormat = str;
  }//setInfoFormat

  public void setSequenceNumber(int n) {
    m_SequenceNumber = n;
  }//setSequenceNumber

  public void addTag(String tag) {
    m_Tags.add(tag);
  }//addTag

  public void removeTag(String tag) {
    m_Tags.remove(tag);
  }//removeTag

  public void setTags(Set<String> tags) {
    m_Tags = tags;
  }//setTags

  public void setModeratedStrict(boolean b) {
    m_ModeratedStrict = b;
  }//setModeratedStrict

  public void setAnonymousAllowed(boolean b) {
    m_AnonymousAllowed = b;
  }//setAnonymousAllowed

  public void setInviteOnly(boolean b) {
    m_InviteOnly = b;
  }//setInviteOnly

  public void setPublic(boolean b) {
    m_Public = b;
  }//setPublic

  public void setMaxActive(int num) {
    m_MaxActive = Math.max(num,200);
  }//setMaxActive
  
  public void setModerator(AgentIdentifier aid) {
    m_Moderator = aid;
  }//setModerator

  public void setModified() {
    m_Modified = System.currentTimeMillis();
  }//setModified

  public boolean verify() {
    return (
        m_ForumIdentifier != null
            && m_Name != null
            && m_Name.length() > 0
            && m_Mnemonic != null
            && m_Mnemonic.length() > 0
            && m_SequenceNumber > 0
            && m_Language != null
    );
  }//verify

  public ForumDescriptorImpl toForumDescriptor() {
    ForumDescriptorImpl fdi = new ForumDescriptorImpl();
    fdi.m_ForumIdentifier = this.m_ForumIdentifier;
    fdi.m_SequenceNumber = this.m_SequenceNumber;
    fdi.m_Mnemonic = this.m_Mnemonic;
    fdi.m_Name = this.m_Name;
    fdi.m_Language = this.m_Language;
    fdi.m_Creator = this.m_Creator;
    fdi.m_Moderator = this.m_Moderator;
    fdi.m_Tags = this.m_Tags;
    fdi.m_InfoFormat = this.m_InfoFormat;
    fdi.m_Created = this.m_Created;
    fdi.m_Modified = this.m_Modified;
    fdi.m_InviteOnly = this.m_InviteOnly;
    fdi.m_ModeratedStrict = this.m_ModeratedStrict;
    fdi.m_AnonymousAllowed = this.m_AnonymousAllowed;
    fdi.m_Public = this.m_Public;
    fdi.m_MaxActive = this.m_MaxActive;
    return fdi;
  }//toForumDescriptor

}//class EditableForumDescriptorImpl
