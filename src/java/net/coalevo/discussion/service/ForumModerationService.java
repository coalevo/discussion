/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.service;

import net.coalevo.discussion.model.*;
import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Service;

import java.util.List;

/**
 * Defines the forum moderation service.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface ForumModerationService
    extends Service {

  //*** Forum Handling ***//

  /**
   * Returns an editable forum descriptor for updates.
   * <p/>
   * This method should be followed by {@link #commitForumUpdate(Agent,EditableForumDescriptor)}
   * to commit the update (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return an {@link EditableForumDescriptor}
   * @throws SecurityException     if the calling {@link Agent} is not authentic or
   *                               not authorized.
   * @throws IllegalStateException if the transaction was already started for the given forum.
   * @see #commitForumUpdate(Agent,EditableForumDescriptor)
   */
  public EditableForumDescriptor beginForumUpdate(Agent caller, ForumIdentifier fid)
      throws SecurityException, IllegalStateException;

  /**
   * Commits a forum update.
   * <p/>
   * This method should be preceded by {@link #beginForumUpdate(Agent,ForumIdentifier)}
   * to begin an update (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param efd    an {@link EditableForumDescriptor}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws IllegalStateException      if the transaction was not started
   *                                    by {@link #beginForumUpdate(Agent,ForumIdentifier)}.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   * @see #beginForumUpdate(Agent,ForumIdentifier)
   */
  public void commitForumUpdate(Agent caller, EditableForumDescriptor efd)
      throws SecurityException, IllegalStateException, DiscussionServiceException;

  /**
   * Cancels a forum update.
   * <p/>
   *
   * @param caller the requesting {@link Agent}.
   * @param efd    an {@link EditableForumDescriptor}.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public void cancelForumUpdate(Agent caller, EditableForumDescriptor efd)
      throws SecurityException;


  /**
   * Returns an {@link EditableContent} instance to update the forum info.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return an {@link EditableContent} instance.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws IllegalStateException      if the transaction was already started for the given forum
   *                                    by {@link #beginForumUpdate(Agent,ForumIdentifier)}.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public EditableContent beginForumInfoUpdate(Agent caller, ForumIdentifier fid)
      throws SecurityException, IllegalStateException, DiscussionServiceException;


  /**
   * Returns an {@link EditableContent} instance to update the forum info.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param ec     an {@link EditableContent} instance.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws IllegalStateException      if the transaction was not started
   *                                    by {@link #beginForumUpdate(Agent,ForumIdentifier)}.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */

  public void commitForumInfoUpdate(Agent caller, ForumIdentifier fid, EditableContent ec)
      throws SecurityException, IllegalStateException, DiscussionServiceException;


  /**
   * Cancels a forum info update.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public void cancelForumInfoUpdate(Agent caller, ForumIdentifier fid)
      throws SecurityException;

  //*** Ban Handling ***//

  /**
   * Returns a {@link Ban} to be created.
   * <p/>
   * This method should be followed by {@link #commitBan(Agent,Ban)}
   * to commit the update (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param aid    the banned {@link Agent}.
   * @return a {@link Ban} instance.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @throws BannedException   if the agent is already banned.
   * @see #commitBan(Agent,Ban)
   */
  public Ban beginBanCreate(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException, BannedException;

  /**
   * Commits a {@link Ban}.
   * <p/>
   * This method should be preceded by {@link #beginBanCreate(Agent,ForumIdentifier,AgentIdentifier)}
   * to begin a create (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param ban    a {@link Ban}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws IllegalStateException      if the transaction was not started by
   *                                    {@link #beginBanCreate(Agent,ForumIdentifier,AgentIdentifier)}.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   * @see #beginBanCreate(Agent,ForumIdentifier,AgentIdentifier)
   */
  public void commitBan(Agent caller, Ban ban)
      throws SecurityException, IllegalStateException, DiscussionServiceException;

  /**
   * Cancels a {@link Ban} create transaction.
   * <p/>
   *
   * @param caller the requesting {@link Agent}.
   * @param ban    a {@link Ban}.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public void cancelBanCreate(Agent caller, Ban ban)
      throws SecurityException;

  /**
   * Tests if a given agent is banned.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param aid    the banned {@link Agent}.
   * @return true if banned, false otherwise.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public boolean isBanned(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException;

  /**
   * Returns a specific {@link Ban}.
   * <p/>
   * Note: you might use this instance to update the content.
   * These updates will be propagated to the store when the cache is
   * synced.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param aid    the banned {@link Agent}.
   * @return the corresponding {@link Ban}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   * @throws NoSuchBanException         if the specified agent is not banned from the given forum.
   */
  public Ban getBan(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException, DiscussionServiceException, NoSuchBanException;

  /**
   * Lists bans for the given forum.
   *
   * @param caller the requesting {@link Agent}
   * @param fid    a {@link ForumIdentifier}.
   * @return a <tt>List</tt> of {@link Ban} instances.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public List<AgentIdentifier> listBanned(Agent caller, ForumIdentifier fid)
      throws SecurityException;


  /**
   * Removes a {@link Ban} from a given agent and forum.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param aid    the banned {@link Agent}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void removeBan(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException, DiscussionServiceException;

  //*** END Ban Handling ***//

  //*** Link Handling ***//

  /**
   * Returns an editable link updates.
   * <p/>
   * This method should be followed by {@link #commitLinkUpdate(Agent,EditableLink)}
   * to commit the update (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param id     the identifier of the link.
   * @return an {@link EditableLink}
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   * @see #commitLinkUpdate(Agent,EditableLink)
   */
  public EditableLink beginLinkUpdate(Agent caller, ForumIdentifier fid, String id)
      throws SecurityException, DiscussionServiceException;

  /**
   * Commits a link update.
   * <p/>
   * This method should be preceded by {@link #beginLinkUpdate(Agent,ForumIdentifier,String)}
   * to begin an update (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param link   an {@link EditableLink}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws IllegalStateException      if the transaction was not started by
   *                                    {@link #beginLinkUpdate(Agent,ForumIdentifier,String)}
   * @throws DiscussionServiceException if an error occurs while executing this action.
   * @see #beginLinkUpdate(Agent,ForumIdentifier,String)
   */
  public void commitLinkUpdate(Agent caller, EditableLink link)
      throws SecurityException, DiscussionServiceException;

  /**
   * Cancel the update of an existing {@link Link}.
   * <p/>
   * This method should be preceded by {@link #beginLinkUpdate(Agent,ForumIdentifier,String)}
   * to begin the update (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param el     an {@link EditableLink} instance.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @see #beginLinkUpdate(Agent,ForumIdentifier,String)
   */
  public void cancelLinkUpdate(Agent caller, EditableLink el)
      throws SecurityException;

  /**
   * Lists the Links of a given forum pending for approval.
   *
   * @param caller the requesting {@link Agent}
   * @param fid    a {@link ForumIdentifier}.
   * @return a <tt>List</tt> of {@link String} instances with link identifiers.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public List<String> listPendingLinks(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException;

  /**
   * Approves a pending link.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param linkid     the identifier of the link.
   * @param notify true if the author is to be notified, false otherwise.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void approveLink(Agent caller, ForumIdentifier fid, String linkid, boolean notify)
      throws SecurityException, DiscussionServiceException;

  /**
   * Disapproves a pending link.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param linkid     the identifier of the link.
   * @param notify true if the author is to be notified, false otherwise.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void disapproveLink(Agent caller, ForumIdentifier fid, String linkid, boolean notify)
      throws SecurityException, DiscussionServiceException;

  /**
   * Moves a {@link Link} from a given forum.
   * <p/>
   * The link will be pending for approval by the moderator of the destination
   * forum.
   *
   * @param caller the requesting {@link Agent}.
   * @param from   the {@link ForumIdentifier} of the actual forum.
   * @param to     the {@link ForumIdentifier} of the destination forum.
   * @param id     the identifier of the entry.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void moveLink(Agent caller, ForumIdentifier from, ForumIdentifier to, String id)
      throws SecurityException, DiscussionServiceException;

  /**
   * Removes a {@link Link} from a given forum.
   * <p/>
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param id     the identifier of the link.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void removeLink(Agent caller, ForumIdentifier fid, String id)
      throws SecurityException, DiscussionServiceException;

  /**
   * Checks the links in the given forum.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void checkLinks(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException;

  //*** END Link Handling ***//

  //*** Entry Handling ***//

  /**
   * Lists the discussion entries of a given forum pending for approval.
   *
   * @param caller the requesting {@link Agent}
   * @param fid    a {@link ForumIdentifier}.
   * @return a <tt>List</tt> of entry .
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public List<String> listPendingEntries(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException;

  /**
   * Approves a pending discussion entry.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param id     the identifier of the entry.
   * @param notify true if the author is to be notified, false otherwise.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void approveEntry(Agent caller, ForumIdentifier fid, String id, boolean notify)
      throws SecurityException, DiscussionServiceException;

  /**
   * Disapproves a pending discussion entry.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param id     the identifier of the entry.
   * @param notify true if the author is to be notified, false otherwise.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void disapproveEntry(Agent caller, ForumIdentifier fid, String id, boolean notify)
      throws SecurityException, DiscussionServiceException;

  /**
   * Moves an entry.
   * <p/>
   *
   * @param caller the requesting {@link Agent}.
   * @param from   the {@link ForumIdentifier} of the actual forum.
   * @param to     the {@link ForumIdentifier} of the destination forum.
   * @param id     the identifier of the entry.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void moveEntry(Agent caller, ForumIdentifier from, ForumIdentifier to, String id)
      throws SecurityException, DiscussionServiceException;

  /**
   * Removes an entry.
   * <p/>
   *
   * @param caller the requesting {@link Agent}.
   * @param ed     the {@link EntryDescriptor} of the entry.
   * @param notify true if the author is to be notified, false otherwise.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void removeEntry(Agent caller, EntryDescriptor ed, boolean notify)
      throws SecurityException, DiscussionServiceException;

  /**
   * Tests if the given {@link Agent} is allowed to moderate the given forum.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return true if allowed, false otherwise.
   */
  public boolean isAllowedToModerate(Agent caller, ForumIdentifier fid);

}//interface ForumModerationService
