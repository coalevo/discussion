/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.service;

/**
 * Defines a tag interface for the registration of the
 * unified bundle configuration.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 * @see net.coalevo.foundation.util.BundleConfiguration
 */
public interface DiscussionConfiguration {

  public static final String DATA_SOURCE_KEY = "datasource";
  public static final String DATA_DIR_KEY = "datadir";
  public static final String CONNECTION_POOLSIZE_KEY = "connections.poolsize";
  public static final String FORUMIDENTIFIERS_CACHESIZE_KEY = "cache.forumidentifiers";
  public static final String AGENTIDENTIFIERS_CACHESIZE_KEY = "cache.agentidentifiers";
  public static final String BANS_CACHESIZE_KEY = "cache.bans";
  public static final String SUBSCRIPTIONS_CACHESIZE_KEY = "cache.subscriptions";
  public static final String AGENTSUBSCRIPTIONS_CACHESIZE_KEY = "cache.agentsubscriptions";
  public static final String ENTRYDESCRIPTORS_CACHESIZE_KEY = "cache.entrydescriptors";
  public static final String ENTRYCONTENT_CACHESIZE_KEY = "cache.entrycontent";
  public static final String FORUMENTRYLISTS_CACHESIZE_KEY = "cache.forumentrylists";
  public static final String FORUMINFOS_CACHESIZE_KEY = "cache.foruminfos";
  public static final String LINKS_CACHESIZE_KEY = "cache.links";
  public static final String FORUMLINKLISTS_CACHESIZE_KEY = "cache.forumlinklists";
  public static final String LINKS_CHECKINTERVAL_KEY = "links.checkinterval";
  
}//class DicussionConfiguration
