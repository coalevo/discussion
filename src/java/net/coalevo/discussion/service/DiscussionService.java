/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.service;

import net.coalevo.discussion.model.*;
import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Maintainable;
import net.coalevo.foundation.model.Service;

import java.util.List;

/**
 * Defines the discussion service.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface DiscussionService
    extends Service, Maintainable {

  //*** Fora Handling ***//

  /**
   * Returns an editable forum descriptor for creation.
   * <p/>
   * This method should be followed by {@link #commitForum(Agent,EditableForumDescriptor)}
   * to commit the update (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @return an {@link EditableForumDescriptor}
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @see #commitForum(Agent,EditableForumDescriptor)
   */
  public EditableForumDescriptor beginForumCreate(Agent caller)
      throws SecurityException;

  /**
   * Cancels a forum creation.
   * <p/>
   * This method should be preceded by {@link #beginForumCreate(Agent)}
   * to begin a create (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param efd    an {@link EditableForumDescriptor}.
   * @throws SecurityException     if the calling {@link Agent} is not authentic or
   *                               not authorized.
   * @throws IllegalStateException if the transaction was not started
   *                               by {@link #beginForumCreate(Agent)}.
   * @see #beginForumCreate(Agent)
   */
  public void cancelForumCreate(Agent caller, EditableForumDescriptor efd)
      throws SecurityException, IllegalStateException;

  /**
   * Commits a forum creation.
   * <p/>
   * This method should be preceded by {@link #beginForumCreate(Agent)}
   * to begin a create (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param efd    an {@link EditableForumDescriptor}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws IllegalStateException      if the transaction was not started
   *                                    by {@link #beginForumCreate(Agent)}.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   * @see #beginForumCreate(Agent)
   */
  public void commitForum(Agent caller, EditableForumDescriptor efd)
      throws SecurityException, IllegalStateException, DiscussionServiceException;

  /**
   * Returns a forum descriptor by its identifier.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return a {@link ForumDescriptor}.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or
   *                              not authorized.
   * @throws NoSuchForumException if a forum with the given name does not exist.
   */
  public ForumDescriptor getForumDescriptor(Agent caller, ForumIdentifier fid)
      throws SecurityException, NoSuchForumException;

  /**
   * Returns a {@link ForumDescriptor} by sequence number.
   *
   * @param caller the requesting {@link Agent}.
   * @param seqnum a forum sequence number.
   * @return a {@link ForumDescriptor}.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or
   *                              not authorized.
   * @throws NoSuchForumException if a forum with the given name does not exist.
   */
  public ForumDescriptor getForumDescriptorBySequenceNumber(Agent caller, int seqnum)
      throws SecurityException, NoSuchForumException;

  /**
   * Returns a forum descriptor by name.
   *
   * @param caller the requesting {@link Agent}.
   * @param name   a forum name.
   * @return a {@link ForumDescriptor}.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or
   *                              not authorized.
   * @throws NoSuchForumException if a forum with the given name does not exist.
   */
  public ForumDescriptor getForumDescriptorByName(Agent caller, String name)
      throws SecurityException, NoSuchForumException;

  /**
   * Returns a forum descriptor by mnemonic.
   *
   * @param caller   the requesting {@link Agent}.
   * @param mnemonic a forum mnemonic.
   * @return a {@link ForumDescriptor}.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or
   *                              not authorized.
   * @throws NoSuchForumException if a forum with the given name does not exist.
   */
  public ForumDescriptor getForumDescriptorByMnemonic(Agent caller, String mnemonic)
      throws SecurityException, NoSuchForumException;
  
  /**
   * @param caller the requesting {@link Agent}.
   * @param list   a <tt>List</tt> of {@link ForumIdentifier} instances.
   * @param names  true if resolve to names, false if to mnemonics.
   * @return a <tt>List</tt> of string instances, either names or mnemonics.
   */
  public List<String> resolveIdentifiers(Agent caller, List<ForumIdentifier> list, boolean names);

  /**
   * Lists the existing foras.
   *
   * @param caller the requesting {@link Agent}.
   * @return a <tt>List</tt> of {@link ForumDescriptor} instances.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public List<ForumDescriptor> listFora(Agent caller)
      throws SecurityException;

  /**
   * Lists existing public foras.
   *
   * @param caller the requesting {@link Agent}.
   * @return a <tt>List</tt> of {@link ForumDescriptor} instances.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public List<ForumDescriptor> listPublicFora(Agent caller)
      throws SecurityException;

  /**
   * Lists all forum names.
   *
   * @param caller the requesting {@link Agent}.
   * @return a <tt>List</tt> of <tt>String</tt> instances.
   */
  public List<String> listForaNames(Agent caller);

  /**
   * Lists all forum mnemonics.
   *
   * @param caller the requesting {@link Agent}.
   * @return a <tt>List</tt> of <tt>String</tt> instances.
   */
  public List<String> listForaMnemonics(Agent caller);

  /**
   * Lists all fora with new entries from a given timestamp.
   *
   * @param caller    the requesting {@link Agent}.
   * @param timestamp the timestamp as long millis from UTC.
   * @return a <tt>List</tt> of {@link ForumIdentifier} instances.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public List<ForumIdentifier> listForaWithNewEntries(Agent caller, long timestamp)
      throws SecurityException, DiscussionServiceException;

  /**
   * Destroys a forum and all its related information.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void destroyForum(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException;

  /**
   * Assigns the forum moderator of a given forum.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param aid    the {@link AgentIdentifier} of the moderator.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   * @throws NoSuchForumException       if a forum with the given name does not exist.
   */
  public void assignForumModerator(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException, DiscussionServiceException, NoSuchForumException;

  /**
   * Returns the forum info of a given forum.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return the forum info as {@link Content}.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public Content getForumInfo(Agent caller, ForumIdentifier fid)
      throws SecurityException;

  //*** END Fora Handling ***//

  //*** Subscription Handling ***//

  /**
   * Lists all the subscribers of a given forum.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return a <tt>List</tt> of {@link AgentIdentifier} instances.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public List<AgentIdentifier> listSubscribers(Agent caller, ForumIdentifier fid)
      throws SecurityException;

  /**
   * @param caller the requesting {@link Agent}.
   * @param aid    an {@link AgentIdentifier}.
   * @return a <tt>List</tt> of {@link ForumIdentifier} instances.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public List<ForumIdentifier> listSubscriptions(Agent caller, AgentIdentifier aid)
      throws SecurityException, DiscussionServiceException;

  /**
   * Returns a {@link Subscription} for subscription.
   * <p/>
   * This method should be followed by {@link #commitSubscription(Agent,Subscription)}
   * to commit the update (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param aid    the banned {@link Agent}.
   * @return a {@link Subscription} instance.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @throws BannedException   if the specified agent is banned.
   * @see #commitSubscription(Agent,Subscription)
   */
  public Subscription beginSubscriptionCreate(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException, BannedException;

  /**
   * Commits a {@link Subscription}.
   * <p/>
   * This method should be preceded by {@link #beginSubscriptionCreate(Agent,ForumIdentifier,AgentIdentifier)}
   * to begin acreate (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param s      a {@link Subscription}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws IllegalStateException      if the transaction was not started by
   *                                    {@link #beginSubscriptionCreate(Agent,ForumIdentifier,AgentIdentifier)}.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   * @see #beginSubscriptionCreate(Agent,ForumIdentifier,AgentIdentifier)
   */
  public void commitSubscription(Agent caller, Subscription s)
      throws SecurityException, IllegalStateException, DiscussionServiceException;

  /**
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param aid    the banned {@link Agent}.
   * @return a {@link Subscription} instance.
   * @throws SecurityException           if the calling {@link Agent} is not authentic or
   *                                     not authorized.
   * @throws DiscussionServiceException  if an error occurs while executing this action.
   * @throws NoSuchSubscriptionException if such a subscription does not exist.
   */
  public Subscription getSubscription(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException, DiscussionServiceException, NoSuchSubscriptionException;

  /**
   * Tracks a subscription update and ensures that notification is updated.
   * @param caller the requesting {@link Agent}.
   * @param s a {@link Subscription}.
   * @throws SecurityException   if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   */
   public void updateSubscription(Agent caller, Subscription s)
       throws SecurityException;

  /**
   * Tests if a given agent is subscribed.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param aid    the identifier of an {@link Agent}.
   * @return true if subscribed, false otherwise.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public boolean isSubscribed(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException;

  /**
   * Cancels the subscription of an agent.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param aid    the identifier of an {@link Agent}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void cancelSubscription(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException, DiscussionServiceException;

  /**
   * Tests if a given forum is invite only.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid a {@link ForumIdentifier}.
   * @return true if inivteonly, false otherwise.
   * @throws NoSuchForumException if such a forum does not exist.
   */
  public boolean isInviteOnly(Agent caller, ForumIdentifier fid) throws NoSuchForumException;


  //*** Ban Handling (see ForumModerationService for more functionality) ***//

  /**
   * Tests if a given agent is banned.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param aid    the banned {@link Agent}.
   * @return true if banned, false otherwise.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public boolean isBanned(Agent caller, ForumIdentifier fid, AgentIdentifier aid)
      throws SecurityException;

  //*** END Ban Handling ***//

  //*** Link Handling ***//

  /**
   * Begin to create a new {@link Link}.
   * <p/>
   * This method should be followed by {@link #commitLink(Agent,EditableLink)}
   * to commit the update (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return an {@link EditableLink} instance.
   * @throws SecurityException      if the calling {@link Agent} is not authentic or
   *                                not authorized.
   * @throws BannedException        if the caller is banned.
   * @throws NotSubscribedException if the caller is not subscribed.
   * @see #commitLink(Agent,EditableLink)
   */
  public EditableLink beginLinkCreate(Agent caller, ForumIdentifier fid)
      throws SecurityException, BannedException, NotSubscribedException;

  /**
   * Cancel the creation of a new {@link Link}.
   * <p/>
   * This method should be preceded by {@link #beginLinkCreate(Agent,ForumIdentifier)}
   * to begin the create (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param el     an {@link EditableLink} instance.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @see #beginLinkCreate(Agent,ForumIdentifier)
   */
  public void cancelLinkCreate(Agent caller, EditableLink el)
      throws SecurityException;

  /**
   * Commits a  new {@link Link}.
   * <p/>
   * This method should be preceded by {@link #beginLinkCreate(Agent,ForumIdentifier)}
   * to begin an update (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param link   an {@link EditableLink} instance.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws IllegalStateException      if the transaction was not started by
   *                                    {@link #beginLinkCreate(Agent,ForumIdentifier)}.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   * @see #beginLinkCreate(Agent,ForumIdentifier)
   */
  public void commitLink(Agent caller, EditableLink link)
      throws SecurityException, IllegalStateException, DiscussionServiceException;

  /**
   * Returns a {@link Link} for the given identifier.
   * <p/>
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param id     the identifier of the link.
   * @return link   an {@link Link} instance.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public Link getLink(Agent caller, ForumIdentifier fid, String id)
    throws SecurityException, DiscussionServiceException;

  /**
   * Removes a {@link Link} from a given forum.
   * <p/>
   * This method is supposed to be used by the author of a {@link Link} to remove
   * it him/herself.
   *
   * @param caller the requesting {@link Agent}.
   * @param id     the identifier of the entry.
   * @param fid    a {@link ForumIdentifier}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void removeLink(Agent caller, ForumIdentifier fid, String id)
      throws SecurityException, DiscussionServiceException;

  /**
   * Returns the links by identifier in a given forum.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return a <tt>List</tt> of {@link Link} instances.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public List<String> listLinks(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException;

  /**
   * Returns the number of links in a given forum.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return the number of links in the given forum.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public int getLinkCount(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException;

  //*** END Link Handling ***//

  //*** Entry Handling ***//

  /**
   * Begin to create a new entry.
   * <p/>
   * This method should be followed by {@link #commitEntry(Agent,EditableEntry)}
   * to commit the create (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return an {@link EditableEntry} instance.
   * @throws SecurityException      if the calling {@link Agent} is not authentic or
   *                                not authorized.
   * @throws BannedException        if the caller is banned.
   * @throws NotSubscribedException if the caller is not subscribed.
   * @see #commitEntry(Agent,EditableEntry)
   */
  public EditableEntry beginEntryCreate(Agent caller, ForumIdentifier fid)
      throws SecurityException, BannedException, NotSubscribedException;

  /**
   * Begin to create a new official entry.
   * <p/>
   * This method should be followed by {@link #commitEntry(Agent,EditableEntry)}
   * to commit the create (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return an {@link EditableEntry} instance.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @see #commitEntry(Agent,EditableEntry)
   */
  public EditableEntry beginOfficialEntryCreate(Agent caller, ForumIdentifier fid)
      throws SecurityException;

  /**
   * Begin to create a new anonymous entry.
   * <p/>
   * This method should be followed by {@link #commitEntry(Agent,EditableEntry)}
   * to commit the create (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return an {@link EditableEntry} instance.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @throws AnonymousNotAllowedException if anonymous entries are not allowed in the
   *                                      specified forum.
   * @see #commitEntry(Agent,EditableEntry)
   */
  public EditableEntry beginAnonymousEntryCreate(Agent caller, ForumIdentifier fid)
      throws SecurityException, AnonymousNotAllowedException;
  
  /**
   * Commits a  new entry.
   * <p/>
   * This method should be preceded by {@link #beginEntryCreate(Agent,ForumIdentifier)}
   * to begin an update or {@link #beginOfficialEntryCreate(Agent,ForumIdentifier)} to begin
   * a create (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param entry  an {@link EditableEntry}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws IllegalStateException      if the transaction was not started either by
   *                                    {@link #beginEntryCreate(Agent,ForumIdentifier)} or by
   *                                    {@link #beginOfficialEntryCreate(Agent,ForumIdentifier)}.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   * @see #beginEntryCreate(Agent,ForumIdentifier)
   * @see #beginOfficialEntryCreate(Agent,ForumIdentifier)
   */
  public void commitEntry(Agent caller, EditableEntry entry)
      throws SecurityException, IllegalStateException, DiscussionServiceException;

  /**
   * Cancel the creation of a new entry.
   * <p/>
   * This method should be preceded by {@link #beginEntryCreate(Agent,ForumIdentifier)}
   * to begin the create (transaction style).
   *
   * @param caller the requesting {@link Agent}.
   * @param ee     an {@link EditableEntry} instance.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   * @see #beginEntryCreate(Agent,ForumIdentifier)
   */
  public void cancelEntryCreate(Agent caller, EditableEntry ee)
      throws SecurityException;

  /**
   * Removes an entry.
   * <p/>
   * This method is supposed to be used by the author of an entry to remove
   * it him/herself.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param id     the identifier of the entry.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void removeEntry(Agent caller, ForumIdentifier fid, String id)
      throws SecurityException, DiscussionServiceException;


  /**
   * Lists the entries in a given forum.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return a <tt>List</tt> of identifiers.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public List<String> listEntries(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException;


  /**
   * Lists the entries in a given forum from the given timestamp.
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param timestamp the timestamp of the last read message.
   * @return  a <tt>List</tt> of identifiers.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public List<String> listEntriesFrom(Agent caller, ForumIdentifier fid, long timestamp)
      throws SecurityException,DiscussionServiceException;

  /**
   * Lists a given number of entries in a given forum.
   * <p>
   * Note that the entries are ordered most recent first, and the number of
   * entries returned is from the most recent backwards in approval time.
   * </p>
   *
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @param numentries the number of messages.
   * @return  a <tt>List</tt> of identifiers.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public List<String> listEntries(Agent caller, ForumIdentifier fid, int numentries)
      throws SecurityException, DiscussionServiceException;

  /**
   * Searches for Entries with a given lucene query.
   * <p>
   * Note that the search will always be restricted to the actually subscribed
   * fora. Also the number of hits should be ranged to make sure you don't
   * get too much hits. You may restrict the forum yourself to a single, as
   * long as it is subscribed.
   * </p>
   * @param caller the requesting {@link Agent}.
   * @param query the query.
   * @param numhits the number of max hits to be returned.
   * @return A <tt>List</tt> of {@link EntryHit} instances identifying the hit.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public List<EntryHit> searchEntries(Agent caller, String query, int numhits)
      throws SecurityException, DiscussionServiceException;

  /**
   * Rebuilds the entry index.
   * @param caller the requesting {@link Agent}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void rebuildEntryIndex(Agent caller)
      throws SecurityException, DiscussionServiceException;

  /**
   * Updates the entry index with entries added after the last index modification.
   *
   * @param caller the requesting {@link Agent}.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public void updateEntryIndex(Agent caller)
      throws SecurityException, DiscussionServiceException;

  /**
   * Returns the number of active entries in the given forum.
   * @param caller the requesting {@link Agent}.
   * @param fid    a {@link ForumIdentifier}.
   * @return number of active entries in the forum.
   *
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public int getEntryCount(Agent caller, ForumIdentifier fid)
      throws SecurityException, DiscussionServiceException;

  /**
   * Checks if the discussion service has a new entry.
   *
   * @param caller    the requesting {@link Agent}.
   * @param timestamp long in millis UTC.
   * @param fid       a {@link ForumIdentifier}.
   * @return true if there is any new entry, false otherwise.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public boolean hasNewEntries(Agent caller, ForumIdentifier fid, long timestamp)
      throws SecurityException, DiscussionServiceException;

  /**
   * Returns the number of new entries in a specific forum.
   * <p>
   * Note that this is a high cost operation compared
   * to {@link #hasNewEntries(Agent,ForumIdentifier,long)}.
   * </p>
   * @param caller    the requesting {@link Agent}.
   * @param timestamp long in millis UTC.
   * @param fid       a {@link ForumIdentifier}.
   * @return the number of new entries in the given forum.
   *
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   */
  public int getNewEntryCount(Agent caller, ForumIdentifier fid, long timestamp)
     throws SecurityException,DiscussionServiceException;

  /**
   * Returns the {@link EntryDescriptor} of a given entry.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid       a {@link ForumIdentifier}.
   * @param id     the identifier of the entry.
   * @return a {@link EntryDescriptor} instance.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   *                                    not authorized.
   */
  public EntryDescriptor getEntryDescriptor(Agent caller, ForumIdentifier fid, String id)
      throws SecurityException, DiscussionServiceException;

  /**
   * Returns the actual {@link Content} of a given entry or comment.
   *
   * @param caller the requesting {@link Agent}.
   * @param fid       a {@link ForumIdentifier}.
   * @param id     the identifier of the entry.
   * @return a {@link Content} instance to access the content.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws DiscussionServiceException if an error occurs while executing this action.
   *                                    not authorized.
   */
  public Content getEntryContent(Agent caller, ForumIdentifier fid, String id)
      throws SecurityException, DiscussionServiceException;

  //*** END Entry Handling ***//


}//interface DiscussionService
