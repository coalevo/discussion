/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.service;

import net.coalevo.discussion.model.Link;
import net.coalevo.foundation.model.Service;

import java.io.Writer;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Defines a service for XML transformations of discussion
 * bundle models and collections.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface DiscussionXMLService
    extends Service {

  /**
   * Serializes the given {@link Link} instance
   * as XML to the given writer.
   * <p>
   * The serialized form is an XBEL fragment.
   * </p>
   *
   * @param w      a <tt>Writer</tt>.
   * @param l      a {@link Link} instance.
   *
   * @throws IOException if an I/O error occurs using the writer.
   */
  public void toXML(Writer w, Link l)
      throws IOException;


  /**
   * Serializes the given {@link Link} instance
   * as XML to the given output stream.
   * <p>
   * The serialized form is an XBEL fragment.
   * </p>
   *
   * @param out      an <tt>OutputStream</tt>.
   * @param l      a {@link Link} instance.
   *
   * @throws IOException if an I/O error occurs using the output stream.
   */
  public void toXML(OutputStream out, Link l)
      throws IOException;

  /**
   * Convenience method that will return a serialized
   * {@link Link} instance as XML fragment in a <tt>String</tt>.
   * <p>
   * The serialized form is an XBEL fragment.
   * </p>
   *
   * @param l      a {@link Link} instance.
   * @return a <tt>String</tt> containing an XBEL fragment.
   *
   * @throws IOException if an I/O error occurs writing the fragment.
   */
  public String toXML(Link l)
      throws IOException;

}//interface DiscussionXMLService
