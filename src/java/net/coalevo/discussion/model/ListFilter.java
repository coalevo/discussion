/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

import java.util.List;
import java.util.Iterator;

/**
 * Defines a generic filter for lists from the discussion service.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class ListFilter<T> {

  /**
   * Tests if this filter passes a given item.
   *
   * @param item the item to be tested.
   * @return true if passes, false otherwise.
   */
  public abstract boolean passes(T item);

  /**
   * Applies this filter to a given list.
   * @param list the list of items to be filtered.
   */
  public void apply(List<T> list) {
    for (Iterator<T> iterator = list.iterator(); iterator.hasNext();) {
      T t =  iterator.next();
      if(!passes(t)) {
        iterator.remove();
      }
    }
  }//apply

}//class ListFilter
