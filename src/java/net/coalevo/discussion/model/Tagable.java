/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

import java.util.Set;

/**
 * Defines an interface for elements that are <tt>Tagable</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Tagable {

  /**
   * Sets all tags of this <tt>Tagable</tt>.
   *
   * @param tags a set of strings.
   */
  public void setTags(Set<String> tags);

  /**
   * Adds a tag to the entry.
   *
   * @param tag a tag String.
   */
  public void addTag(String tag);

  /**
   * Removes a tag from this entry.
   *
   * @param tag a tag String.
   */
  public void removeTag(String tag);

}//interface Tagable
