/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

/**
 * This class defines tokens for XML transformation of
 * discussion entries.
 * <p>
 * Note that these are also used partially for the index
 * documents.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class DiscussionTokens {

  public static final String ATTR_ID = "id";
  public static final String ATTR_FORUM = "forum";
  public static final String ATTR_ANCESTOR = "ancestor";
  public static final String ATTR_AUTHOR = "author";
  public static final String ATTR_TAGS = "tags";
  public static final String ATTR_LANG = "lang";
  
  public static final String ELEMENT_CAPTION = "caption";
  public static final String ELEMENT_TAGS = "tags";

  public static final String ELEMENT_CONTENT_FORMAT = "content-format";
  public static final String ELEMENT_CREATED = "created";
  public static final String ELEMENT_APPROVED = "approved";
  public static final String ELEMENT_OFFICIAL = "official";
  public static final String ELEMENT_ARCHIVED = "archived";


  /*** XBEL ***/
  public static final String ELEMENT_XBEL = "xbel";
  public static final String ELEMENT_XBEL_INFO = "info";
  public static final String ELEMENT_XBEL_METADATA = "metadata";
  public static final String ELEMENT_XBEL_TITLE = "title";
  public static final String ELEMENT_XBEL_DESC = "desc";
  public static final String ELEMENT_XBEL_BOOKMARK = "bookmark";
  public static final String ELEMENT_XBEL_SEPARATOR = "separator";
  public static final String ELEMENT_XBEL_FOLDER = "folder";

  public static final String ELEMENT_XBEL_THISMETA = "link";
  public static final String ELEMENT_XBEL_THISMETA_NS = "cdb";

  public static final String ATTR_XBEL_ADDED = "added";
  public static final String ATTR_XBEL_VISITED = "visited";
  public static final String ATTR_XBEL_MODIFIED= "modified";
  public static final String ATTR_XBEL_HREF = "href";
  public static final String ATTR_XBEL_OWNER = "owner";
  public static final String ATTR_XBEL_OWNER_VALUE = "net.coalevo.discussion";


}//class DiscussionTokens
