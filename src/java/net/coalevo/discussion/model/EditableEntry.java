/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

/**
 * Defines an editable entry to be added to the discussion.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface EditableEntry
    extends EntryDescriptor, Tagable {

  /**
   * Sets the ancestor of this post.
   *
   * @param id the identifier of the ancestor.
   */
  public void setAncestor(String id);

  /**
   * Sets the caption of the entry.
   *
   * @param str the subject of the entry.
   */
  public void setCaption(String str);

  /**
   * Sets the format of the content.
   *
   * @param str a string identifying the content format.
   */
  public void setContentFormat(String str);

  /**
   * Sets the flag for allowing or disallowing descendants.
   *
   * @param b true if descendants are allowed, false otherwise.
   */
  public void setDescendantAllowed(boolean b);

  /**
   * Sets the flag that determines if the entry is approved.
   *
   * @param b true if approved, false otherwise.
   */
  public void setApproved(boolean b);

  /**
   * Returns the editable content of this <tt>EditableEntry</tt>.
   * @return the {@link EditableContent}.
   */
  public EditableContent getContent();

}//interface EditableEntry
