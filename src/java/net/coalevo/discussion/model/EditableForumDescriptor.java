/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

import net.coalevo.foundation.model.AgentIdentifier;

import java.util.Locale;

/**
 * Defines an editable forum descriptor.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface EditableForumDescriptor
    extends ForumDescriptor, Tagable {

  /**
   * Sets the name of the forum.
   * @param str the name as String.
   */
  public void setName(String str);

  /**
   * Sets the mnemonic of the forum.
   *
   * @param str the mnemonic as String.
   */
  public void setMnemonic(String str);

  /**
   * Sets the locale defining the language of the forum.
   *
   * @param l a locale defining the language.
   */
  public void setLanguage(Locale l);

  /**
   * Sets the format of the forum info.
   *
   * @param str a string defining the format of the info.
   */
  public void setInfoFormat(String str);

  /**
   * Sets the sequence number.
   *
   * @param n the sequence number.
   */
  public void setSequenceNumber(int n);

  /**
   * Sets the moderator.
   * @param aid an {@link AgentIdentifier}.
   */
  public void setModerator(AgentIdentifier aid);

  /**
   * Sets the forum's strict moderation flag.
   *
   * @param b true if moderated strictly, false otherwise.
   */
  public void setModeratedStrict(boolean b);

  /**
   * Sets the flag that determines if anonymous entries are allowed.
   * @param b true if allowed, false otherwise.
   */
  public void setAnonymousAllowed(boolean b);

  /**
   * Sets the forum's invite only flag.
   *
   * @param b true if to be invite only, false otherwise.
   */
  public void setInviteOnly(boolean b);

  /**
   * Sets the forum's public flag.
   * @param b true if to be public, false otherwise.
   */
  public void setPublic(boolean b);

  /**
   * Sets the maximum number of active entries.
   * Note that the minimum is currently 200, any number lower than
   * 200 will be ignored and replaced by 200.
   *
   * @param num the maximum number of active entries in this.
   */
  public void setMaxActive(int num);

}//interface EditableForumDescriptorImpl
