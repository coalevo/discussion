/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

import net.coalevo.foundation.model.Identifiable;
import net.coalevo.foundation.model.AgentIdentifier;

import java.util.Set;

/**
 * Defines a descriptor for discussion entries.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface EntryDescriptor
    extends Identifiable {

  /**
   * Returns the identifier of the forum this entry belongs to.
   *
   * @return a {@link ForumIdentifier}.
   */
  public ForumIdentifier getForumIdentifier();

  /**
   * Returns the identifier of the ancestor entry.
   *
   * @return the ancestors identfier.
   */
  public String getAncestor();

  /**
   * Returns the identifier of the author of the entry.
   * @return the {@link AgentIdentifier} of the author.
   */
  public AgentIdentifier getAuthor();

  /**
   * Returns the caption of the entry.
   *
   * @return the caption of the entry as String.
   */
  public String getCaption();

  /**
   * Returns the content format of this entry.
   * @return a String defining the content format.
   */
  public String getContentFormat();

  /**
   * Returns a list of tags attached to thr entry.
   *
   * @return a <tt>List</tt> of String tags.
   */
  public Set<String> getTags();

  /**
   * Returns the creation timestamp.
   *
   * @return long as millis from UTC.
   */
  public long getCreated();

  /**
   * Returns the timestamp of the approval.
   * <p />
   *
   * @return long as millis from UTC.
   */
  public long getApproved();

  /**
   * Tests if this entry is approved.
   *
   * @return true if approved, false otherwise.
   */
  public boolean isApproved();

  /**
   * Tests if this entry is official.
   *
   * @return true if official, false otherwise.
   */
  public boolean isOfficial();

  /**
   * Tests if this entry is a descendant.
   *
   * @return true if descendant, false otherwise.
   */
  public boolean isDescendant();

  /**
   * Tests if further threading is allowed.
   *
   * @return true if allowed, false otherwise.
   */
  public boolean isDescendantAllowed();

  /**
   * Tests if this <tt>EntryDescriptor</tt> is archived.
   * @return true if archived, false otherwise.
   */
  public boolean isArchived();

}//interface EntryDescriptor
