/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

/**
 * Defines an editable link.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface EditableLink
    extends Link, Tagable {

  /**
   * Returns the identifier of the forum.
   * @return a {@link ForumIdentifier} instance.
   */
  public ForumIdentifier getForumIdentifier();

  /**
   * Sets the caption of this link.
   * @param caption a string caption.
   */
  public void setCaption(String caption);

  /**
   * Sets the description format of this link.
   * @param format a string identifying the format of the description.
   */
  public void setDescriptionFormat(String format);

  /**
   * Sets the description of this link.
   * @param desc the description as string.
   */
  public void setDescription(String desc);

  /**
   * Sets the link.
   *
   * @param link the link as String.
   */
  public void setLink(String link);

  /**
   * Sets the functional flag of this link.
   * <p />
   * When set functional, the timestamp of the last check
   * should be automatically updated.
   *
   * @param b true if functional, false otherwise.
   */
  public void setFunctional(boolean b);

  /**
   * Sets the flag for approval or disapproval of the link.
   *
   * @param b true if approved, false otherwise.
   */
  public void setApproved(boolean b);

  /**
   * Updates the UTC timestamp for the last check.
   */
  public void updateLastChecked();

}//interface EditableLink
