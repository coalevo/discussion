/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

import net.coalevo.foundation.model.AgentIdentifier;

import java.util.Locale;
import java.util.Set;

/**
 * Defines the forum descriptor.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface ForumDescriptor {

  /**
   * Returns the identifier of the forum.
   * @return a {@link ForumIdentifier} instance.
   */
  public ForumIdentifier getIdentifier();

  /**
   * Returns the sequence number of the forum.
   * @return a forum sequence number.
   */
  public int getSequenceNumber();

  /**
   * Returns the mnemonic assigned to the forum.
   *
   * @return the forum mnemonic.
   */
  public String getMnemonic();

  /**
   * Returns the forum name.
   *
   * @return the forum name.
   */
  public String getName();

  /**
   * Returns the locale that defines the forum's language.
   *
   * @return a <tt>Locale</tt> defining the forum language.
   */
  public Locale getLanguage();

  /**
   * Returns the identifier of the creator of the forum.
   *
   * @return an {@link AgentIdentifier} instance.
   */
  public AgentIdentifier getCreator();

  /**
   * Returns the identifier of the moderator of the forum.
   *
   * @return an {@link AgentIdentifier} instance.
   */
  public AgentIdentifier getModerator();

  /**
   * Returns a list of tags attached to thr entry.
   *
   * @return a <tt>List</tt> of String tags.
   */
   public Set<String> getTags();

  /**
   * Returns a string defining the format of the forum info.
   *
   * @return a string defining the info format.
   */
  public String getInfoFormat();

  /**
   * Tests if the forum is moderated strictly.
   * <p />
   * Strict moderation means that each entry and comment requires
   * moderator approval.
   *
   * @return true if moderated strictly, false otherwise.
   */
  public boolean isModeratedStrict();

  /**
   * Tests if anonymous entries are allowed.
   * @return true if allowed, false otherwise.
   */
  public boolean isAnonymousAllowed();

  /**
   * Tests if the forum is invite only.
   * <p />
   * Invite only means that the moderator has to subscribe users
   * to the forum.
   *
   * @return true if invite only, false otherwise.
   */
  public boolean isInviteOnly();

  /**
   * Tests if this forum is public.
   * <p />
   * If the forum is public, this means that it may be
   * accessible by anonymous, non authenticated users.
   * For example by public web access.
   *
   * @return true if public, false otherwise.
   */
  public boolean isPublic();

  /**
   * The creation date of the forum.
   *
   * @return the creation date as UTC timestamp.
   */
  public long getCreated();

  /**
   * The modification date of the forum.
   *
   * @return the last modification date as UTC timestamp.
   */
  public long getModified();

  /**
   * Returns the maximum number of active entries.
   * If there are more entries in a specific forum, they will be archived
   * by the maintenance mechanism.
   *
   * @return the number of max. active entries in this forum.
   */
  public int getMaxActive();

  /**
   * Tests if this descriptor is invalidated.
   * @return true if invalidated, false otherwise.
   */
  public boolean isInvalidated();

}//interface ForumDescriptor
