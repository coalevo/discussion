/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

/**
 * Exception thrown when trying to subscribe to, or access content
 * from a forum the agent has been banned from.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class BannedException
    extends Exception {


  public BannedException() {
  }

  public BannedException(String s) {
    super(s);
  }

  public BannedException(String message, Throwable cause) {
    super(message, cause);
  }

  public BannedException(Throwable cause) {
    super(cause);
  }
}//class BannedException
