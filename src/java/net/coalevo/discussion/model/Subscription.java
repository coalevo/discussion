/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Identifiable;

/**
 * Defines a subscription.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Subscription
    extends Identifiable, Cachable {

  /**
   * Returns the forum identifier if this
   * subscription.
   *
   * @return a {@link ForumIdentifier}.
   */
  public ForumIdentifier getForumIdentifier();

  /**
   * Returns the agent identifier of the subscriber.
   *
   * @return an {@link AgentIdentifier} instance.
   */
  public AgentIdentifier getSubscriber();

  /**
   * Returns a note about this subscription.
   * May be used to comment subscriptions to invite only
   * fora.
   *
   * @return a note as String.
   */
  public String getNote();

  /**
   * Sets the note about this subscription.
   * @param note the note as String.
   */
  public void setNote(String note);

  /**
   * Returns the timestamp of the last read message.
   *
   * @return the timestamp as millis UTC.
   */
  public long getLastRead();

  /**
   * Sets the timestamp of the last read message.
   *
   * @param timestamp as millis UTC.
   */
  public void setLastRead(long timestamp);

  /**
   * Tests if this subscription is notifying.
   *
   * @return true if notification required, false otherwise.
   */
  public boolean isNotifying();

  /**
   * Sets this subscription to be notifying.
   *
   * @param b true if notification required, false otherwise.
   */
  public void setNotifying(boolean b);

  /**
   * Returns the timestamp of creation of this subscription.
   *
   * @return long as millis UTC.
   */
  public long getCreated();

}//interface Subscription
