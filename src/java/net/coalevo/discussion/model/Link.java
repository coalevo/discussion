/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Identifiable;

import java.util.Set;

/**
 * Defines a link descriptor.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Link extends Identifiable {

  /**
   * Returns the forum identifier of the forum this <tt>Link</tt> belongs to.
   * @return a {@link ForumIdentifier}.
   */
  public ForumIdentifier getForumIdentifier();

  /**
   * Returns the identifier of the link author.
   *
   * @return an {@link AgentIdentifier} instance.
   */
  public AgentIdentifier getAuthor();

  /**
   * Returns the link's caption.
   *
   * @return the caption of the link.
   */
  public String getCaption();

  /**
   * Returns the format of the description.
   * @return a string identifying the format of the description.
   */
  public String getDescriptionFormat();

  /**
   * Returns the description of the link.
   * @return a string representing the description.
   */
  public String getDescription();

  /**
   * Returns the link.
   *
   * @return the link as String.
   */
  public String getLink();

  /**
   * Returns a list of tags attached to this link.
   *
   * @return a <tt>Set</tt> of String tags.
   */
  public Set<String> getTags();

  /**
   * Tests if the link is approved.
   *
   * @return true if approved, false otherwise.
   */
  public boolean isApproved();

  /**
   * Tests if the link is functional.
   * @return true if functional, false otherwise.
   */
  public boolean isFunctional();

  /**
   * Returns the timestamp of this link in millis UTC.
   *
   * @return the timestamp as long millis UTC.
   */
  public long getCreated();

  /**
   * Returns the timestamp of the last modification of this link in millis UTC.
   *
   * @return the timestamp as long millis UTC.
   */
  public long getModified();


  /**
   * The timestamp of the last check if the link is functional.
   * @return a long millis UTC timestamp.
   */
  public long getLastChecked();

}//interface Link
