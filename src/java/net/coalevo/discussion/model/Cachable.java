/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

/**
 * Defines an interface for a simple cachable element.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Cachable {

  /**
   * Tests if this <tt>Cachable</tt> is dirty.
   *
   * @return true if dirty, false otherwise.
   */
  public boolean isDirty();

  /**
   * Sets the flag that indicates if this <tt>Cachable</tt>
   * is dirty.
   * @param b true if dirty, false otherwise.
   */
  public void setDirty(boolean b);

}//interface Cachable
