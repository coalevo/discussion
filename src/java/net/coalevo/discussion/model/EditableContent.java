/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

import java.io.InputStream;
import java.io.IOException;

/**
 * Defines an editable content.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface EditableContent extends Content {

  /**
   * Sets the content.
   * @param content a String.
   */
  public void setContent(String content);

  /**
   * Sets the content from the given <tt>InputStream</tt>.
   * @param in the <tt>InputStream</tt> to obtain the content from.
   * @throws IOException if I/O fails.
   */
  public void setContent(InputStream in) throws IOException;

  /**
   * Returns a new non-editable content instance.
   * @return a {@link Content} instance.
   */
  public Content toContent();
  
}//interface EditableContent
