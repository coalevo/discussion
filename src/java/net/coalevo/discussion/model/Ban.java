/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.discussion.model;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Identifiable;

/**
 * Defines a ban from a forum.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Ban
    extends Identifiable, Cachable {

  /**
   * Returns the identifier of the banned agent.
   *
   * @return the banned agent's {@link AgentIdentifier}.
   */
  public AgentIdentifier getWho();

  /**
   * Returns the identifier of the forum.
   *
   * @return the forum's {@link ForumIdentifier}.
   */
  public ForumIdentifier getForumIdentifier();

  /**
   * Returns the identifier of the banning agent.
   *
   * @return the banning agent's {@link AgentIdentifier}.
   */
  public AgentIdentifier getByWhom();

  /**
   * Returns the reason for this <tt>Ban</tt>.
   *
   * @return the reason as String.
   */
  public String getReason();

  /**
   * Returns the timestamp specifying until when this
   * ban is active.
   *
   * @return the timestamp or -1 if undefined (e.g. forever until revoked).
   */
  public long getUntil();

  /**
   * Returns the timestamp specifying when this ban was created.
   *
   * @return the timestamp as millis from UTC.
   */
  public long getCreated();

  /**
   * Tests if this <tt>Ban</tt> is active.
   *
   * @return tru if active, false otherwise.
   */
  public boolean isActive();

  /**
   * Sets the identifier of the banning agent.
   *
   * @param aid the banning agent's {@link AgentIdentifier}.
   */
  public void setByWhom(AgentIdentifier aid);

  /**
   * Sets the reason for this <tt>Ban</tt>.
   *
   * @param reason the reason for this <tt>Ban</tt> as String.
   */
  public void setReason(String reason);

  /**
   * Sets the validity of this <tt>Ban</tt>.
   *
   * @param timestamp the validity of the ban as long millis UTC or -1 if forever.
   */
  public void setUntil(long timestamp);

}//interface Ban
